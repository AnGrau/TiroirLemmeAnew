#! /bin/sh

echo "Check des dépendances"
echo ""
dpkg -s python3-pip > trash_file
varpip=$?

for pkt in "numpy" "nltk" "pyqt5" "pyqt5.qtsql" "lxml" 
do
	eval "dpkg -s python3-$pkt > trash_file"
	if [ $? -eq 0 ]
	then
		echo "$pkt est installé via un .deb"
	elif [ $varpip -eq 0 ]
	then
		eval "pip3 show $pkt > trash_file"
		if [ $? -eq 0 ]
		then
			echo "$pkt est installé via pip3"
		else
			eval "apt-get install python3-$pkt"
			if [ $? -eq 0 ]
			then 
				echo "Installation de $pkt OK"
			else 
				echo "Installation de $pkt ECHEC" 
			fi
		fi
	else
		eval "apt-get install python3-$pkt"
		if [ $? -eq 0 ]
		then 
			echo "Installation de $pkt OK"
		else 
			echo "Installation de $pkt ECHEC" 
		fi
	fi
done

dpkg -s libqt5sql5-psql > trash_file
if [ $? -eq 0 ]
then
	echo "libqt5sql5-psql est installé via un .deb"
elif [ $varpip -eq 0 ]
then
	pip3 show libqt5sql5-psql > trash_file
	if [ $? -eq 0 ]
	then
		echo "libqt5sql5-psql est installé via pip3"
	else
		apt-get install libqt5sql5-psql
		if [ $? -eq 0 ]
		then 
			echo "Installation de libqt5sql5-psql OK"
		else 
			echo "Installation de libqt5sql5-psql ECHEC" 
		fi
	fi
else
	apt-get install libqt5sql5-psql
	if [ $? -eq 0 ]
	then 
		echo "Installation de libqt5sql5-psql OK"
	else 
		echo "Installation de libqt5sql5-psql ECHEC" 
	fi
fi

echo "Téléchargement des stopwords pour nltk"
echo "import nltk" > cplt.py
echo "if nltk.download('stopwords') == False :" >> cplt.py
echo "\texit(-1)">> cplt.py

python3 cplt.py
if [ $? -eq 0 ];
then
	echo "Installation des nltk.stopwords OK"
else
	echo "Installation des nltk.stopwords ECHEC"
fi
echo "Suppression de cplt.py"
rm cplt.py trash_file

echo ""
echo "Fin du check des dépendances"

if [ $# -eq 1 ]
then
	if [ $1 = "-i" ]
	then
		echo "Lancement de l'installation du .deb"
		dpkg -i python3-tiroirlemmeanew_1.0.0-1_all.deb
	fi
fi

