#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

def readme():
	with open('README.rst') as f:
		return f.read()


setup(name='TiroirLemmeAnew',
	version='1.0.0',
	description='Création de fichiers racines, flexions et modèles de noms et de verbes pour le programme AnalMor',
	long_description=readme(),
	classifiers=[
		'Development Status :: 5 - Production/Stable',
		'License :: OSI Approved :: GNU General Public License v2 or later (GPLv2+)',
		'Programming Language :: Python :: 3.5',
		'Topic :: Text Processing :: Linguistic',
	],
	keywords='lemmatisation racinisation morphologique',
	url='https://gitlab.com/AnGrau/TiroirLemmeAnew',
	author='Boris Merminod, Anthony Grau',
	author_email='anthony.grau@etud.univ-paris8.fr',
	license='GPLv2+',
	packages=find_packages(),
	install_requires=[
		'beautifulsoup4',
	],
	test_suite='nose.collector',
	tests_require=['nose'],
	entry_points = {
		'console_scripts': ['tiroirLemme=TiroirLemmeAnew.mod:main', 'tirLemDb=TiroirLemmeAnew.database:main_db'],
	},
	include_package_data=True,
	zip_safe=False)
