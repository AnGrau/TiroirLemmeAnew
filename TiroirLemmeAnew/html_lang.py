#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# Utilisation
# ./html_lang.py https://fr.wikipedia.org/wiki/Wiki

import sys
import urllib.request
import urllib.parse
from threading import Thread, RLock
from bs4 import BeautifulSoup
import string
import re
from operator import itemgetter
try:
    from nltk_stopwords import detect_language
except ImportError :
    from TiroirLemmeAnew.nltk_stopwords import detect_language

import nltk

try:
    from data_fetch import write_pickle, word_tags, erreur, build_tags
except ImportError :
    from TiroirLemmeAnew.data_fetch import write_pickle, word_tags, erreur, build_tags

from string import ascii_letters
from pprint import pprint

LIMIT_VERBES = 4000
LIMIT_NOMS = 4000

liens = []
url_complete = ''
url_done = []
liste_verbes = []
liste_noms = []
liste_mots = []
latin_alphabet = ascii_letters + "àâäéèêëïîôöùûüÿçÀÂÄÇÉÈÊËÎÏÔÖÙÛÜŸæÆœŒ"
verrou = RLock()

def construit(url):
	"""Cette fonction construit l'url à suivre à partir du nom de domaine ou de l'url partielle.

Args:
	url(str): le nom de domaine ou l'url partielle.

Returns:
	url(str): l'url complétée."""
	
	if 'https' not in url and '//fr.wikipedia.org' in url:
		url = 'https:' + url
	elif 'https://fr.wikipedia.org' not in url:
		url = 'https://fr.wikipedia.org' + url
	return url

def lire_url(url_name):
	"""Cette fonction ouvre le lien fourni et extrait le code html de la page.

Args:
	url_name(str): le lien à ouvrir

Return:
	s(str): le code html de la page ouverte.
	"""
	try:
		with urllib.request.urlopen(url_name) as url:
			s = url.read()
	except:
		return None
	return s

def extraire_texte(html):
	"""Cette fonction extrait les liens de la page, ainsi que le texte hors balises.

Args:
	html(str): le code html de la page

Returns:
	text(str): le texte hors balises
	"""
	global liens

	soup = BeautifulSoup(html, 'html.parser')		# traite le code html

	for link in soup.find_all('a'):					# extraction des liens
		liens += [link.get('href')]

	for x in liens:
		if x and 'javascript' in x:
			liens.remove(x)

	liens = [construit(i) for i in liens if i and 'https://' not in i and '/wiki/' in i and i != url_complete and '.png' not in i and '.jpg' not in i]

	for script in soup(["script", "style"]):		# supprimer les scripts et les éléments de style
		script.extract()    						# extraire

	text = soup.get_text(separator=u' ')			# obtenir le texte

	# séparer les lignes
	lines = (line.strip() for line in text.splitlines())
	chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
	text = '\n'.join(chunk for chunk in chunks if chunk)
	return text

def tolist(texte):
        """Conversion du texte brut en liste de mots.

Args:
        texte(str): le texte brut

Return:
        result_list(list): la liste des mots.
        """

        result_list = []
        to_add = []
        line = ''

        tmp = texte.split('\n')

        for x in tmp:
            to_add += x.split()

        for elt in to_add:
            result_list += [word.lower() for word in re.split(r'''[—!"#$%&'’()\*\+,-\./:;<=>\?@\[\\\]\^_`{\|}~0123456789»«·•–↑]+''', elt) if (len(word) > 2 and word[0] in latin_alphabet)]

        return result_list


class TraiteUrl(Thread):


	"""Thread chargé d'ouvrir les url."""

	__counter = 0


	def __init__(self, compteur, dico_tag_verbs, dico_tag_names):
		Thread.__init__(self)
		self.dico_tag_verbs = dico_tag_verbs
		self.dico_tag_names = dico_tag_names
		self.compteur = compteur


	def run(self):

		"""Code à exécuter pendant l'exécution du thread."""

		global url_done
		global liste_verbes
		global liste_noms
		global liste_mots

		
		nom = liens[self.compteur]


		if nom not in url_done:
			html = lire_url(nom)
			if html:
				texte = extraire_texte(html)
				tmp = tolist(texte)
				url_done += [nom]

				language = detect_language(' '.join(tmp))

				if language == 'french':
					with verrou:
						#print(nom)
						liste_mots += tmp
					
						if len(liste_noms) >= LIMIT_NOMS :
							liste_verbes, liste_noms = word_tags(self.dico_tag_verbs, self.dico_tag_names,liste_verbes, liste_noms, tmp, False)
						elif len(liste_verbes) >= LIMIT_VERBES :
							liste_verbes, liste_noms = word_tags(self.dico_tag_verbs, self.dico_tag_names,liste_verbes, liste_noms, tmp, True, False)
						else :
							liste_verbes, liste_noms = word_tags(self.dico_tag_verbs, self.dico_tag_names,liste_verbes, liste_noms, tmp)
					
					if liste_verbes == None or liste_noms == None :
						return erreur("main : Erreur échec de la fonction word_tags", (None, None))
					
				else:
					pass

def main_html(url):
	"""Cette fonction extrait les données d'une url
	en suivant les liens si le nombre de mots trouvés est insuffisant.
	Elle determine le langage utilisé sur la page. Si c'est du français, elle extrait les mots.

Args:
	url(str): l'adresse du site à analyser.

Returns:
	liste_mots(list): les mots extraits de la page sous forme de liste
	liste_verbes (list): les verbes extraits de la page sous forme de liste
	liste_noms (list): les noms extraits de la page sous forme de liste
	dico_tag_verbs (dict) : Dictionnaire contenant tous les verbes du dictionnaire éléctronique et les tags possible pour ces verbes
	dico_tag_names (dict) : Dictionnaire contenant tous les noms communs du dictionnaire éléctronique et les tags possible pour ces noms
	"""

	global url_complete
	global url_done
	global liste_verbes
	global liste_noms
	global liste_mots

	if '/wiki/' in url:
		url = urllib.parse.quote(url, safe=':/%')
	else:
		exit("\nL'argument doit être une adresse Wikipédia.\n")

	compteur = 0
	url_complete = construit(url)
	nom = url_complete
	url_done = [url_complete]

	if 'http' in nom:
		html = lire_url(nom)
	else:
		exit("l'argument doit être une url")

	texte = extraire_texte(html)
	liste_mots = tolist(texte)

	language = detect_language(' '.join(liste_mots))

	if language != 'french':
		exit('Le langage de la page ne semble pas être le français. Le programme est interrompu.')
	
	dico_tag_verbs, dico_tag_names = build_tags("Morphalou3.1_LMF.xml")
	if dico_tag_verbs == None :
		return erreur("main : Erreur échec de la fonction data_fetch.build_tags", (None, None))

	liste_verbes, liste_noms = word_tags(dico_tag_verbs, dico_tag_names,liste_verbes, liste_noms, liste_mots)

	longueur_a_suivre = len(liens)

	if longueur_a_suivre > 0:
		while compteur+8 < longueur_a_suivre  and (len(liste_verbes) < LIMIT_VERBES or len(liste_noms) < LIMIT_NOMS):
			lien_1 = TraiteUrl(compteur, dico_tag_verbs, dico_tag_names)
			compteur += 1
			lien_2 = TraiteUrl(compteur, dico_tag_verbs, dico_tag_names)
			compteur += 1
			lien_3 = TraiteUrl(compteur, dico_tag_verbs, dico_tag_names)
			compteur += 1
			lien_4 = TraiteUrl(compteur, dico_tag_verbs, dico_tag_names)
			compteur += 1
			lien_5 = TraiteUrl(compteur, dico_tag_verbs, dico_tag_names)
			compteur += 1
			lien_6 = TraiteUrl(compteur, dico_tag_verbs, dico_tag_names)
			compteur += 1
			lien_7 = TraiteUrl(compteur, dico_tag_verbs, dico_tag_names)
			compteur += 1
			lien_8 = TraiteUrl(compteur, dico_tag_verbs, dico_tag_names)
			compteur += 1
			lien_1.start()
			lien_2.start()
			lien_3.start()
			lien_4.start()
			lien_5.start()
			lien_6.start()
			lien_7.start()
			lien_8.start()
			lien_1.join()
			lien_2.join()
			lien_3.join()
			lien_4.join()
			lien_5.join()
			lien_6.join()
			lien_7.join()
			lien_8.join()

	liste_mots.sort()
	liste_verbes.sort(key=itemgetter(0))
	liste_noms.sort(key=itemgetter(0))

	return liste_mots, liste_verbes, liste_noms, dico_tag_verbs, dico_tag_names



if __name__ == '__main__':
	if "-t" in sys.argv :
		LIMIT_VERBES = 4000
		LIMIT_NOMS = 4000
		mots, verbes, noms, dicoVerbs, dicoNames = main_html("https://fr.wikipedia.org/wiki/France")
		print("Sauvegarde des structures de données pour les tests")
		write_pickle(mots, "tests/wordlist.pickle")
		write_pickle(verbes, "tests/lverbs.pickle")
		write_pickle(noms, "tests/lnames.pickle")
		write_pickle(dicoVerbs, "tests/dico_tag_verbs.pickle")
		write_pickle(dicoNames, "tests/dico_tag_names.pickle")	
		
	elif len(sys.argv) == 2:

		mots, verbes, noms, dicoVerbs, dicoNames = main_html(sys.argv[1])

	else:
		exit("Un argument, l'url de la page à crawler ou -t.")












