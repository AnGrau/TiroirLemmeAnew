#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#Utilisation :
# ./build_lemme.py https://fr.wikipedia.org/wiki/Wiki
#Lancement des test :
# ./build_lemme.py -t


try:
    from data_fetch import erreur, write_pickle
except ImportError :
    from TiroirLemmeAnew.data_fetch import erreur, write_pickle

try:
    from html_lang import *
except ImportError :
    from TiroirLemmeAnew.html_lang import *

try:
    from clustering import p_similar, transcaracter, main_clust
except ImportError :
    from TiroirLemmeAnew.clustering import p_similar, transcaracter, main_clust

import sys
from pprint import pprint
from sys import argv
from operator import itemgetter

def build_lemmeList(dico_clust, termTree = []) :
	"""
		Construction de la lemmeList en vue de l'écriture du fichier lemmeBrut. La fonction prend en paramètre dico_clust, un dictionnaire des clusters des verbes/noms modèles
	
		Arg:
		dico_clust (dict) : dictionnaire des clusters des verbes/noms modèles
		termTree (list) : index arborescent qui va servir à identifier les modèles des verbes
	
		Return:
		lemmeList (list) : liste de chaînes de caractères en vue d'écrire le fichier lemmeBrut
	"""
	
	if type(dico_clust) != dict :
		return erreur("build_lemmeList : Erreur paramètre dico_clust non valide {}".format(dico_clust), None)
	if type(termTree) != list :
		return erreur("build_lemmeList : Erreur paramètre termTree non valide {}".format(termTree), None)
	
	lemmeList = []
	list_key = list(dico_clust.keys())
	list_key.sort()
	for word in  list_key:
		if type(dico_clust[word][-1]) != list :
			return erreur("build_lemmeList : Erreur problème d'identification des racines ici {}".format(dico_clust[word]), None)
			
		for i in range(len(dico_clust[word][-1])) :
			if termTree != [] :
				modele = search_term_tree(termTree, word[::-1], "")
				if modele == None :
					return erreur("build_lemmeList : Erreur problème à l'identification du modèle pour le mot :{}".format(word),None)
			else :
				modele = word
			line = [dico_clust[word][-1][i], word, modele, i+1]
			lemmeList.append(line)
	
	return lemmeList		

def stemmatise(dico_clust) :
	"""
		Optimisation simple des racines contenues dans le dictionnaire dico_clust
	
		Arg :
		dico_clust (dict) : Dictionnaire des verbes/noms modèles avec leurs formes fléchies et leurs racines à optimiser
	
		Return :
		dico_clust (dict) :  Dictionnaire des verbes modèles avec leur formes fléchies et leurs racines après optimisation. 
		None : La fonction retourne None en cas d'erreur
	"""
	
	if type(dico_clust) != dict :
		return erreur("stemmatise : Erreur paramètre attendu incorrect : {}".format(dico_clust), None)

	for modele in dico_clust.keys() :
		flexlist = dico_clust[modele][:-1][:]
		stemlist =  dico_clust[modele][-1][:]
		stemlist.sort()
		#stemlist.reverse()
		
		finalstem = []
		
		for stem in stemlist : 
			if len(stem) == 1 :
				continue
			for i in range(len(flexlist)) :
				if flexlist[i] == 0 :
					continue
				if stem in flexlist[i] :
					flexlist[i] = 0
					if stem not in finalstem :
						finalstem.append(stem)
						
		dico_clust[modele][-1] = finalstem
	
	return dico_clust

def final_clust(families, dico_tag = {}, modelist=[]) :
	"""
		Fonction permettant de finir d'unifier les clusters construits automatiquement par l'algorithme de Gaussier. La fonction construit un dictionnaire avec l'ensemble des verbes modèles en clés, et une liste de leurs formes fléchies et de leurs racines en valeur
	
		Arg :
		families (list) : Liste des clusters de verbes 
		dico_tag (dict) : Dictionnaire électronique pour l'identification des verbes modèles
		modelist (list) : Liste contenant les verbes modèles à l'infinitif
	
		Returns :
		dico_clust (dict) : Dictionnaire des formes fléchies des verbes modèles 
		families (list) :  Liste des clusters de verbes vidée au maximum
	"""
	
	if type(families) != list :
		return erreur("final_clust : Erreur paramètre families incorrect", (None, None))
	if type(dico_tag) != dict :
		return erreur("final_clust : Erreur paramètre dico_tag incorrect", (None, None))
	if type(modelist) != list :
		return erreur("final_clust : Erreur paramètre modelist incorrect", (None, None))
	
	dico_clust = {}

	if modelist != [] and dico_tag != {}:
		#Recherche des modèles à l'aide du dictionnaire électronique
		dico_clust, families = pop_models(families, dico_tag, dico_clust, modelist)
		if dico_clust == None :
			return erreur("final_clust : Echec de la fonction pop_models", (None, None))
	size = len(families)
	similiseuil = 5
	
	#Assemblage des clusters finaux de families
	while similiseuil != 2 :
		for i in range(size) :
			if families[i] == 0 :
				continue
			for j in range(i+1, size) :
				if families[j] == 0 :
					continue
				if len(families[i]) + len(families[j]) >= 47 :
					continue
				similitude = p_similar(transcaracter(families[i][0]), transcaracter(families[j][0]))
				if similitude < similiseuil :
					break
				families[i] = families[i][:-1] + families[j][:-1] + [families[i][-1] + families[j][-1]] 
				families[j] = 0
		similiseuil -= 1
	
	#Recherche dans chaque cluster de la forme infinitive
	for words in families :
		flag = False
		if words == 0 or type(words) != list :
			continue
		for word in words :
			if type(word) != str :
				continue
			#On commence par les verbes en er et ir
			if word[-2:] in ["er", "ir", "ïr"] :
				dico_clust[word] = words
				flag = True
				words = 0
				break
		if flag == True :
			continue
		#On fini par les verbes en re
		for word in words :
			if type(word) != str :
				continue
			if word[-2:] in ["re"] :
				dico_clust[word] = words
				words = 0
				break
		
	return dico_clust, families

def pop_models(families, dico_tag, dico_clust, modelist) :
	"""
		Construction d'un dictionnaire dans lequel chaque verbe/nom modèle et ses formes fléchies,
		sont regroupés dans une liste commune dont la clé est la forme infinitive du verbe/nom modèle
	
		Args :
		families (list) : Familles de cluster construit via le corpus et l'algorithme de Gaussier
		dico_tag (dict) : Dictionnaire électronique où l'on va récupérer les verbes/noms modèles
		dico_clust (dict) : Dictionnaire qui va contenir les verbes/noms modèles et leurs formes fléchies
		modelist (list) : Liste de l'ensemble des verbes/noms modèles
	
		Returns :
		dico_clust(dict) : Le dictionnaire des clusters finaux contenant les modèles de verbes/noms
		families (list) : La liste des clusters dans laquel on a retiré les formes fléchies des verbes modèles identifiés
		En cas d'erreur la fonction retourne le tuple (None, None) 
	"""
	
	if type(families) != list :
		return erreur("pop_models : Le paramètre families est incorrect", (None, None)) 
	if type(dico_tag) != dict :
		return erreur("pop_models : Le paramètre dico_tag est incorrect", (None, None))
	if type(dico_clust) != dict :
		return erreur("pop_models : Le paramètre dico_clust est incorrect", (None, None))
	if type(modelist) != list :
		return erreur("pop_models : Le paramètre modelist est incorrect", (None, None))
	

	#Les modèles sont recherchés dans les formes fléchies des clusters formé par l'algo de Gaussier
	for i in range(len(families)) :
		if families[i] == 0 :
			continue
		for modele in modelist :
			if modele == '' :
				continue
			if families[i] == 0 :
				break
			#aieul ne serai pas dans le dico électronique à vérifier
			if modele == "aieul" :
				continue	
				
			for flex in dico_tag[modele] :
				#On recherche dans le dico électronique si la forme fléchie d'un cluster correspond à une forme fléchie d'un verbe modèle
				if families[i][0] == flex[0] :
					if modele not in dico_clust.keys() :
						dico_clust[modele] = families[i]
					else :
						dico_clust[modele] = dico_clust[modele][:-1] + families[i][:-1] + [dico_clust[modele][-1] + families[i][-1]] 
					families[i] = 0
					#Si c'est le cas on ajoute le cluster dans le dictionnaire avec en clé la forme infinitive du modèle correspondant et on supprime le cluster de la liste families
					break
					
	return dico_clust, families

def final_clustName(familiesName) :
	"""
	Réunifications des clusters comportant les noms. 
	
	Arg :
	familiesName (list) : liste des clusters à réunifier
	
	Return :
	dico_clustName (dict) : Dictionnaire des clusters réunifiés avec en clé la forme masculine (généralement) de chacun des clusters
	"""
	
	if type(familiesName) != list:
		return erreur("final_clustName : Erreur paramètre attendu incorrect : {}".format(familiesName), None)	
	#Recherche dans chaque cluster de la forme la plus courte généralement masculine et singulière
	dico_clustName = {}
	
	for words in familiesName :
		if words == 0:
			continue
		if type(words[0]) != str :
			continue
		vainq = words[0]
		for word in words :
			if type(word) == list :
				continue
			if len(word) < len(vainq) :
				vainq = word
		dico_clustName[vainq] = words
	
	return dico_clustName


def final_lemmeBrut(dico_models, modelist, lemmeList) :
	"""
		Cette fonction va prendre un dictionnaire de schémas des modèles construits dans le module mod.py. Les modèles ayant le même schéma de conjugaison vont être regroupés ensemble. Ensuite lemmeBrut est analysé après sa construction pour attribuer à chaque verbe/nom déjà présent le modèle adéquat.
	
		Args:
		dico_models (dict) : Un dictionnaire des schémas de conjugaison
		modelist (list) : Liste des verbes/noms modèles sélectionnés par le travaille de mod.py
		lemmeList(list) : Une version de lemmeBrut sous forme de liste
	
		Returns :
		lemmeList (list) : La version lemmeBrut avec la correction des modèles de verbes
		En cas d'erreur la fonction retourne le tuple None
	"""
	
	if type(dico_models) != dict :
		return erreur("final_lemmeBrut : Erreur paramètre dico_models incorrect {}".format(dico_models), None)
	if type(modelist) != list :
		return erreur("final_lemmeBrut : Erreur paramètre modelist incorrect {}".format(modelist), None)	
	if type(lemmeList) != list :
		return erreur("final_lemmeBrut : Erreur paramètre lemmeList incorrect {}".format(lemmeList), None)
	
	#Regroupement des modèles ayant le même schéma de conjugaison
	keylist = list(dico_models.keys())
	keylist.sort()
	modelist.sort()
	mod_final_term = {}
	
	for key in keylist :
		for key2 in modelist :
			if key2 not in keylist :
				return erreur("final_lemmeBrut : Erreur la clé {} n'est pas dans le dictionnaire des modèles", None)
			if dico_models[key] == dico_models[key2] :
				mod_final_term[key] = key2
	
	#pprint(mod_final_term)
	
	#Finalisation de lemmeBrut par l'attribution des modèles
	for line in lemmeList : 
		if line[2] not in mod_final_term.keys() :
			return erreur("final_lemmeBrut : Echec d'identification du modèle : {}".format(line), None)
		line[2] = mod_final_term[line[2]]
		
	#pprint(lemmeList)
	
	return lemmeList

	
def main_lem(familiesVerbs, familiesNames, saveTest = False) :
	lfamilies = [familiesVerbs, familiesNames]
	lemmeBrutVerbs = []
	lemmeBrutNames = []
	final_clustVerbs = {}
	final_clustNames = {}
	for lfamily in lfamilies :
		if lfamily is familiesVerbs :
			print("Traitement des familles de verbes\n")
			print("finalisation des clusters")
			dico_clust, family = final_clust(lfamily)
		else :
			print("\nTraitement des familles de noms\n")
			print("finalisation des clusters")
			dico_clust = final_clustName(lfamily)
		
		if dico_clust == None :
			exit("Echec de la finalisation des clusters")
		
		print("Optimisation des racines")
		dico_clust = stemmatise(dico_clust)
		if dico_clust == None :
			exit("Echec de l'optimisation des racines")
		
		print("Construction de lemmeBrut")
		lemmeList = build_lemmeList(dico_clust)
		if lemmeList == None :
			exit("Echec de la construction de lemmeBrut")
		
		if lfamily is familiesVerbs :
			lemmeBrutVerbs = lemmeList
		else :
			lemmeBrutNames = lemmeList

		if lfamily is familiesVerbs :
			final_clustVerbs = dico_clust
		else :
			final_clustNames = dico_clust
		
		if saveTest :
			print("Sauvegarde des fichiers pour les tests")
			if lfamily is familiesVerbs :
				write_pickle(dico_clust, "tests/final_clust.pickle")
				write_pickle(lemmeList, "tests/lemmeList.pickle")
			else :
				write_pickle(dico_clust, "tests/final_clustName.pickle")
				write_pickle(lemmeList, "tests/lemmeListName.pickle")
				
	return lemmeBrutVerbs, lemmeBrutNames, final_clustVerbs, final_clustNames	
		
if __name__ == '__main__':
	if "-t" in sys.argv :
		print("Récupération des verbes et des noms")
		LIMIT_VERBES = 4000
		LIMIT_NOMS = 4000 
		mots, verbes, noms, dictVerbs, dictNoms = main_html("https://fr.wikipedia.org/wiki/France")
		verbes.sort()
		noms.sort()
		print("\nFormation des clusters\n")
		gFamiliesVerbs, gFamiliesNames = main_clust(verbes, noms)
		
		print("\nConstruction de lemmeBrut\n")
		gLemmeBrutVerbs, gLemmeBrutNames, final_clust, final_clustNames = main_lem(gFamiliesVerbs, gFamiliesNames, True)
		
		print("lemmeBrut des verbes")
		#pprint(gLemmeBrutVerbs)
		
		print("lemmeBrut des noms")
		#pprint(gLemmeBrutNames)
	
	elif len(sys.argv) == 2:
		print("Récupération des verbes et des noms")
		mots, verbes, noms, dictVerbs, dictNoms = main_html(argv[1])
		verbes.sort()
		noms.sort()
		print("\nFormation des clusters\n")
		gFamiliesVerbs, gFamiliesNames = main_clust(verbes, noms)
		print("\nConstruction de lemmeBrut\n")
		gLemmeBrutVerbs, gLemmeBrutNames, final_clust, final_clustNames = main_lem(gFamiliesVerbs, gFamiliesNames)
		
		print("lemmeBrut des verbes")
		#pprint(gLemmeBrutVerbs)
		print("\n----------\n")
		print("lemmeBrut des noms")
		#pprint(gLemmeBrutNames)
		
	else:
		exit("Un argument, l'url de la page à crawler.")
