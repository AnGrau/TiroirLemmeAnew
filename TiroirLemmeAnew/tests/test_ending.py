#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
import os

try:
    from build_tiroir import ending
except ImportError :
    from TiroirLemmeAnew.build_tiroir import ending

class TestFonction_ending(unittest.TestCase):

    def setUp(self):
        self.stems = ['achèt', 'achet']
        self.stems.sort(key=len, reverse=True)

    def tearDown(self):
        os.system("rm -rf __pycache__")
        os.system("rm -rf tests/__pycache__")

    def test_ending_no_word(self):
    	self.assertEqual(ending('', self.stems), 0)
    
    def test_ending_not_in_word(self):
        self.assertEqual(ending('totor', self.stems), 0)
    
    def test_ending_result(self):
        self.assertEqual(ending('achètera', self.stems)[0], 'era')

if __name__ == '__main__':
    unittest.main()
