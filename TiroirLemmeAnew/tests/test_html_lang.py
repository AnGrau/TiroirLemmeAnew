#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
import os

import warnings

try:
    from html_lang import *
except ImportError :
    from TiroirLemmeAnew.html_lang import *

class Test_html_lang(unittest.TestCase):

    def setUp(self):
        self.url_https = "//fr.wikipedia.org/wiki/France"
        self.url_all = "/wiki/France"
        self.url = "https://fr.wikipedia.org/wiki/France"
        self.html = lire_url(self.url)
        self.text = extraire_texte(self.html)

    def tearDown(self):
        os.system("rm -rf __pycache__")
        os.system("rm -rf tests/__pycache__")

    def test_construit_https(self):
        warnings.simplefilter("ignore")
        result = construit(self.url_https)
        self.assertEqual(result, self.url)

    def test_construit_all(self):
        warnings.simplefilter("ignore")
        result = construit(self.url_all)
        self.assertEqual(result, self.url)

    def test_construit_rien(self):
        warnings.simplefilter("ignore")
        result = construit(self.url)
        self.assertEqual(result, self.url)

    def test_lire_url(self):
        warnings.simplefilter("ignore")
        result = lire_url(self.url)
        self.assertIsNotNone(result)

    def test_extraire_texte(self):
        warnings.simplefilter("ignore")
        text = extraire_texte(self.html)
        self.assertIsNotNone(text)
        self.assertIsInstance(text[0], str)

    def test_tolist(self):
        warnings.simplefilter("ignore")
        liste_mots = tolist(self.text)
        self.assertIsNotNone(liste_mots)
        for word in liste_mots:
            self.assertIsInstance(word[0], str)
            self.assertNotIn(' ', word)

    def test_main(self):
        warnings.simplefilter("ignore")
        mots, verbes, noms, a, b = main_html(self.url)
        self.assertTrue(len(mots) > 4000)
        self.assertTrue(len(verbes) > 4000)
        self.assertTrue(len(noms) > 4000)
        
        

if __name__ == '__main__':
    unittest.main()
