#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
import os

try:
    from build_tiroir import constructNames
except ImportError :
    from TiroirLemmeAnew.build_tiroir import constructNames

class TestFonction_constructNames(unittest.TestCase):

    def setUp(self):
        self.cluster = {'agneau': ['agneau', 'agneaux', 'agnelle', 'agnelles', ['agneau', 'agnelle']],\
        'ail': ['ail', 'ails', ['ail']], 'aval': ['aval', 'avals', ['aval']], \
        'avocat': ['avocat', 'avocats', 'avocate', 'avocates', ['avocat']],\
        'bal': ['bal', 'bals', ['bal']], 'berceau': ['berceau', 'berceaux', ['berceau']],\
        'berger': ['berger', 'bergers', 'bergère', 'bergères', ['berger', 'bergèr']]}

        self.data = [['agneau', 'masculine', 1], ['agneaux', 'masculine', 2],\
        ['agnelle', 'feminine', 1], ['agnelles', 'feminine', 2], ['ail', 'masculine', 1],\
        ['ails', 'masculine', 2],['aval', 'masculine', 1], ['avals', 'masculine', 2],\
        ['avocat', 'masculine', 1], ['avocats', 'masculine', 2], ['avocate', 'feminine', 1],\
        ['avocates', 'feminine', 2], ['bal', 'masculine', 1], ['bals', 'masculine', 2],\
        ['berceau', 'masculine', 1], ['berceaux', 'masculine', 2],\
        ['berger', 'masculine', 1], ['bergers', 'masculine', 2], ['bergère', 'feminine', 1],\
        ['bergères', 'feminine', 2]]

        self.gender = ['masculine', 'feminine']
    
    def tearDown(self):
        os.system("rm -rf __pycache__")
        os.system("rm -rf tests/__pycache__")

    def test_constructNames_dictionary(self):
        sorted_suff = constructNames(self.data, self.cluster)[0]
        to_compare = {'avocat': {'feminine': ['e', 'es'], 'masculine': ['', 's']},\
        'berceau': {'feminine': [0, 0], 'masculine': ['', 'x']},\
        'agneau': {'feminine': ['', 's'], 'masculine': ['', 'x']},\
        'bal': {'feminine': [0, 0], 'masculine': ['', 's']},\
        'aval': {'feminine': [0, 0], 'masculine': ['', 's']},\
        'ail': {'feminine': [0, 0], 'masculine': ['', 's']},\
        'berger': {'feminine': ['e', 'es'], 'masculine': ['', 's']}}
        self.assertEqual(sorted_suff, to_compare)

    def test_constructNames_sub_dictionary_keys(self):
        sorted_suff = constructNames(self.data, self.cluster)[0]
        to_test = []
        for elt in sorted_suff.keys():
            tmp = sorted_suff[elt]
            to_test.append(list(tmp.keys()))
        for keys_list in to_test:
            self.assertEqual(sorted(keys_list), sorted(self.gender))

if __name__ == '__main__':
    unittest.main()
