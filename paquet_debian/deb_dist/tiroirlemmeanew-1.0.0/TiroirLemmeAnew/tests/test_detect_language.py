#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
import os

import warnings

try:
    from nltk_stopwords import *
except ImportError :
    from TiroirLemmeAnew.nltk_stopwords import *

import warnings

class TestFonction_detect_language(unittest.TestCase):

    def setUp(self):
        self.textRien = ""
        self.textFr = """La Baule-Escoublac est une commune de l'Ouest de la France, dans le département de la Loire-Atlantique, en région Pays de la Loire. Située sur le littoral atlantique, elle fait partie de la Côte d'Amour, entre Le Pouliguen et Pornichet."""
        self.textEn = "In the old days, ants and cicadas were friends. They were very different. The ants were hardworking, but the cicadas were lazy."
        self.textIt = "E adesso andate via, Voglio restare sola, Con la malinconia, Volare nel suo cielo"
        self.textEs = "La lectura, la lectura esta hoy para mi una palabra poco conocido."
        self.lang = set(['spanish', 'english', 'dutch', 'romanian', 'portuguese', 'danish', 'norwegian', 'kazakh', 'french', 'italian', 'hungarian', 'arabic', 'finnish', 'swedish', 'turkish', 'russian', 'german'])

    def tearDown(self):
        os.system("rm -rf __pycache__")
        os.system("rm -rf tests/__pycache__")

    def test_detect_language_rien(self):
        warnings.simplefilter("ignore")
        lan = detect_language(self.textRien)
        self.assertEqual(lan, None)

    def test_detect_language_fr(self):
        warnings.simplefilter("ignore")
        lan = detect_language(self.textFr)
        self.assertEqual(lan, 'french')

    def test_detect_language_en(self):
        warnings.simplefilter("ignore")
        lan = detect_language(self.textEn)
        self.assertEqual(lan, 'english')

    def test_detect_language_it(self):
        warnings.simplefilter("ignore")
        lan = detect_language(self.textIt)
        self.assertEqual(lan, 'italian')

    def test_detect_language_es(self):
        warnings.simplefilter("ignore")
        lan = detect_language(self.textEs)
        self.assertEqual(lan, 'spanish')

if __name__ == '__main__':
    unittest.main()
