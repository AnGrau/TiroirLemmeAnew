#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
import sys
import os
sys.path.append(os.getenv("PWD")[:-len("/tests")])

try:
	from data_fetch import read_pickle
except ImportError :
	from TiroirLemmeAnew.data_fetch import read_pickle

try:
	from database import *
except ImportError :
	from TiroirLemmeAnew.database import *

from pprint import pprint

class Test_database(unittest.TestCase) :
	
	nbTest = 0
	db = None
	
	def setUp(self) :
		path = os.getcwd()
		if "TiroirLemmeAnew/TiroirLemmeAnew/tests" in path :
			path =  ""
		elif "TiroirLemmeAnew/TiroirLemmeAnew" in path :
			path = "tests/"
		else :
			path = "TiroirLemmeAnew/tests"
		if Test_database.nbTest == 0 :
			Test_database.db = connecDb("tluser", "tldatabase", "tluser")	
		Test_database.nbTest += 1	
		self.lemmeBrutVerbs = read_pickle(path + "final_lemmeBrutVerbs.pickle")
		self.lemmeBrutNames = read_pickle(path + "final_lemmeBrutNames.pickle")
		self.to_save = read_pickle(path + "to_save.pickle")
		self.names_to_save = read_pickle(path + "names_to_save.pickle")
		self.modeles_verbes = read_pickle(path + "modeles_verbes_final.pickle")
		self.modeles_noms = read_pickle(path +"modeles_noms_final.pickle")
		self.dbUser = "postgres"
		self.dbName = "TLDatabase"
		self.tableTest = [["Boris", "Merminod", 20], ["Anthony", "Grau", 21], ["Sybille", "Louvel", 19]]
		self.attributsTest = [["Prénom", "str"], ["Nom", "str"], ["Age", "int"]] 
		self.tableTest2 = [["Boris", "Merminod", [20]], ["Anthony", "Grau", [21]], ["Sybille", "Louvel", [19]]]
		self.attributsTest2 = [ [["Index", "int"],["Nom_prenom", "str"]], [["Index", "int"],["Age", "int"]] ]
		
	
	def tearDown(self):
		if Test_database.nbTest >= 3 :
			Test_database.db.close()
		os.system("rm -rf __pycache__")
		os.system("rm -rf tests/__pycache__")

	"""
	def test_connecDb(self) :
		test_db = connecDb(self.dbUser, self.dbName)
		self.assertNotEqual(test_db, None)
		test_db.close()
		
		test_db = connecDb(None, self.dbName)
		self.assertEqual(test_db, None)
		
		test_db = connecDb(self.dbUser, None)
		self.assertEqual(test_db, None)
		
		test_db = connecDb("testUser", self.dbName)
		self.assertEqual(test_db, None)
		
		test_db = connecDb(self.dbUser, "testdb")
		self.assertEqual(test_db, None)
	"""
	def test_workTable(self) :
		
		#Tests de createTable
		self.assertEqual(createTable("tableTest", self.attributsTest), True)
		self.assertEqual(createTable(None, self.attributsTest), False)
		self.assertEqual(createTable("tableTest", None), False)
		self.assertEqual(createTable("tableTest", ["test", "test", 20]), False)
		
		#Tests de insertTable
		for line in self.tableTest :
			self.assertEqual(insertTable("tableTest", line), True)
		self.assertEqual(insertTable(None, line), False)
		self.assertEqual(insertTable("tableTest", None), False)
		self.assertEqual(insertTable("tableTest", [20, 20, "test"]), False)
		
		#Tests de readTable
		self.assertEqual(readTable("tableTest"), self.tableTest)
		self.assertEqual(readTable(None), None)
		self.assertEqual(readTable("fausseTable"), None)
		
		#Tests de dropTable
		self.assertEqual(dropTable("tableTest"), True)
		self.assertEqual(dropTable(None), False)
		self.assertEqual(dropTable("fausseTable"), True)
		
	def test_results_db(self) :
		
		#Tests resultsToDb
		#Sauvegarde des structure pour les verbes
		results = resultsToDb("lemmeBrut_test", self.lemmeBrutVerbs,  [["racine", "str"], ["lemme", "str"], ["modele", "str"], ["NoRacine", "int"]])
		self.assertEqual(results, True)
		results = resultsToDb("tiroirBrut_test", self.to_save,  [["terminaison", "str"], ["temps", "str"], ["NoTiroir", "int"], ["NoPersonne", "int"]])
		self.assertEqual(results, True) 
		
		#Sauvegarde des structures pour les noms
		results = resultsToDb("names_lemmeBrut_test", self.lemmeBrutNames,  [["racine", "str"], ["lemme", "str"], ["modele", "str"], ["NoRacine", "int"]])
		self.assertEqual(results, True)
		results = resultsToDb("names_tiroirBrut_test", self.names_to_save,  [["terminaison", "str"], ["genre", "str"], ["NoTiroir", "int"], ["NoForme", "int"]])
		self.assertEqual(results, True)
		
		#Test insertion dans une table
		results = resultsToDb("table_test", self.tableTest,  self.attributsTest)
		self.assertEqual(results, True)
		results = resultsToDb("table_test", self.tableTest)
		self.assertEqual(results, True)
		
		
		#Echecs de resultsToDb
		results = resultsToDb(None, self.lemmeBrutVerbs,  [["racine", "str"], ["lemme", "str"], ["modele", "str"], ["NoRacine", "int"]])
		self.assertEqual(results, False)
		results = resultsToDb("lemmeBrut_test", None,  [["racine", "str"], ["lemme", "str"], ["modele", "str"], ["NoRacine", "int"]])
		self.assertEqual(results, False)
		results = resultsToDb("lemmeBrut_test", self.lemmeBrutVerbs,  None)
		self.assertEqual(results, False)
		results = resultsToDb("fausse_table", self.tableTest)
		self.assertEqual(results, False)
		
		#Test dbToResults
		#Lecture des structures pour les verbes
		results = dbToResults("lemmeBrut_test")
		self.assertEqual(self.lemmeBrutVerbs, results)
		results = dbToResults("tiroirBrut_test", True)
		self.assertEqual(self.to_save, results)
		
		#Lecture des structures pour les noms
		results = dbToResults("names_lemmeBrut_test")
		self.assertEqual(self.lemmeBrutNames, results)
		results = dbToResults("names_tiroirBrut_test", True)
		self.assertEqual(self.names_to_save, results) 
		
		#Echecs de dbToResults
		results = dbToResults(None)
		self.assertEqual(results, None)
		results = dbToResults("table_test", None) #Ne doit pas échouer
		self.assertNotEqual(results, None)
		results = dbToResults("fausse_table")
		self.assertEqual(results, None)
		
		#Suppression des tables de test
		dropTable("lemmeBrut_test")
		dropTable("tiroirBrut_test")
		dropTable("names_lemmeBrut_test")
		dropTable("names_tiroirBrut_test")
		dropTable("table_test")
		
	def test_models_db(self) :
		
		#Tests modelsToDb
		#Création de la table et insertion des modèles dans une table
		results = modelsToDb("modeles_verbes_test", self.modeles_verbes, [["key", "str"], ["shiftdown", "int"], ["shiftup", "int"], ["value", "str"]])
		self.assertEqual(results, True)
		
		results = modelsToDb("modeles_noms_test", self.modeles_noms, [["key", "str"], ["shiftdown", "int"], ["shiftup", "int"], ["value", "str"]])
		self.assertEqual(results, True)
		
		#Tests dbToModels
		#Récupération des modèles
		test_models = dbToModels("modeles_verbes_test")
		self.assertEqual(test_models, self.modeles_verbes)
		
		test_models = dbToModels("modeles_noms_test")
		self.assertEqual(test_models, self.modeles_noms)
		
		#Echec de modelsToDb
		results = modelsToDb(None, self.modeles_verbes, [["key", "str"], ["shiftdown", "int"], ["shiftup", "int"], ["value", "str"]])
		self.assertEqual(results, False)
		results = modelsToDb("modeles_noms_test", None, [["key", "str"], ["shiftdown", "int"], ["shiftup", "int"], ["value", "str"]])
		self.assertEqual(results, False)
		results = modelsToDb("modeles_noms_test", self.modeles_noms, None)
		self.assertEqual(results, False)
		
		
		results = modelsToDb("modeles_verbes_test", [], [["key", "str"], ["shiftdown", "int"], ["shiftup", "int"], ["value", "str"]])
		self.assertEqual(results, False)
		results = modelsToDb("modeles_verbes_test", ["test", 123, ["test", 123], []], [["key", "str"], ["shiftdown", "int"], ["shiftup", "int"], ["value", "str"]])
		self.assertEqual(results, False)
		
		#Echec de la fonction dbToModels
		test_models = dbToModels(None)
		self.assertEqual(test_models, None)
		
		#Suppression des tables
		dropTable("modeles_verbes_test")
		dropTable("modeles_noms_test")
		
		
if __name__ == '__main__':
	unittest.main()	
