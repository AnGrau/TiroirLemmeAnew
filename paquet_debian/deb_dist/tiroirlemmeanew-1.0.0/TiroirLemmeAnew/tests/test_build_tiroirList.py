#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
import os

try:
    from build_tiroir import build_tiroirList
except ImportError :
    from TiroirLemmeAnew.build_tiroir import build_tiroirList

class TestFonction_build_tiroirList(unittest.TestCase):

    def setUp(self):
        self.tenses = ['infinitif', 'indpres', 'indimpar', 'indfut', 'simple', 'subjpres', 'subjimpar', 'cond']
        self.sorted_suff = {'acheter': {'indfut': ['erai', 'eras', 'era', 'erons', 'erez', 'eront'], 'infinitif': ['er'], 'subjimpar': ['asse', 'asses', 'ât', 'assions', 'assiez', 'assent'], 'subjpres': ['e', 'es', 'e', 'ions', 'iez', 'ent'], 'indimpar': ['ais', 'ais', 'ait', 'ions', 'iez', 'aient'], 'simple': ['ai', 'as', 'a', 'âmes', 'âtes', 'èrent'], 'cond': [0, 'erais', 'erait', 'erions', 'eriez', 'eraient'], 'indpres': ['e', 'es', 'e', 'ons', 'ez', 'ent']}, 'accroître': {'indfut': ['oîtrai', 'oîtras', 'oîtra', 'oîtrons', 'oîtrez', 'oîtront'], 'infinitif': ['oître'], 'subjimpar': ['usse', 'usses', 'ût', 'ussions', 'ussiez', 'ussent'], 'subjpres': ['oisse', 'oisses', 'oisse', 'oissions', 'oissiez', 'oissent'], 'indimpar': ['oissais', 'oissais', 'oissait', 'oissions', 'oissiez', 'oissaient'], 'simple': ['us', 'us', 'ut', 'ûmes', 'ûtes', 'urent'], 'cond': [0, 'oîtrais', 'oîtrait', 'oîtrions', 'oîtriez', 'oîtraient'], 'indpres': ['ois', 'ois', 'oît', 'oissons', 'oissez', 'oissent']}}

    def tearDown(self):
        os.system("rm -rf __pycache__")
        os.system("rm -rf tests/__pycache__")

    def test_to_print(self):
        mem = build_tiroirList(self.sorted_suff)
        to_compare_1 = [('oîtrais-', 'cond', 1, 2), ('oîtrait-', 'cond', 1, 3), ('oîtrions-', 'cond', 1, 4), ('oîtriez-', 'cond', 1, 5), ('oîtraient-', 'cond', 1, 6), ('erais-', 'cond', 2, 2), ('erait-', 'cond', 2, 3), ('erions-', 'cond', 2, 4), ('eriez-', 'cond', 2, 5), ('eraient-', 'cond', 2, 6), ('oîtrai-', 'indfut', 1, 1), ('oîtras-', 'indfut', 1, 2), ('oîtra-', 'indfut', 1, 3), ('oîtrons-', 'indfut', 1, 4), ('oîtrez-', 'indfut', 1, 5), ('oîtront-', 'indfut', 1, 6), ('erai-', 'indfut', 2, 1), ('eras-', 'indfut', 2, 2), ('era-', 'indfut', 2, 3), ('erons-', 'indfut', 2, 4), ('erez-', 'indfut', 2, 5), ('eront-', 'indfut', 2, 6), ('oissais-', 'indimpar', 1, 1), ('oissais-', 'indimpar', 1, 2), ('oissait-', 'indimpar', 1, 3), ('oissions-', 'indimpar', 1, 4), ('oissiez-', 'indimpar', 1, 5), ('oissaient-', 'indimpar', 1, 6), ('ais-', 'indimpar', 2, 1), ('ais-', 'indimpar', 2, 2), ('ait-', 'indimpar', 2, 3), ('ions-', 'indimpar', 2, 4), ('iez-', 'indimpar', 2, 5), ('aient-', 'indimpar', 2, 6), ('ois-', 'indpres', 1, 1), ('ois-', 'indpres', 1, 2), ('oît-', 'indpres', 1, 3), ('oissons-', 'indpres', 1, 4), ('oissez-', 'indpres', 1, 5), ('oissent-', 'indpres', 1, 6), ('e-', 'indpres', 2, 1), ('es-', 'indpres', 2, 2), ('e-', 'indpres', 2, 3), ('ons-', 'indpres', 2, 4), ('ez-', 'indpres', 2, 5), ('ent-', 'indpres', 2, 6), ('us-', 'simple', 1, 1), ('us-', 'simple', 1, 2), ('ut-', 'simple', 1, 3), ('ûmes-', 'simple', 1, 4), ('ûtes-', 'simple', 1, 5), ('urent-', 'simple', 1, 6), ('ai-', 'simple', 2, 1), ('as-', 'simple', 2, 2), ('a-', 'simple', 2, 3), ('âmes-', 'simple', 2, 4), ('âtes-', 'simple', 2, 5), ('èrent-', 'simple', 2, 6), ('usse-', 'subjimpar', 1, 1), ('usses-', 'subjimpar', 1, 2), ('ût-', 'subjimpar', 1, 3), ('ussions-', 'subjimpar', 1, 4), ('ussiez-', 'subjimpar', 1, 5), ('ussent-', 'subjimpar', 1, 6), ('asse-', 'subjimpar', 2, 1), ('asses-', 'subjimpar', 2, 2), ('ât-', 'subjimpar', 2, 3), ('assions-', 'subjimpar', 2, 4), ('assiez-', 'subjimpar', 2, 5), ('assent-', 'subjimpar', 2, 6), ('oisse-', 'subjpres', 1, 1), ('oisses-', 'subjpres', 1, 2), ('oisse-', 'subjpres', 1, 3), ('oissions-', 'subjpres', 1, 4), ('oissiez-', 'subjpres', 1, 5), ('oissent-', 'subjpres', 1, 6), ('e-', 'subjpres', 2, 1), ('es-', 'subjpres', 2, 2), ('e-', 'subjpres', 2, 3), ('ions-', 'subjpres', 2, 4), ('iez-', 'subjpres', 2, 5), ('ent-', 'subjpres', 2, 6)]
        to_compare_2 = [('erais-', 'cond', 1, 2), ('erait-', 'cond', 1, 3), ('erions-', 'cond', 1, 4), ('eriez-', 'cond', 1, 5), ('eraient-', 'cond', 1, 6), ('oîtrais-', 'cond', 2, 2), ('oîtrait-', 'cond', 2, 3), ('oîtrions-', 'cond', 2, 4), ('oîtriez-', 'cond', 2, 5), ('oîtraient-', 'cond', 2, 6), ('erai-', 'indfut', 1, 1), ('eras-', 'indfut', 1, 2), ('era-', 'indfut', 1, 3), ('erons-', 'indfut', 1, 4), ('erez-', 'indfut', 1, 5), ('eront-', 'indfut', 1, 6), ('oîtrai-', 'indfut', 2, 1), ('oîtras-', 'indfut', 2, 2), ('oîtra-', 'indfut', 2, 3), ('oîtrons-', 'indfut', 2, 4), ('oîtrez-', 'indfut', 2, 5), ('oîtront-', 'indfut', 2, 6), ('ais-', 'indimpar', 1, 1), ('ais-', 'indimpar', 1, 2), ('ait-', 'indimpar', 1, 3), ('ions-', 'indimpar', 1, 4), ('iez-', 'indimpar', 1, 5), ('aient-', 'indimpar', 1, 6), ('oissais-', 'indimpar', 2, 1), ('oissais-', 'indimpar', 2, 2), ('oissait-', 'indimpar', 2, 3), ('oissions-', 'indimpar', 2, 4), ('oissiez-', 'indimpar', 2, 5), ('oissaient-', 'indimpar', 2, 6), ('e-', 'indpres', 1, 1), ('es-', 'indpres', 1, 2), ('e-', 'indpres', 1, 3), ('ons-', 'indpres', 1, 4), ('ez-', 'indpres', 1, 5), ('ent-', 'indpres', 1, 6), ('ois-', 'indpres', 2, 1), ('ois-', 'indpres', 2, 2), ('oît-', 'indpres', 2, 3), ('oissons-', 'indpres', 2, 4), ('oissez-', 'indpres', 2, 5), ('oissent-', 'indpres', 2, 6), ('ai-', 'simple', 1, 1), ('as-', 'simple', 1, 2), ('a-', 'simple', 1, 3), ('âmes-', 'simple', 1, 4), ('âtes-', 'simple', 1, 5), ('èrent-', 'simple', 1, 6), ('us-', 'simple', 2, 1), ('us-', 'simple', 2, 2), ('ut-', 'simple', 2, 3), ('ûmes-', 'simple', 2, 4), ('ûtes-', 'simple', 2, 5), ('urent-', 'simple', 2, 6), ('asse-', 'subjimpar', 1, 1), ('asses-', 'subjimpar', 1, 2), ('ât-', 'subjimpar', 1, 3), ('assions-', 'subjimpar', 1, 4), ('assiez-', 'subjimpar', 1, 5), ('assent-', 'subjimpar', 1, 6), ('usse-', 'subjimpar', 2, 1), ('usses-', 'subjimpar', 2, 2), ('ût-', 'subjimpar', 2, 3), ('ussions-', 'subjimpar', 2, 4), ('ussiez-', 'subjimpar', 2, 5), ('ussent-', 'subjimpar', 2, 6), ('e-', 'subjpres', 1, 1), ('es-', 'subjpres', 1, 2), ('e-', 'subjpres', 1, 3), ('ions-', 'subjpres', 1, 4), ('iez-', 'subjpres', 1, 5), ('ent-', 'subjpres', 1, 6), ('oisse-', 'subjpres', 2, 1), ('oisses-', 'subjpres', 2, 2), ('oisse-', 'subjpres', 2, 3), ('oissions-', 'subjpres', 2, 4), ('oissiez-', 'subjpres', 2, 5), ('oissent-', 'subjpres', 2, 6)]
        to_comp = [to_compare_1, to_compare_2]
        self.assertIn(mem, to_comp)
    
    def test_tenses(self):
        mem = build_tiroirList(self.sorted_suff)
        result_tenses = []
        for elt in mem:
            if elt[1] not in result_tenses:
                result_tenses.append(elt[1])
        self.assertEqual(self.tenses[1:].sort(), result_tenses.sort())
        
    def test_person(self):
        mem = build_tiroirList(self.sorted_suff)
        person = [1, 2, 3, 4, 5, 6]
        for elt in mem:
            self.assertIn(elt[3], person)

if __name__ == '__main__':
    unittest.main()
