#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
import os
import sys
sys.path.append(os.getenv("PWD")[:-len("/tests")])

try:
	from data_fetch import *
except ImportError :
	from TiroirLemmeAnew.data_fetch import *

class Test_data_fetch(unittest.TestCase) :
	
	def setUp(self):
		self.path = os.getcwd()
		pathDico = ""
		if "TiroirLemmeAnew/TiroirLemmeAnew/tests" in self.path :
			self.path =  ""
			pathDico = os.getcwd() - "/tests"
		elif "TiroirLemmeAnew/TiroirLemmeAnew" in self.path :
			self.path = "tests/"
			pathDico = ""
		else :
			self.path = "TiroirLemmeAnew/tests"
			pathDico = "TiroirLemmeAnew"
		self.filedico = pathDico + "Morphalou3.1_LMF.xml"
		self.dico_tag_verbs = read_pickle(self.path + "dico_tag_verbs.pickle")
		self.dico_tag_names = read_pickle(self.path + "dico_tag_names.pickle")
		self.lverbs = [['avoir', 'infinitif', 0], ['avoir', 'infinitif', 0], ['a', 'indpres', 3], ['ai', 'indpres', 1], ['aie', 'subjpres', 1], ['aient', 'subjpres', 6], ['aies', 'subjpres', 2], ['ait', 'subjpres', 3], ['as', 'indpres', 2], ['aura', 'indfut', 3], ['aurai', 'indfut', 1], ['auraient', 'cond', 6], ['aurais', 'cond', 2], ['aurait', 'cond', 3], ['auras', 'indfut', 2], ['aurez', 'indfut', 5], ['auriez', 'cond', 5], ['aurions', 'cond', 4], ['aurons', 'indfut', 4], ['auront', 'indfut', 6], ['avaient', 'indimpar', 6], ['avais', 'indimpar', 2], ['avait', 'indimpar', 3], ['avez', 'indpres', 5], ['aviez', 'indimpar', 5], ['avions', 'indimpar', 4], ['avons', 'indpres', 4], ['ayez', 'subjpres', 5], ['ayons', 'subjpres', 4], ['eurent', 'simple', 6], ['eusse', 'subjimpar', 1], ['eussent', 'subjimpar', 6], ['eusses', 'subjimpar', 2], ['eussiez', 'subjimpar', 5], ['eussions', 'subjimpar', 4], ['eut', 'simple', 3], ['eûmes', 'simple', 4], ['eût', 'subjimpar', 3], ['eûtes', 'simple', 5], ['ont', 'indpres', 6], ['eus', 'simple', 2], ['eussé', 'subjimpar', 1], ['avais', 'indimpar', 1], ['eus', 'simple', 1], ['aurais', 'cond', 1]]
		self.lnames = [['avoir', 'masculine', 1], ['avoirs', 'masculine', 2], ['faim', 'feminine', 1], ['faims', 'feminine', 2]]
		self.wordlist = "avoir faim".split(" ")
		#self.wordlist = read_pickle("tests/wordlist.pickle")
	
	def test_build_tags(self) :
		dico_test_verbs, dico_test_names = build_tags(self.filedico)
		
		for key in dico_test_verbs.keys() :
			self.assertEqual(dico_test_verbs[key], self.dico_tag_verbs[key])
		
		for key in dico_test_names.keys() :
			self.assertEqual(dico_test_names[key], self.dico_tag_names[key])
		
		dico_test_verbs, dico_test_names = build_tags(self.path + "errorTest.xml")
		self.assertEqual(dico_test_verbs, {})
		self.assertEqual(dico_test_names, {})
		
		dico_test_verbs, dico_test_names = build_tags("bidon.xml")
		self.assertEqual(dico_test_verbs, None)
		self.assertEqual(dico_test_names, None)
		
		dico_test_verbs, dico_test_names = build_tags("bidon")
		self.assertEqual(dico_test_verbs, None)
		self.assertEqual(dico_test_names, None)
		
		dico_test_verbs, dico_test_names = build_tags(None)
		self.assertEqual(dico_test_verbs, None)
		self.assertEqual(dico_test_names, None)
	
	
	def test_word_tags(self) :
		global lstoplist
		
		#Fonctionnement normal de la fonction
		lverbsTest = []
		lnamesTest = []
		lverbsTest, lnamesTest = word_tags(self.dico_tag_verbs, self.dico_tag_names, lverbsTest, lnamesTest, self.wordlist, True, True, True)
		self.assertEqual(lverbsTest, self.lverbs)
		self.assertEqual(lnamesTest, self.lnames)
	
		lverbsTest = []
		lnamesTest = []
		lverbsTest, lnamesTest = word_tags(self.dico_tag_verbs, self.dico_tag_names, lverbsTest, lnamesTest, self.wordlist, False, True, True)
		self.assertEqual(lverbsTest, self.lverbs)
		self.assertEqual(lnamesTest, [])
		
		lverbsTest = []
		lnamesTest = []
		lverbsTest, lnamesTest = word_tags(self.dico_tag_verbs, self.dico_tag_names, lverbsTest, lnamesTest, self.wordlist, True, False, True)
		self.assertEqual(lverbsTest, [])
		self.assertEqual(lnamesTest, self.lnames)
		
		#Echecs de la fonction
		lverbsTest, lnamesTest = word_tags(None, self.dico_tag_names, lverbsTest, lnamesTest, self.wordlist, True, False, True)
		self.assertEqual(lverbsTest, None)
		self.assertEqual(lnamesTest, None)
		
		lverbsTest, lnamesTest = word_tags(self.dico_tag_verbs, None, lverbsTest, lnamesTest, self.wordlist, True, False, True)
		self.assertEqual(lverbsTest, None)
		self.assertEqual(lnamesTest, None)
		
		lverbsTest, lnamesTest = word_tags(self.dico_tag_verbs, self.dico_tag_names, None, lnamesTest, self.wordlist, True, False, True)
		self.assertEqual(lverbsTest, None)
		self.assertEqual(lnamesTest, None)

		lverbsTest, lnamesTest = word_tags(self.dico_tag_verbs, self.dico_tag_names, lverbsTest, None, self.wordlist, True, False, True)
		self.assertEqual(lverbsTest, None)
		self.assertEqual(lnamesTest, None)

		lverbsTest, lnamesTest = word_tags(self.dico_tag_verbs, self.dico_tag_names, lverbsTest, lnamesTest, None, True, False, True)
		self.assertEqual(lverbsTest, None)
		self.assertEqual(lnamesTest, None)
		
		lverbsTest, lnamesTest = word_tags(self.dico_tag_verbs, self.dico_tag_names, lverbsTest, lnamesTest, self.wordlist, None, False, True)
		self.assertEqual(lverbsTest, None)
		self.assertEqual(lnamesTest, None)
		
		lverbsTest, lnamesTest = word_tags(self.dico_tag_verbs, self.dico_tag_names, lverbsTest, lnamesTest, self.wordlist, True, None, True)
		self.assertEqual(lverbsTest, None)
		self.assertEqual(lnamesTest, None)
		
		lverbsTest, lnamesTest = word_tags({}, self.dico_tag_names, lverbsTest, lnamesTest, self.wordlist, True, False, True)
		self.assertEqual(lverbsTest, None)
		self.assertEqual(lnamesTest, None)
		
		lverbsTest, lnamesTest = word_tags(self.dico_tag_verbs, {}, lverbsTest, lnamesTest, self.wordlist, True, False, True)
		self.assertEqual(lverbsTest, None)
		self.assertEqual(lnamesTest, None)
			
		
	def test_locateFlex(self) :
		retour = None
		
		retour = locateFlex([None])
		self.assertEqual(retour, ("","",""))
		
		#Traitement des participes présent et passé
		retour = locateFlex(["testé", "participle", "present", "test"])
		self.assertEqual(retour, ("","",""))
		retour = locateFlex(["testons", "singular", "participle", "test"])
		self.assertEqual(retour, ("","",""))
		
		#Traitement de l'indicatif présent
		retour = locateFlex(["test", "singular", "indicative", "firstPerson", "present"])
		self.assertEqual(retour, ("indpres",1,0))
		retour = locateFlex(["test", "singular", "indicative", "secondPerson", "present"])
		self.assertEqual(retour, ("indpres",2,0))
		retour = locateFlex(["test", "singular", "indicative", "thirdPerson", "present"])
		self.assertEqual(retour, ("indpres",3,0))
		retour = locateFlex(["test", "plural", "indicative", "firstPerson", "present"])
		self.assertEqual(retour, ("indpres",1,3))
		retour = locateFlex(["test", "plural", "indicative", "secondPerson", "present"])
		self.assertEqual(retour, ("indpres",2,3))
		retour = locateFlex(["test", "plural", "indicative", "thirdPerson", "present"])
		self.assertEqual(retour, ("indpres",3,3))
		
		#Traitement de l'indicatif future
		retour = locateFlex(["test", "singular", "indicative", "firstPerson", "future"])
		self.assertEqual(retour, ("indfut",1,0))
		retour = locateFlex(["test", "singular", "indicative", "secondPerson", "future"])
		self.assertEqual(retour, ("indfut",2,0))
		retour = locateFlex(["test", "singular", "indicative", "thirdPerson", "future"])
		self.assertEqual(retour, ("indfut",3,0))
		retour = locateFlex(["test", "plural", "indicative", "firstPerson", "future"])
		self.assertEqual(retour, ("indfut",1,3))
		retour = locateFlex(["test", "plural", "indicative", "secondPerson", "future"])
		self.assertEqual(retour, ("indfut",2,3))
		retour = locateFlex(["test", "plural", "indicative", "thirdPerson", "future"])
		self.assertEqual(retour, ("indfut",3,3))
		
		#Traitement de l'indicatif imparfait
		retour = locateFlex(["test", "singular", "indicative", "firstPerson", "imperfect"])
		self.assertEqual(retour, ("indimpar",1,0))
		retour = locateFlex(["test", "singular", "indicative", "secondPerson", "imperfect"])
		self.assertEqual(retour, ("indimpar",2,0))
		retour = locateFlex(["test", "singular", "indicative", "thirdPerson", "imperfect"])
		self.assertEqual(retour, ("indimpar",3,0))
		retour = locateFlex(["test", "plural", "indicative", "firstPerson", "imperfect"])
		self.assertEqual(retour, ("indimpar",1,3))
		retour = locateFlex(["test", "plural", "indicative", "secondPerson", "imperfect"])
		self.assertEqual(retour, ("indimpar",2,3))
		retour = locateFlex(["test", "plural", "indicative", "thirdPerson", "imperfect"])
		self.assertEqual(retour, ("indimpar",3,3))
		
		#Traitement de l'indicatif passé simple
		retour = locateFlex(["test", "singular", "indicative", "firstPerson", "simplePast"])
		self.assertEqual(retour, ("simple",1,0))
		retour = locateFlex(["test", "singular", "indicative", "secondPerson", "simplePast"])
		self.assertEqual(retour, ("simple",2,0))
		retour = locateFlex(["test", "singular", "indicative", "thirdPerson", "simplePast"])
		self.assertEqual(retour, ("simple",3,0))
		retour = locateFlex(["test", "plural", "indicative", "firstPerson", "simplePast"])
		self.assertEqual(retour, ("simple",1,3))
		retour = locateFlex(["test", "plural", "indicative", "secondPerson", "simplePast"])
		self.assertEqual(retour, ("simple",2,3))
		retour = locateFlex(["test", "plural", "indicative", "thirdPerson", "simplePast"])
		self.assertEqual(retour, ("simple",3,3))
		
		#Traitement de subjonctif présent
		retour = locateFlex(["test", "singular", "subjunctive", "firstPerson", "present"])
		self.assertEqual(retour, ("subjpres",1,0))
		retour = locateFlex(["test", "singular", "subjunctive", "secondPerson", "present"])
		self.assertEqual(retour, ("subjpres",2,0))
		retour = locateFlex(["test", "singular", "subjunctive", "thirdPerson", "present"])
		self.assertEqual(retour, ("subjpres",3,0))
		retour = locateFlex(["test", "plural", "subjunctive", "firstPerson", "present"])
		self.assertEqual(retour, ("subjpres",1,3))
		retour = locateFlex(["test", "plural", "subjunctive", "secondPerson", "present"])
		self.assertEqual(retour, ("subjpres",2,3))
		retour = locateFlex(["test", "plural", "subjunctive", "thirdPerson", "present"])
		self.assertEqual(retour, ("subjpres",3,3))
		
		#Traitement de subjonctif imparfait
		retour = locateFlex(["test", "singular", "subjunctive", "firstPerson", "imperfect"])
		self.assertEqual(retour, ("subjimpar",1,0))
		retour = locateFlex(["test", "singular", "subjunctive", "secondPerson", "imperfect"])
		self.assertEqual(retour, ("subjimpar",2,0))
		retour = locateFlex(["test", "singular", "subjunctive", "thirdPerson", "imperfect"])
		self.assertEqual(retour, ("subjimpar",3,0))
		retour = locateFlex(["test", "plural", "subjunctive", "firstPerson", "imperfect"])
		self.assertEqual(retour, ("subjimpar",1,3))
		retour = locateFlex(["test", "plural", "subjunctive", "secondPerson", "imperfect"])
		self.assertEqual(retour, ("subjimpar",2,3))
		retour = locateFlex(["test", "plural", "subjunctive", "thirdPerson", "imperfect"])
		self.assertEqual(retour, ("subjimpar",3,3))
		
		#Traitement de conditionel présent
		retour = locateFlex(["test", "singular", "conditional", "firstPerson", "present"])
		self.assertEqual(retour, ("cond",1,0))
		retour = locateFlex(["test", "singular", "conditional", "secondPerson", "present"])
		self.assertEqual(retour, ("cond",2,0))
		retour = locateFlex(["test", "singular", "conditional", "thirdPerson", "present"])
		self.assertEqual(retour, ("cond",3,0))
		retour = locateFlex(["test", "plural", "conditional", "firstPerson", "present"])
		self.assertEqual(retour, ("cond",1,3))
		retour = locateFlex(["test", "plural", "conditional", "secondPerson", "present"])
		self.assertEqual(retour, ("cond",2,3))
		retour = locateFlex(["test", "plural", "conditional", "thirdPerson", "present"])
		self.assertEqual(retour, ("cond",3,3))
		
		#Echecs de la fonction
		retour = locateFlex(["test", 0, "indicative", "firstPerson", "present"])
		self.assertEqual(retour, ("indpres",1,-1))
		retour = locateFlex(["test", "singular", "indicative", 0, "present"])
		self.assertEqual(retour, ("indpres",-1,0))
		retour = locateFlex(["test", "singular", "test", "firstPerson", "test"])
		self.assertEqual(retour, ("",1,0))
		retour = locateFlex(None)
		self.assertEqual(retour, (None, None, None))
	
		
if __name__ == '__main__':
	unittest.main()
