#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
import os

try:
    from build_tiroir import construct
except ImportError :
    from TiroirLemmeAnew.build_tiroir import construct

class TestFonction_construct(unittest.TestCase):

    def setUp(self):
        self.cluster = {'accroître': ['accroîtra', 'accroîtras', 'accroîtrai', 'accroîtraient', 'accroîtrais', 'accroîtrait', \
        'accroître', 'accroîtrez', 'accroîtriez', 'accroîtrions', 'accroîtrons', 'accroîtront', 'accrois', \
        'accroissaient', 'accroissais', 'accroissait', 'accroisse', 'accroissent', 'accroisses', 'accroissez', \
        'accroissiez', 'accroissions', 'accroissons', 'accroît', 'accrurent', 'accrus', 'accrusse', \
        'accrussent', 'accrusses', 'accrussiez', 'accrussions', 'accrut', 'accrûmes', 'accrût', \
        'accrûtes', ['accr']], \
        'acheter': ['acheta', 'achetas', 'achetai', 'achetaient', 'achetais', 'achetait', 'achetasse', \
        'achetassent', 'achetasses', 'achetassiez', 'achetassions', 'acheter', 'achetez', 'achetiez', \
        'achetions', 'achetons', 'achetâmes', 'achetât', 'achetâtes', 'achetèrent', 'achète', \
        'achètent', 'achètera', 'achèterai', 'achèteraient', 'achèterais', 'achèterait', 'achèteras', \
        'achèterez', 'achèteriez', 'achèterions', 'achèterons', 'achèteront', 'achètes', \
        ['achèt', 'achet']]}

        self.data = [['accroître', 'infinitif', 0], ['accrois', 'indpres', 2], ['accroissaient', 'indimpar', 6], \
        ['accroissais', 'indimpar', 2], ['accroissait', 'indimpar', 3], ['accroisse', 'subjpres', 3], \
        ['accroissent', 'indpres', 6], ['accroissent', 'subjpres', 6], ['accroisses', 'subjpres', 2], \
        ['accroissez', 'indpres', 5], ['accroissiez', 'indimpar', 5], ['accroissiez', 'subjpres', 5], \
        ['accroissions', 'indimpar', 4], ['accroissions', 'subjpres', 4], ['accroissons', 'indpres', 4], \
        ['accroît', 'indpres', 3], ['accroîtra', 'indfut', 3], ['accroîtrai', 'indfut', 1], \
        ['accroîtraient', 'cond', 6], ['accroîtrais', 'cond', 2], ['accroîtrait', 'cond', 3], \
        ['accroîtras', 'indfut', 2], ['accroîtrez', 'indfut', 5], ['accroîtriez', 'cond', 5], \
        ['accroîtrions', 'cond', 4], ['accroîtrons', 'indfut', 4], ['accroîtront', 'indfut', 6], \
        ['accrurent', 'simple', 6], ['accrus', 'simple', 2], ['accrusse', 'subjimpar', 1], \
        ['accrussent', 'subjimpar', 6], ['accrusses', 'subjimpar', 2], ['accrussiez', 'subjimpar', 5], \
        ['accrussions', 'subjimpar', 4], ['accrûmes', 'simple', 4], ['accrut', 'simple', 3], ['accrut', 'subjimpar', 3], \
        ['accrûtes', 'simple', 5], ['accrût', 'subjimpar', 3], ['accroisse', 'subjpres', 1], \
        ['accrois', 'indpres', 1], ['accroissais', 'indimpar', 1], ['accrus', 'simple', 1], \
        ['acheter', 'infinitif', 0], ['acheta', 'simple', 3], ['achetai', 'simple', 1], \
        ['achetaient', 'indimpar', 6], ['achetais', 'indimpar', 2], ['achetait', 'indimpar', 3], \
        ['achetas', 'simple', 2], ['achetasse', 'subjimpar', 1], ['achetassent', 'subjimpar', 6], \
        ['achetasses', 'subjimpar', 2], ['achetassiez', 'subjimpar', 5], ['achetassions', 'subjimpar', 4], \
        ['achetez', 'indpres', 5], ['achetiez', 'indimpar', 5], ['achetiez', 'subjpres', 5], \
        ['achetions', 'indimpar', 4], ['achetions', 'subjpres', 4], ['achetâmes', 'simple', 4], \
        ['achetât', 'subjimpar', 3], ['achetâtes', 'simple', 5], ['achetons', 'indpres', 4], \
        ['achetèrent', 'simple', 6], ['achète', 'indpres', 1], ['achète', 'indpres', 3], \
        ['achète', 'subjpres', 3], ['achètent', 'indpres', 6], ['achètent', 'subjpres', 6], \
        ['achètera', 'indfut', 3], ['achèterai', 'indfut', 1], ['achèteraient', 'cond', 6], \
        ['achèterais', 'cond', 2], ['achèterait', 'cond', 3], ['achèteras', 'indfut', 2], \
        ['achèterez', 'indfut', 5], ['achèteriez', 'cond', 5], ['achèterions', 'cond', 4], \
        ['achèterons', 'indfut', 4], ['achèteront', 'indfut', 6], ['achètes', 'indpres', 2], \
        ['achètes', 'subjpres', 2], ['achète', 'subjpres', 1], ['achetais', 'indimpar', 1]]

        self.tenses = ['infinitif', 'indpres', 'indimpar', 'indfut', 'simple', 'subjpres', 'subjimpar', 'cond']
    
    def tearDown(self):
        os.system("rm -rf __pycache__")
        os.system("rm -rf tests/__pycache__")

    def test_construct_dictionary(self):
        sorted_suff = construct(self.data, self.cluster)[0]
        to_compare = {'acheter': {'indfut': ['erai', 'eras', 'era', 'erons', 'erez', 'eront'], 'infinitif': ['er'], 'subjimpar': ['asse', 'asses', 'ât', 'assions', 'assiez', 'assent'], 'subjpres': ['e', 'es', 'e', 'ions', 'iez', 'ent'], 'indimpar': ['ais', 'ais', 'ait', 'ions', 'iez', 'aient'], 'simple': ['ai', 'as', 'a', 'âmes', 'âtes', 'èrent'], 'cond': [0, 'erais', 'erait', 'erions', 'eriez', 'eraient'], 'indpres': ['e', 'es', 'e', 'ons', 'ez', 'ent']}, 'accroître': {'indfut': ['oîtrai', 'oîtras', 'oîtra', 'oîtrons', 'oîtrez', 'oîtront'], 'infinitif': ['oître'], 'subjimpar': ['usse', 'usses', 'ût', 'ussions', 'ussiez', 'ussent'], 'subjpres': ['oisse', 'oisses', 'oisse', 'oissions', 'oissiez', 'oissent'], 'indimpar': ['oissais', 'oissais', 'oissait', 'oissions', 'oissiez', 'oissaient'], 'simple': ['us', 'us', 'ut', 'ûmes', 'ûtes', 'urent'], 'cond': [0, 'oîtrais', 'oîtrait', 'oîtrions', 'oîtriez', 'oîtraient'], 'indpres': ['ois', 'ois', 'oît', 'oissons', 'oissez', 'oissent']}}
        self.assertEqual(sorted_suff, to_compare)

    def test_sub_dictionary_keys(self):
        sorted_suff = construct(self.data, self.cluster)[0]
        to_test = []
        for elt in sorted_suff.keys():
            tmp = sorted_suff[elt]
            to_test.append(list(tmp.keys()))
        for keys_list in to_test:
            self.assertEqual(sorted(keys_list), sorted(self.tenses))

if __name__ == '__main__':
    	unittest.main()
