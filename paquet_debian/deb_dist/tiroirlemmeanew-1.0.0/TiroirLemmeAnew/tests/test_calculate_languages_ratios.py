#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
import os

try:
    from nltk_stopwords import _calculate_languages_ratios
except ImportError :
    from TiroirLemmeAnew.nltk_stopwords import _calculate_languages_ratios

import nltk

import warnings

class TestFonction_calculate_languages_ratios(unittest.TestCase):

    def setUp(self):
        self.textRien = ""
        self.textFr = "C'est à trente minutes de trajet. J'y serai dans dix."
        self.textEn = "That's thirty minutes away. I'll be there in ten."
        self.textIt = "E adesso andate via, Voglio restare sola, Con la malinconia, Volare nel suo cielo"
        self.textEs = "La lectura, la lectura esta hoy para mi una palabra poco conocido."
        self.lang = set(nltk.corpus.stopwords.fileids())

    def tearDown(self):
        os.system("rm -rf __pycache__")
        os.system("rm -rf tests/__pycache__")

    def test_calculate_languages_ratios_rien(self):
        warnings.simplefilter("ignore")
        lang_dic = _calculate_languages_ratios(self.textRien)
        for key,value in lang_dic.items():
            self.assertEqual(value, 0)
            self.assertIn(key, self.lang)

    def test_calculate_languages_ratios_fr(self):
        warnings.simplefilter("ignore")
        lang_dic = _calculate_languages_ratios(self.textFr)
        for key in lang_dic:
            self.assertIn(key, self.lang)

    def test_calculate_languages_ratios_En(self):
        warnings.simplefilter("ignore")
        lang_dic = _calculate_languages_ratios(self.textEn)
        for key in lang_dic:
            self.assertIn(key, self.lang)

if __name__ == '__main__':
    unittest.main()
        
