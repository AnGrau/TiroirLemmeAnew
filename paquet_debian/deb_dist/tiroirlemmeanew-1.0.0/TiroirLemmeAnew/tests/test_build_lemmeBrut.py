#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
import sys
import os
sys.path.append(os.getenv("PWD")[:-len("/tests")])

try:
	from data_fetch import *
except ImportError :
	from TiroirLemmeAnew.data_fetch import *

try:
	from clustering import *
except ImportError :
	from TiroirLemmeAnew.clustering import *

try:
	from build_lemme import *
except ImportError :
	from TiroirLemmeAnew.build_lemme import *

from pprint import pprint

class Test_build_lemme(unittest.TestCase) :
	
	def setUp(self) :
		path = os.getcwd()
		if "TiroirLemmeAnew/TiroirLemmeAnew/tests" in path :
			path =  ""
		elif "TiroirLemmeAnew/TiroirLemmeAnew" in path :
			path = "tests/"
		else :
			path = "TiroirLemmeAnew/tests"
		self.families = read_pickle(path + "familiesVerbs.pickle")
		self.lemmeList = read_pickle(path + "lemmeList.pickle")
		self.dico_clust = read_pickle(path + "final_clust.pickle")
		self.familiesName = read_pickle(path + "familiesNames.pickle")
		self.dico_clustName = read_pickle(path + "final_clustName.pickle")
		self.lemmeListName = read_pickle(path + "lemmeListName.pickle")
		self.dico_tag_verbs = read_pickle(path + "dico_tag_verbs.pickle")
		self.dico_tag_names = read_pickle(path + "dico_tag_names.pickle")
		
		self.final_lemmeBrutVerbs = read_pickle(path + "final_lemmeBrutVerbs.pickle")
		self.final_lemmeBrutNames = read_pickle(path + "final_lemmeBrutNames.pickle")
		self.modeles_verbes = read_pickle(path + "modeles_verbes.pickle")
		self.modeles_noms = read_pickle(path + "modeles_noms.pickle")
		self.modeles_verbes_final = read_pickle(path + "modeles_verbes_final.pickle")
		self.modeles_noms_final = read_pickle(path + "modeles_noms_final.pickle")
		
	def test_build_lemmeList(self) :
		test_lemmeList = build_lemmeList(self.dico_clust)
		self.assertEqual(test_lemmeList, self.lemmeList)
		
		test_lemmeListName = build_lemmeList(self.dico_clustName)
		self.assertEqual(test_lemmeListName, self.lemmeListName)
		
		test_lemmeList = build_lemmeList(None)
		self.assertEqual(test_lemmeList, None)
		
		test_lemmeListName = build_lemmeList(self.dico_clustName, None)
		self.assertEqual(test_lemmeListName, None)
		
		test_lemmeList = build_lemmeList({})
		self.assertEqual(test_lemmeList, [])
		
		test_lemmeList = build_lemmeList({"test":[1,2,3,4,5]})
		self.assertEqual(test_lemmeList, None)

	def test_stemmatise(self) :
		test_dico_clust, test_families = final_clust(self.families)
		test_dico_clust = stemmatise(test_dico_clust)
		
		
		
		for k in test_dico_clust.keys() :
			self.assertEqual(test_dico_clust[k][-1], self.dico_clust[k][-1])

		test_dico_clustName = final_clustName(self.familiesName)
		test_dico_clustName = stemmatise(test_dico_clustName)
		
		for k in test_dico_clustName.keys() :
			self.assertEqual(test_dico_clustName[k][-1], self.dico_clustName[k][-1])
		
		test_dico_clust = stemmatise(None)
		self.assertEqual(test_dico_clust, None)
		
		test_dico_clust = stemmatise([])
		self.assertEqual(test_dico_clust, None)
		
		test_dico_clust = stemmatise([1,2,3,4,5])
		self.assertEqual(test_dico_clust, None)
	
	def test_final_clust(self) :
		test_dico_clust, test_families = final_clust(self.families)
		
		test_dico_clust = stemmatise(test_dico_clust)
		
		for k in test_dico_clust.keys() :
			for flex in test_dico_clust[k] :
				self.assertIn(flex, self.dico_clust[k])
			for flex in self.dico_clust[k] :
				self.assertIn(flex, test_dico_clust[k])
		
		test_dico_clust = final_clust(None)
		self.assertEqual(test_dico_clust, (None, None))
		
		test_dico_clust = final_clust(self.families, None)
		self.assertEqual(test_dico_clust, (None, None))
		
		test_dico_clust = final_clust(self.families, {}, None)
		self.assertEqual(test_dico_clust, (None, None))
		
		test_dico_clust = final_clust([])
		self.assertEqual(test_dico_clust, ({}, []))
		
		test_dico_clust = final_clust([[1,2,3,4,5]])
		self.assertEqual(test_dico_clust, ({}, [[1,2,3,4,5]]))
		
		test_dico_clust = final_clust([["test", []]])
		self.assertEqual(test_dico_clust, ({}, [["test", []]]))
		
		test_dico_clust = final_clust([["test", [1, 2, 3, 4, 5]]])
		self.assertEqual(test_dico_clust, ({}, [["test", [1, 2, 3, 4, 5]]]))
	
	def test_final_clustName(self) :
		test_dico_clustName = final_clustName(self.familiesName)
		test_dico_clustName = stemmatise(test_dico_clustName)
		
		for k in test_dico_clustName.keys() :
			for name in test_dico_clustName[k] :
				self.assertIn(name, self.dico_clustName[k])
			for name in self.dico_clustName[k] :
				self.assertIn(name, test_dico_clustName[k])
		
		test_dico_clustName = final_clustName(None)
		self.assertEqual(test_dico_clustName, None)
		
		test_dico_clustName = final_clustName([])
		self.assertEqual(test_dico_clustName, {})
		
		test_dico_clustName = final_clustName([[1,2,3,4,5]])
		self.assertEqual(test_dico_clustName, {})
		
		test_dico_clustName = final_clustName([["test", []]])
		self.assertEqual(test_dico_clustName, {'test': ['test', []]})
		
		test_dico_clustName = final_clustName([["test", [1,2,3,4,5]]])
		self.assertEqual(test_dico_clustName, {'test': ['test', [1, 2, 3, 4, 5]]})


	def test_final_lemmeBrut(self) :
		test_lemmeBrutVerbs = final_lemmeBrut(self.modeles_verbes, list(self.modeles_verbes_final.keys()), self.lemmeList)
		self.assertEqual(len(test_lemmeBrutVerbs), len(self.final_lemmeBrutVerbs))
		
		test_lemmeBrutNames = final_lemmeBrut(self.modeles_noms, list(self.modeles_noms_final.keys()), self.lemmeListName)
		self.assertEqual(len(test_lemmeBrutNames), len(self.final_lemmeBrutNames))
		
		test_lemmeBrutNames = final_lemmeBrut(None,list(self.modeles_noms_final.keys()),  self.lemmeListName)
		self.assertEqual(test_lemmeBrutNames, None)
		
		test_lemmeBrutNames = final_lemmeBrut(self.modeles_noms,None, self.lemmeListName)
		self.assertEqual(test_lemmeBrutNames, None)
		
		test_lemmeBrutNames = final_lemmeBrut(self.modeles_noms,list(self.modeles_noms_final.keys()), None)
		self.assertEqual(test_lemmeBrutNames, None)
		
		test_lemmeBrutNames = final_lemmeBrut({},list(self.modeles_noms_final.keys()), self.lemmeListName)
		self.assertEqual(test_lemmeBrutNames, None)
		
		test_lemmeBrutNames = final_lemmeBrut(self.modeles_noms,list(self.modeles_noms_final.keys()), [])
		self.assertEqual(len(test_lemmeBrutNames), 0)
		
		test_lemmeBrutNames = final_lemmeBrut(self.modeles_noms, [], self.lemmeListName)
		self.assertEqual(None, None)
		
		test_lemmeBrutNames = final_lemmeBrut({"test":"test"},list(self.modeles_noms_final.keys()),  self.lemmeListName)
		self.assertEqual(test_lemmeBrutNames, None)
		
		
		
		
if __name__ == '__main__':
	unittest.main()		
