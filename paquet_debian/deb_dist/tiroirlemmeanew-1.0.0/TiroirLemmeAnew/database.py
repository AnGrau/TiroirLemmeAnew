#! /usr/bin/env python3
# -*- coding: utf-8 -*-


#Le script contient un ensemble de fonctions permettant d'intéragir avec la base de données. Le script peut être appelé de plusieurs façons :

#- python3 database.py -n : L'option -n permet de créer une base de données TLDatabase

#- python3 database.py -d : L'option -d permet de supprimer la base de données TLDatabase

#- python3 database.py -r : L'option -r permet de supprimer toutes les tables de la base de données 



from PyQt5 import QtSql
try:
    from data_fetch import erreur
except ImportError :
    from TiroirLemmeAnew.data_fetch import erreur

from getpass import getpass
from os import system
from sys import argv
from pprint import pprint


def createDb(dbUser, dbName, dbPass) :
	"""
	Connexion en mode superutilisateur postgres pour créer un utilisateur et une base de données postgresql pour le programme
	
	Args :
	dbUser (str) : Le nom de l'utilisateur à créer
	dbName (str) : Le nom de la base de donnée à créer
	dbPass (str) : Le mot de passe de l'utilisateur de la base de données
	
	Return :
	True (bool) : Si tout c'est bien passé la fonction renvoie True. En cas d'erreur elle renvoie False
	"""
	if type(dbUser) != str :
		return erreur("createDb : Paramètre dbUser incorrecte : {}".format(dbUser), False)
	if type(dbName) != str :
		return erreur("createDb : Paramètre dbName incorrecte : {}".format(dbName), False)
	if type(dbPass) != str :
		return erreur("createDb : Paramètre dbPass incorrecte", False)
		
	db = connecDb("postgres", "")
	if db == None :
		return erreur("createDb : Echec de connexion à la base de données", False)
	
	query = QtSql.QSqlQuery()
	if type(query) != QtSql.QSqlQuery :
		return erreur("createDb : Echec de création de l'objet QSqlQuery", False)
		
	if query.exec_("CREATE ROLE {} PASSWORD '{}';".format(dbUser, dbPass)) == False :
		return erreur("createDb : Echec de création de l'utilisateur {}".format(dbUser), False)
	if query.exec_("ALTER ROLE {} WITH LOGIN;".format(dbUser)) == False :
		return erreur("createDb : Echec de paramétrage de l'utilisateur {}".format(dbUser), False)
	if query.exec_("CREATE DATABASE {} OWNER {};".format(dbName, dbUser)) == False :
		return erreur("createDb : Echec de création de la base de donnée".format(dbName), False)
		
	db.close()
	return True 

def dropDb(dbUser, dbName) :
	"""
	Connexion en mode superutilisateur postgres pour supprimer une base de données et l'utilisateur postgresql utilisé pour le programme
	
	Args :
	dbUser (str) : Le nom de l'utilisateur à supprimer
	dbName (str) : Le nom de la base de données à supprimer
	
	Return :
	True (bool) : Si tout c'est bien passé la fonction renvoie True. En cas d'erreur la fonction renvoie False
	"""
	
	if type(dbUser) != str :
		return erreur("dropDb : Paramètre dbUser incorrecte : {}".format(dbUser), False)
	if type(dbName) != str : 
		return erreur("dropDb : Paramètre dbName incorrecte : {}".format(dbName), False)
	
	db = connecDb("postgres", "")
	if db == None :
		return erreur("dropDb : Echec de connexion à la base de donnée", False)
		
	query = QtSql.QSqlQuery()
	if type(query) != QtSql.QSqlQuery : 
		return erreur("dropDb : Echec de création de l'objet QSqlQuery", False)
	
	if query.exec_("DROP DATABASE {};".format(dbName)) == False :
		return erreur("dropDb : Echec de suppression de la base de données {}".format(dbName), False)
	if query.exec_("DROP ROLE {}".format(dbUser)) == False :
		return erreur("dropDb : Echec de suppression de l'utilisateur {}".format(dbUser), False)
	db.close()
	return True


def connecDb(dbUser, dbName, dbPass="") :
	"""
	Réalise une connexion à la base de donnée Postgresql. Pour cela elle va paramétrer cette connexion en récupérant le nom de l'utilisateur, le nom de la base de donnée, le mot de passe et le type de connexion désiré.
	
	Args:
	dbUser (str) : Utilisateur avec lequel on se connecte à postgresql
	dbName (str) : Nom de la base de donnée à laquel on se connecte
	
	Return :
	db (PyQt5.QtSql.QSqlDatabase) : Retourne un objet représentant la connexion. En cas d'erreur la fonction retourne None
	"""
	if (type(dbUser) != str) or (type(dbName) != str) :
		return erreur("connecDb: Paramètres de la fonction incorrectes", None)
	
	try :
		db = QtSql.QSqlDatabase.addDatabase('QPSQL')
		if dbName != "" :
			db.setDatabaseName(dbName)
		db.setUserName(dbUser)
		db.setHostName("localhost")
		if dbPass == "" :	
			db.setPassword(getpass("Connexion à postgresql -> mot de passe de {} : ".format(dbUser)))
		else :
			db.setPassword(dbPass)
		if db.open() == False :
			return erreur("connecDb : Echec de connexion à la base de donnée", None)
		
		query = QtSql.QSqlQuery()
	except :
		return erreur("connecDb : Echec de la fonction connecDb", None)
	
	return db

def createTable(tableName, tableHead) :
	"""
	Ajout d'une table à la base de donnée
	
	Args:
	tableName (str) : Le nom de la table à créer
	tableHead (list) : Une liste de sous-liste indiquant les attributs de la table. Chaque sous-liste contient le nom de l'attribut et son type (par exemple : [[Nom, str], [Age, int], ...])
	
	Return :
	True : Si l'exécution s'est bien passée, la fonction retourne True. En cas d'erreur elle retourne False
	"""
	
	if type(tableName) != str :
		return erreur("createTable : Erreur paramètre tableName incorrecte {}".format(tableName), False)
	if type(tableHead) != list :
		return erreur("createTable : Erreur paramètre tableHead incorrecte {}".format(tableHead), False)
	
	try :
		query = QtSql.QSqlQuery()
		chaine = []
		for head in tableHead :
			if head[1] == "int" :
				chaine.append("{} INT".format(head[0]))
			elif head[1] == "str" :
				chaine.append("{} VARCHAR(50)".format(head[0]))
			else :
				return erreur("createTable : Type de l'attribut incorrecte {}".format(head), False)
	
		chaine = ",".join(chaine)
	
		if query.exec_("DROP TABLE IF EXISTS {};".format(tableName)) == False :
			return erreur("createTable : Echec de la requête DROP TABLE", False)
		
		if query.exec_("CREATE TABLE {} ({});".format(tableName, chaine)) == False :
			return erreur("createTable : Echec de la requête CREATE TABLE", False)
	except :
		return erreur("createTable : Echec de la fonction createTable", False)
	
	return True

def dropTable(tableName) :
	"""
	Suppression d'une table de la base de donnée
	
	Arg:
	tableName (str) : Nom de la table à supprimer
	
	Return
	True (bool) : Si tout c'est bien passée la fonction retourne True. En cas d'erreur la fonction retourne False 
	"""
	if type(tableName) != str :
		return erreur("dropTable : Paramètre tableName : {} incorrecte".format(tableName), False)
		
	query = QtSql.QSqlQuery()
	if type(query) != QtSql.QSqlQuery : 
		return erreur("dropTable : Echec de création de l'objet PyQt5.QtSql.QSqlQuery", False)
		
	if query.exec_("DROP TABLE IF EXISTS {};".format(tableName)) == False :
		return erreur("dropTable : Echec de la requête DROP TABLE", False)
	return True

def insertTable(tableName, tableData) :
	"""
	Insertion de valeurs dans une table déjà présente dans la base de donnée
	
	Args:
	tableName (str) : Le nom de la table dans laquelle on va réaliser l'insertion
	tableData (list) : Une liste des éléments que l'on va ajouter 
	
	Return:
	True (bool) : Si tout c'est bien passé la fonction retourne True. En cas d'erreur elle retourne False
	"""
	if type(tableName) != str :
		return erreur("insertTable : Paramètre tableName incorrecte {}".format(tableName), False)
	if type(tableData) != list and type(tableData) != tuple :
		return erreur("insertTable : Paramètre tableData incorrecte {}".format(tableData), False)
	
	query = QtSql.QSqlQuery()
	if type(query) != QtSql.QSqlQuery :
		return erreur("insertTable : Echec de construction de l'objet PyQt5.QtSql.QSqlQuery", False)
	
	tableData = list(tableData)	
	tableData = adjust_tableData(tableData)
	if tableData == None :
		return erreur("insertTable : Echec de la fonction adjust_tableData", False)
	try :	
		chaine = ",".join(tableData)
	except :
		return erreur("insertTable : Erreur tableData : {} non conforme".format(tableData), False)
	if query.exec_("INSERT INTO {} VALUES({});".format(tableName, chaine)) == False :
		return erreur("insertTable : Echec d'exécution de la requête INSERT INTO", False)
		
	return True

def adjust_tableData(tableData) :
	"""
	Ajustement des valeurs d'entrée à une table de la base de données pour que l'insertion fonctionne. Cette fonction travaille avec insertTable
	
	Arg:
	tableData (list) : Liste des données à entrer dans une table
	
	Return:
	tableData (list) : Liste des données à entrer dans la table après ajustement. En cas d'erreur la fonction retourne None
	"""
	
	if type(tableData) != list :
		return erreur("adjust_tableData : Paramètre tableData incorrecte {}".format(tableData), None)
	
	try :
		for i in range(len(tableData)) :
			if type(tableData[i]) == str :
				if '\'' not in tableData[i] :
					tableData[i] = "'"+tableData[i]+"'"
				else :
					tableData[i] = tableData[i].replace("'", "''")
					tableData[i] = "'"+tableData[i]+"'"
			elif type(tableData[i]) == int :
				tableData[i] = str(tableData[i])
	except :
		return erreur("adjust_tableData : erreur à l'exécution de la fonction", None)
		
	return tableData


def readTable(tableName) :
	"""
	Récupère le contenu d'une table et le retourne sous la forme d'une liste de sous-liste contenant les lignes de la table
	
	Arg:
	tableName (str) : Nom de la table que l'on va lire dans la base de données
	
	Return:
	table (list) : Liste de sous-liste, où chacune d'elle est une ligne de la table lue. En cas d'erreur la fonction retourne None
	"""
	
	if type(tableName) != str :
		return erreur("readTable : Paramètre tableName incorrecte {}".format(tableName), None)
	
	query = QtSql.QSqlQuery()
	if type(query) != QtSql.QSqlQuery :
		return erreur("readTable : Echec de création de l'objet PyQt5.QtSql.QSqlQuery", None)
		
	if query.exec_("SELECT * FROM {}".format(tableName)) == False :
		return erreur("readTable : Echec de la requête SELECT", None)
	try :	
		table = []
		taille = 100
		while query.next() :
			ligne = []
			j=0
			while  j < taille:
				if query.value(j) == None :
					taille = j
					break
				ligne.append(query.value(j))
				j += 1
			table.append(ligne)
		query.clear()
	except :
		return erreur("readTable : Erreur à l'exécution de la fonction", None)
		
	return table

def resultsToDb(tableName, results, attr=[]) :
	"""
	Fonction destinée à sauvegarder lemmeBrut et tiroirBrut dans la base de données.
	
	Args:
	tableName (str): Le nom de la table qui va accueillir le résultat
	results (list) : Le résultat à sauvegarder sous la forme d'une liste de sous-listes
	attr (list) : Liste des attributs de la table qui va accueillir le résultat
	
	Return:
	True (bool) : Si tout c'est bien passé la fonction retourne True. En cas d'erreur elle retourne False
	"""
	if type(tableName) != str :
		return erreur("resultsToDb: Paramètre tableName incorrect : {}".format(tableName), False)
	if type(attr) != list :
		return erreur("resultsToDb : Paramètre attr incorrect : {}".format(attr), False)
	if type(results) != list :
		return erreur("resultsToDb : Paramètre results incorrect : {}".format(results), False)
	if attr != [] :
		if createTable(tableName, attr) == False :
			return erreur("resultsToDb : Echec de création de la table {}".format(tableName), False)
	
	for line in results :
		if insertTable(tableName, line) == False :
			return erreur("resultsToDb : Echec d'écriture de {} dans {}".format(line, tableName), False)
	
	return True

def dbToResults(tableName, ftuple=False) :
	"""
	Récupère le contenu de la table lemmeBrut ou tiroirBrut et le transforme en liste pour l'analyseur morphologique
	
	Args:
	tableName (str) : Nom de la table dans laquelle récupérer la structure de données
	ftuple (bool) : Flag indiquant si le contenu de la structure de données doit être une liste de tuples (si True), ou une liste de sous-listes (si False par défaut)
	
	Return:
	results (list) : Retourne un lemmeBrut ou un tiroirBrut sous la forme d'une liste de sous-listes, ou d'une liste de tuples 
	"""
	
	if type(tableName) != str :
		return erreur("dbToResults : Erreur paramètre tableName incorrecte : {}".format(tableName), None)
	
	if type(ftuple) != bool :
		ftuple = erreur("dbToResults : Erreur paramètre ftuple non correcte je le change en False et je continue l'exécution par défaut", False)

	results = readTable(tableName)
	if results == None :
		return erreur("dbToResults : Erreur, echec de lecture de la table : {}".format(tableName), None)
	
	if ftuple == True :
		for i in range(len(results)) :
			results[i] = tuple(results[i])

	return results


def modelsToDb(tableName, models, attr) :
	"""
	Cette fonction est spécialement pensée pour sauvegarder la structure de données modeles dans la base de données
	
	Args :
	tableName (str) : Le nom de la table à créer
	models (dict) : Un dictionnaire des modèles sous la forme {"verbe_ou_nom" : [#['1','1', [...],...]]}
	attr (list) : Liste indiquant les en-têtes de la tables à créer
	
	Return :
	La fonction retourne True si le travail s'est effectué sans problème. En cas d'erreur elle retourne False
	"""
	if type(tableName) != str : 
		return erreur("modelsToDb : Erreur paramètre tableName incorrecte {}".format(tableName), False)
	if type(models) != dict :
		return erreur("modelsToDb : Erreur paramètre models incorrecte {}".format(models), False)
	if type(attr) != list :
		return erreur("modelsToDb : Erreur paramètre attr incorrecte {}".format(attr), False)

	keylist = list(models.keys())
	keylist.sort()
	if createTable(tableName, attr) == False :
		return erreur("modelsToDb : Echec de création de la table {}".format(tableName), False)
		
	for key in keylist :
		if indexToDb(tableName, key, 0, 0, models[key]) == None :
			return erreur("modelsToDb : Echec d'exécution de indexToDb pour la clé {}".format(key), False)
	
	return True

def indexToDb(tableName, key, shiftdown, shiftup, sublist) :
	"""
	Cette fonction est appelée par modelsToDb, elle facilite la décomposition de la structure de données modeles pour la stocker dans la base de données
	
	Args :
	tableName (str) : Nom de la table dans laquelle on va insérer les données
	key (str) : La clé actuelle du dictionnaire des modèles
	shiftdown (int) : Nombre négatif indiquant la profondeur à remonter d'une liste
	shiftup (int) : Nombre positif indiquant la profondeur à ajouter d'une liste
	sublist (list) : La liste à parcourir et dont il faut stocker les éléments
	
	Return :
	Si le travail s'effectue sans problème la fonction retourne True, sinon elle retourne False
	"""
	if type(tableName) != str :
		return erreur("indexToDb : Erreur paramètre tableName incorrecte {}".format(tableName), None)
	if type(key) != str :
		return erreur("indexToDb : Erreur paramètre key incorrecte {}".format(key), None)
	if type(sublist) != list :
		return erreur("indexTodDb : Erreur paramètre sublist incorrecte {}".format(sublist), None)
	if type(shiftdown) != int or type(shiftup) != int:
		return erreur("indexToDb : Erreur paramètre shiftdown ou shiftup incorrecte : shiftdown {} | shiftup {}".format(shiftdown, shiftup), None)		
	
	i = 0
	for data in sublist :
		if type(data) == list :
			shiftdown = indexToDb(tableName, key, shiftdown,shiftup + 1, data)
			if shiftdown == None :
				return erreur("indexToDb : Erreur d'exécution de la fonction indexToDb", None)
			shiftup = 0
			continue
		if insertTable(tableName, [key, shiftdown,shiftup, data]) == False :
			return erreur("indexToDb : Echec d'insertion de {} dans la table {}".format([key, shiftup, shiftdown, data], tableName), None)
		shiftup = 0
		shiftdown = 0
	return shiftdown - 1

def dbToModels(tableName) :
	"""
	Fonction destinée à récupérer la structure de données modeles dans la base de données 
	
	Arg:
	tableName (str) : Le nom de la table à lire
	
	Return: 
	models (list) : Structure de données des modèles de la forme {"verbe_ou_nom" : [#['1','1', [...],...]]}
	En cas d'erreur la fonction retourne None
	"""
	if type(tableName) != str :
		return erreur("dbToModels : Erreur paramètre tableName incorrecte {}".format(tableName), None)
	
	tableList = readTable(tableName)
	if tableList == None :
		return erreur("dbToModels : Echec de la fonction readTable", None)
	models = {}
	key = tableList[0][0]
	subList = []
	for line in tableList : 
		if line[0] != key :
			models[key], index, shiftdown, shiftup = dbToIndex(subList, 0, 0, 0)
			key = line[0]
			subList = [line]
		else :
			subList.append(line)
	models[key], index, test1, test2 = dbToIndex(subList, 0, 0, 0)
	return models

def dbToIndex(tableList, shiftdown, shiftup, index) :
	"""
	Cette fonction va reconstruire la structure de données modeles, à partir de la table récupérée depuis la base de données. Cette fonction est destinée à être appelée par la fonction dbToModels
	
	Args:
	tableList (list) : La table récupérée depuis la base de données sous la forme d'une liste ou chaque élément est une liste de la forme suivante [[verbe_noms, shiftdown, shiftup, value(temps conjugé, numéro de lemme ou numéro de tiroir)],...]
	shiftdown (int) : Nombre négatif indiquant la profondeur à remonter d'une liste
	shiftup (int) : Nombre positif indiquant la profondeur à ajouter d'une liste
	index (int) : Indique l'index courant de parcours de la table tableList au moment de l'appel de la fonction
	
	Returns:
	returnList (list) : Structure  de données sous forme de listes imbriquées formant les modèles
	shiftdown (int) : Nombre négatif indiquant la profondeur à remonter d'une liste
	shiftup (int) : Nombre positif indiquant la profondeur à ajouter d'une liste
	index (int) : Indique l'index courant de parcours de la table tableList au moment de l'appel de la fonction
	En cas d'erreur la fonction retourne le tuple (None, None, None, None)
	"""
	
	if type(tableList) != list :
		return erreur("dbToIndex : Erreur tableList est incorrect {}".format(tableList), (None, None, None, None))
	if type(shiftdown) != int or type(shiftup) != int or type(index) != int :
		return erreur("dbToIndex : Erreur paramètre shiftdown {}, shiftup {} et/ou index {} est/sont incorrect(s)".format(shiftdown, shiftup, index), (None, None,None,None))
	
	returnList = []
	i = index
	try :
		while i < len(tableList) : 
			#Quand shiftdown et shiftup égalise la valeur correspondante dans tableList la valeur correspondante est ajoutée à l'étage en cours de returnList
			if tableList[i][1] == shiftdown and tableList[i][2] == shiftup :
				if tableList[i][3].isdigit() : #Si la valeur à ajoutée de tableList est un nombre il est converti via int() (obsolète)
					returnList.append(tableList[i][3])
					shiftdown = 0
					shiftup = 0
				else : #Sinon la valeur est ajoutée en tant que str
					returnList.append(tableList[i][3])
					shiftdown = 0
					shiftup = 0
					#Si on est au bout de tableList et que la table se termine par un temps de conjugaison (et pas un #)
					if i+1 >= len(tableList) and tableList[i][3] != "#":
						returnList.append([]) #Alors on ajoute une liste vide 
					#Si la valeur actuelle est un str et que la valeur suivante est un str alors je rappelle dbToIndex en récursif pour ajouter une liste vide
					elif tableList[i+1][3].isalpha() == True and tableList[i][3] != "#":
						retour = dbToIndex(tableList, shiftdown, shiftup, i+1)
						returnList.append(retour[0])
						shiftdown = retour[1]
						shiftup = retour[2]
						i = retour[3]	
			#Si la valeur de shiftdown est supérieure à la valeur correspondante dans tableList
			elif tableList[i][1] < shiftdown :
				#Alors on retourne returnList et on décrémente shiftdown et l'index
				return returnList, shiftdown - 1, shiftup, i-1
			#Si la valeur de shiftup est inférieure à la valeur correspondante dans tableList
			elif tableList[i][2] > shiftup :
				#Alors on appelle en récursif dbToIndex pour donner plus de profondeur à la liste
				retour = dbToIndex(tableList, shiftdown, shiftup+1, i)
				returnList.append(retour[0])
				shiftdown = retour[1]
				shiftup = retour[2]
				i = retour[3]
			i += 1
	except :
		return erreur("dbToIndex : Erreur pendant la reconstruction de la structure de données", (None, None, None, None))
	return returnList, shiftdown, shiftup, i-1

def main_db():
	"""Cette fonction met en oeuvre la création de la base de donnée (-n),la suppression de la base (-d) ou la suppression des tables de la base (-r).

	Args: -

	Returns: -
	"""

	if len(argv) == 2:
		if "-n" in argv :
			#Créer la base de données
			print("Création de la base de données")
			if createDb("tluser", "tldatabase", "tluser") == False :
				exit("Echec de la fonction createDb")
			print("Création OK")
		elif "-d" in argv :
			#Suppression de la base de données
			print("Suppression de la base de données")
			if dropDb("tluser","tldatabase") == False :
				exit("Echec de suppression de la base de données")
			print("Suppression OK")
		elif "-r" in argv :
			#Suppression de toutes les tables
			print("Suppression des tables de la base de données")
			db = connecDb("tluser", "tldatabase", "tluser")
			if db == None :
				exit("Echec de connexion à la base de donnée")
		
			for table in ["lemmeBrutVerbs", "lemmeBrutNames", "tiroirBrutVerbs", "tiroirBrutNames", "modelsverbs", "modelsnames"] :
				if dropTable(table) == False :
					db.close()
					exit("Echec de suppression de la table {}".format(table))
		
			print("Suppression OK")
			db.close()
	else:
		exit("Un argument attendu: -n, -d ou -r")

if __name__ == "__main__" :
	if "-n" in argv :
		#Créer la base de données
		print("Création de la base de données")
		if createDb("tluser", "tldatabase", "tluser") == False :
			exit("Echec de la fonction createDb")
		print("Création OK")
	elif "-d" in argv :
		#Suppression de la base de données
		print("Suppression de la base de données")
		if dropDb("tluser","tldatabase") == False :
			exit("Echec de suppression de la base de données")
		print("Suppression OK")
	elif "-r" in argv :
		#Suppression de toutes les tables
		print("Suppression des tables de la base de données")
		db = connecDb("tluser", "tldatabase", "tluser")
		if db == None :
			exit("Echec de connexion à la base de donnée")
		
		for table in ["lemmeBrutVerbs", "lemmeBrutNames", "tiroirBrutVerbs", "tiroirBrutNames", "modelsverbs", "modelsnames"] :
			if dropTable(table) == False :
				db.close()
				exit("Echec de suppression de la table {}".format(table))
		
		print("Suppression OK")
		
		db.close()
	
