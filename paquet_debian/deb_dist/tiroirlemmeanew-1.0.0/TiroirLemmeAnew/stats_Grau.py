#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from decimal import Decimal
try:
    import html_lang
except ImportError :
    import TiroirLemmeAnew.html_lang

try:
    import build_tiroir
except ImportError :
    import TiroirLemmeAnew.build_tiroir
import mod
try:
    import mod
except ImportError :
    import TiroirLemmeAnew.mod

NB_MODELES_VERBES_BESCH = 97
NB_MODELES_NOMS_WIKI = 186

def calcule_nb_doublons(liste_a_traiter):
	if type(liste_a_traiter) == list:
		traiter = [elt[0] for elt in liste_a_traiter]
		liste_a_traiter = traiter
	tmp = set(liste_a_traiter)
	result = len(liste_a_traiter) - len(tmp)
	return result

def calc_flag(mod_entree, verb = True):
	tmp_mod_entree = []
	mod_entree_flex = []
	mod_entree_flag = 1
	for mod in mod_entree:
		tmp_mod_entree += [elt for elt in mod[-1]]
	tmp_mod_entree.sort()
	if verb:
		if tmp_mod_entree and tmp_mod_entree != ['1', '2', '3', '4', '5', '6']:
			mod_entree_flag = 0
	else:
		if tmp_mod_entree and tmp_mod_entree != ['1', '2']:
			mod_entree_flag = 0
	return mod_entree_flag

def est_complet_nom(entree):
	valeurs = list(entree.values())
	masc = valeurs[0][1][1]
	masc_genre = []
	tmp_masc = []
	masc_flag = 1
	fem = valeurs[0][2][1]
	fem_genre = []
	tmp_fem = []
	fem_flag = 1
	if masc:
		masc_flag = calc_flag(masc, False)
	if fem:
		fem_flag = calc_flag(fem, False)
	if masc_flag and fem_flag:
		return 1
	return 0

def est_complet_verbe(entree):
	valeurs = list(entree.values())
	indpres = valeurs[0][1][1]
	indpres_pers = []
	tmp_indpres = []
	indpres_flag = 1
	indimpar = valeurs[0][2][1]
	indimpar_pers = []
	tmp_indimpar = []
	indimpar_flag = 1
	indfut = valeurs[0][3][1]
	indfut_pers = []
	tmp_indfut = []
	indfut_flag = 1
	simple = valeurs[0][4][1]
	simple_pers = []
	tmp_simple = []
	simple_flag = 1
	cond = valeurs[0][5][1]
	cond_pers = []
	tmp_cond = []
	cond_flag = 1
	subjpres = valeurs[0][6][1]
	subjpres_pers = []
	tmp_subjpres = []
	subjpres_flag = 1
	subjimpar = valeurs[0][7][1]
	subjimpar_pers = []
	tmp_subjimpar = []
	subjimpar_flag = 1
	if indpres:
		indpres_flag = calc_flag(indpres, True)
	if indimpar:
		indimpar_flag = calc_flag(indimpar, True)
	if indfut:
		indfut_flag = calc_flag(indfut, True)
	if simple:
		simple_flag = calc_flag(simple, True)
	if cond:
		cond_flag = calc_flag(cond, True)
	if subjpres:
		subjpres_flag = calc_flag(subjpres, True)
	if subjimpar:
		subjimpar_flag = calc_flag(subjimpar, True)
	if indpres_flag and indimpar_flag and indfut_flag and simple_flag and cond_flag and subjpres_flag and subjimpar_flag:
		return 1
	return 0

def stats_mod(fichier_pour_calcul):
	complet_nom = 0
	complet_verbe = 0
	with open(fichier_pour_calcul) as f:
		for i, line in enumerate(f):
			if 'name' in fichier_pour_calcul:
				complet_nom += est_complet_nom(eval('{' + line + '}'))
			else:
				complet_verbe += est_complet_verbe(eval('{' + line + '}'))
	nb_modeles = i + 1
	if 'name' in fichier_pour_calcul:
		automatique_vs_reference = round(Decimal(nb_modeles * 100 / NB_MODELES_NOMS_WIKI), 2)
		compl_mod = round(Decimal(complet_nom * 100 / nb_modeles), 2)
	else:
		automatique_vs_reference = round(Decimal(nb_modeles * 100 / NB_MODELES_VERBES_BESCH), 2)
		compl_mod = round(Decimal(complet_verbe * 100 / nb_modeles), 2)
	return automatique_vs_reference, compl_mod

def stats_tiroirs():
	to_save, names_to_save, sorted_suff_complete, names_sorted_suff_complete = build_tiroir.main_tir("https://fr.wikipedia.org/wiki/France", False)
	nb_verbes = round(Decimal(len(sorted_suff_complete) * 100 / NB_MODELES_VERBES_BESCH), 2)
	nb_noms = round(Decimal(len(names_sorted_suff_complete) * 100 / NB_MODELES_NOMS_WIKI), 2)
	return nb_verbes, nb_noms

def stats_extraction():
	mots, verbes, noms, dicoVerbs, dicoNames = html_lang.main_html("https://fr.wikipedia.org/wiki/France")
	separation_verbes_noms = round(Decimal(((len(verbes) + len(noms)) * 100) / len(mots)), 2)
	doublons_mots = round(Decimal(calcule_nb_doublons(mots) * 100 / len(mots)), 2)
	doublons_verbes = round(Decimal(calcule_nb_doublons(verbes) * 100 / len(verbes)), 2)
	doublons_noms = round(Decimal(calcule_nb_doublons(noms) * 100 / len(noms)), 2)
	return separation_verbes_noms, doublons_mots, doublons_verbes, doublons_noms


if __name__ == '__main__':
	sep, dbl_mots, dbl_verbes, dbl_noms = stats_extraction()
	print('Le pourcentage de verbes + noms par rapport aux mots extraits est de: {} %'.format(sep))
	print('Le pourcentage de doublons dans les mots est de: {} %'.format(dbl_mots))
	print('Le pourcentage de doublons dans les verbes est de: {} %'.format(dbl_verbes))
	print('Le pourcentage de doublons dans les noms est de: {} %'.format(dbl_noms))
	nb_verbes, nb_noms = stats_tiroirs()
	print('Le pourcentage de verbes qui serviront pour la construction des modèles par rapport aux nombre de variations dans le bescherelle est de : {} %'.format(nb_verbes))
	print('Le pourcentage de noms qui serviront pour la construction des modèles par rapport aux nombre de variations dans la liste établie après nos recherches sur internet est de : {} %'.format(nb_noms))
	nb_mod_final, complet = stats_mod('modelsverbs')
	nb_modeles_noms_final, compl_noms = stats_mod('modelsnames')
	print('Le pourcentage de modeles verbes obtenus par rapport au Bescerelle est de: {} %'.format(nb_mod_final))
	print('Le pourcentage de modeles noms obtenus par rapport à la liste établie est de: {} %'.format(nb_modeles_noms_final))
	print('Le pourcentage de modeles verbes complets obtenus est de : {} %'.format(complet))
	print('Le pourcentage de modeles noms complets obtenus est de : {} %'.format(compl_noms))
