#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#Utilisation :
# ./build_tiroir.py https://fr.wikipedia.org/wiki/Wiki
#Lancement des test :
# ./build_tiroir.py -t

try:
    from data_fetch import *
except ImportError :
    from TiroirLemmeAnew.data_fetch import *

try:
    from build_lemme import main_lem
except ImportError :
    from TiroirLemmeAnew.build_lemme import main_lem

try:
    from html_lang import *
except ImportError :
    from TiroirLemmeAnew.html_lang import *

try:
    from clustering import main_clust
except ImportError :
    from TiroirLemmeAnew.clustering import main_clust

from pprint import pprint
import operator
import sys

tenses = ['infinitif', 'indpres', 'indimpar', 'indfut', 'simple', 'subjpres', 'subjimpar', 'cond']
gender = ['masculine', 'feminine']

def ending(word, stems):
	"""Permet de dégager le suffixe à partir d'un verbe conjugué et d'une liste de radicaux.
	Le radical le plus long compatible est utilisé.

Args:
	word (str): le verbe conjugué.
	stems (list) : la liste des radicaux de ce verbe, classée par longueur décroissante.

Returns:
	le suffixe ou 0.
"""

	for elt in stems:
		if word and elt in word:
			return (word[len(elt):], word[:len(elt)])
	return 0

def construct(words_data, cluster):
	"""Construit le dictionnaire des terminaisons classé par verbe et par temps.

Args:
	words_data (list): la liste des verbes conjugués et les données grammaticales. Au format:
	[[conjugaison1, temps, personne], ...]
	cluster (dict): la liste des conjugaisons d'un verbe classée par verbe. A la fin de cette liste on a
	inséré la liste des radicaux retenus. Au format:
	{ verbe1 : [conjugaison1, conjugaison2, ..., [radical1, radical2, ...]], ...}

Returns:
	result (list): deux dictionnaires; 0 dans la structure signifie absence de données
		classé par verbe puis par temps, la liste des suffixes; Au format:
	{ verbe1 : { temps1 : [suffixe1, suffixe2, ...], ...}, ...}
		classé par verbe puis par temps, la liste des tuples (flexion, radical); Au format:
	{ verbe1 : { temps1 : [(flexion1, radical1), (flexion2, radical2), ...], ...}, ...}
	"""

	result = {}
	complete = {}

	for k in cluster.keys():

		result[k] = {}
		complete[k] = {}
		for tense in tenses:
			if tense != 'infinitif':
				result[k][tense] = [0] * 6
				complete[k][tense] = [0] * 6
			else:
				result[k][tense] = [0]
				complete[k][tense] = [0]

		to_sort = cluster[k][:-1]
		to_sort.sort()
		stems = cluster[k][-1]
		stems.sort(key=len, reverse=True)

		for word in to_sort:
			end = ending(word, stems)
			for data in words_data:
				if end and data[0] == word and data[2] > 0:
					result[k][data[1]][data[2] - 1] = end[0]
					complete[k][data[1]][data[2] - 1] = end
				elif end and data[0] == word and data[2] == 0:
					result[k][data[1]][0] = end[0]
					complete[k][data[1]][0] = end

	return [result, complete]

def constructNames(words_data, cluster):
	"""Construit le dictionnaire des terminaisons classé par nom et par genre.

Args:
	words_data (list): la liste des noms et les données grammaticales. Au format:
	[[nom1, genre, nombre], ...]
	cluster (dict): la liste des flexions d'un nom classée par nom. A la fin de cette liste on a
	inséré la liste des radicaux retenus. Au format:
	{ nom1 : [flexion1, flexion2, ..., [radical1, radical2, ...]], ...}

Returns:
	result (list): deux dictionnaires; 0 dans la structure signifie absence de données
		classé par nom puis par genre, la liste des suffixes; Au format:
	{ nom1 : { genre1 : [suffixe1, suffixe2, ...], ...}, ...}
		classé par nom puis par genre, la liste des tuples; Au format:
	{ nom1 : { genre1 : [(flexion1, radical1), (flexion2, radical2)], ...}, ...}
	"""

	result = {}
	complete = {}

	for k in cluster.keys():

		result[k] = {}
		complete[k] = {}
		for gen in gender:
			result[k][gen] = [0] * 2
			complete[k][gen] = [0] * 2

		to_sort = cluster[k][:-1]			#flexions
		to_sort.sort()
		to_sort.sort(key=len)
		stems = cluster[k][-1]				#radicaux
		stems.sort(key=len, reverse=True)

		for word in to_sort:
			end = ending(word, stems)
			#Compense certain clusters de noms qui n'ont pas de racine (Bobo)
			if end == 0 :
				end = ("", "")
			for data in words_data:
				if data[0] == word and data[2]:
					if not(result[k][data[1]][data[2] - 1]):
						result[k][data[1]][data[2] - 1] = end[0]
					if not(complete[k][data[1]][data[2] - 1]):
						complete[k][data[1]][data[2] - 1] = end

	return [result, complete]

def test_tir(tup=(), tup_list=[]):
	"""Teste si la valeur tup est redondante dans tup_list.

Args:
	tup (tuple): le tuple à tester au format:
	(terminaison, temps, flexion, personne)
	tup_list (list): la liste des tuples au format:
	[(terminaison1, temps, flexion, personne), ...]

Returns:
	ok (int): 1 si l'élément n'est pas redondant, 0 sinon.
	"""

	ok = 1
	for elt in tup_list:
		if tup[0] == elt[0] and tup[1] == elt[1] and tup[3] == elt[3]:
			ok = 0
	return ok

def build_tiroirList(sorted_suff):
	"""Construction de la liste de tuples pour le fichier tiroirBrut.

Args:
	sorted_suff (dict): contient les suffixes classés par verbes et par temps au format :
	{ verbe : { temps : [personne1, personne2, ...], ...}, ...}
	A noter que les personnes peuvent être remplacées par la valeur 0 en cas d'absence d'entrée.

Returns:
	mem (list): liste de tuples à imprimer dans tiroirBrut. Ordonnée par temps, pis flexion, puis personne.
	Au format :
	[(terminaison1, temps, flexion, personne), ...]
	"""

	mem = []

	number = {}
	for tense in tenses[1:]:
		number[tense] = [1] * 6

	for k in sorted_suff.keys():
		for tense in tenses[1:]:
			to_build = sorted_suff[k][tense]
			i = 0
			for elt in to_build:
				i += 1
				if elt != 0:
					tmp = (elt+'-', tense, number[tense][i - 1], i)
					if test_tir(tmp, mem):
						mem.append(tmp)
						number[tense][i - 1] += 1

	mem = sorted(mem, key=operator.itemgetter(1, 2, 3))

	return mem


def build_names_tiroirList(sorted_suff):
	"""Construction de la liste de tuples pour le fichier tiroirBrut.

Args:
	sorted_suff (dict): classé par nom puis par genre, la liste des suffixes; Au format:
	{ nom1 : { genre1 : [suffixe1, suffixe2, ...], ...}, ...}
	A noter que les personnes peuvent être remplacées par la valeur 0 en cas d'absence d'entrée.

Returns:
	mem (list): liste de tuples à imprimer dans tiroirBrut. Ordonnée par genre, puis flexion, puis nombre.
	Au format :
	[(terminaison1, genre, flexion, nombre), ...]
	"""

	mem = []

	number = {}
	for gen in gender:
		number[gen] = [1] * 2

	for k in sorted_suff.keys():
		for gen in gender:
			to_build = sorted_suff[k][gen]
			i = 0
			for elt in to_build:
				i += 1
				if elt != 0:
					tmp = (elt+'-', gen, number[gen][i - 1], i)
					if test_tir(tmp, mem):
						mem.append(tmp)
						number[gen][i - 1] += 1

	mem = sorted(mem, key=operator.itemgetter(1, 2, 3))

	return mem



def main_tir(url = None, saveTest = False):
	"""Mise en oeuvre de la construction du fichier.

Args:
	url (str): l'url à crawler au départ
	saveTest (bool): falg pour autoriser la construction des fichiers pickles

Returns:
	to_save (list): liste de tuples (verbes) à imprimer dans tiroirBrut. Ordonnée par temps, pis flexion, puis personne.
	Au format :
	[(terminaison1, temps, flexion, personne), ...]

	names_to_save (list): liste de tuples (noms) à imprimer dans tiroirBrut. Ordonnée par genre, puis flexion, puis nombre.
	Au format :
	[(terminaison1, genre, flexion, nombre), ...]

	sorted_suff_complete (dict): dictionnaire des verbes classé par verbe puis par temps, la liste des tuples (flexion, radical); Au format:
	{ verbe1 : { temps1 : [(flexion1, radical1), (flexion2, radical2), ...], ...}, ...}

	names_sorted_suff_complete (dict): dictionnaire des noms classé par nom puis par genre, la liste des tuples; Au format:
	{ nom1 : { genre1 : [(flexion1, radical1), (flexion2, radical2)], ...}, ...}
	"""

	print("Récupération des verbes et des noms")
	LIMIT_VERBES = 4000
	LIMIT_NOMS = 4000 
	mots, verbes, noms, dictVerbs, dictNoms = main_html(url)
	noms.sort()
	verbes.sort()
	print("\nFormation des clusters\n")
	gFamiliesVerbs, gFamiliesNames = main_clust(verbes, noms)

	print("\nConstruction des variables nécessaires\n")
	gLemmeBrutVerbs, gLemmeBrutNames, final_clustVerbs, final_clustNames = main_lem(gFamiliesVerbs, gFamiliesNames, False)

	print("\nConstruction de tiroirBrut\n")
	names_sor = constructNames(noms, final_clustNames)
	names_sorted_suff = names_sor[0]
	names_sorted_suff_complete = names_sor[1]

	sor = construct(verbes, final_clustVerbs)
	sorted_suff = sor[0]
	sorted_suff_complete = sor[1]

	names_to_save = build_names_tiroirList(names_sorted_suff)
	to_save = build_tiroirList(sorted_suff)

	print("\nSauvegarde des résultats dans la base de données")

	if saveTest :
		#Sauvegarde des structure après l'utilisation de valid_pairs
		write_pickle(names_sorted_suff, "tests/names_sorted_suff.pickle")
		write_pickle(names_sorted_suff_complete, "tests/names_sorted_suff_complete.pickle")
		write_pickle(names_to_save, "tests/names_to_save.pickle")

		write_pickle(sorted_suff, "tests/sorted_suff.pickle")
		write_pickle(sorted_suff_complete, "tests/sorted_suff_complete.pickle")
		write_pickle(to_save, "tests/to_save.pickle")
		
	return to_save, names_to_save, sorted_suff_complete, names_sorted_suff_complete

if __name__ == '__main__':
	if "-t" in sys.argv :
		to_save, names_to_save, sorted_suff_complete, names_sorted_suff_complete = main_tir("https://fr.wikipedia.org/wiki/France", True)

		print("tiroirBrut des verbes")
		#pprint(to_save)

		print("tiroirBrut des noms")
		#pprint(names_to_save)
		#pprint(names_sorted_suff_complete)
	else:
		to_save, names_to_save, sorted_suff_complete, names_sorted_suff_complete = main_tir(sys.argv[1], None)

		print("tiroirBrut des verbes")
		#pprint(to_save)

		print("tiroirBrut des noms")
		#pprint(names_to_save)
		
