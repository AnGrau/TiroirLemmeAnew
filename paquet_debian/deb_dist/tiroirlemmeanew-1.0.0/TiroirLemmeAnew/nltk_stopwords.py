#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# Auteur: Alejandro Nolla - z0mbiehunt3r
# adapté par Anthony Grau
# Objet: exemple de détection de langue en utilisant l'approche basée sur les stopwords
# Création: 15/05/13, modifications 20/04/2017

import sys

try:
    from nltk import wordpunct_tokenize
    from nltk.corpus import stopwords
except ImportError:
    print ("[!] Vous devez installer nltk (http://nltk.org/index.html)")



#----------------------------------------------------------------------
def _calculate_languages_ratios(text):
    """
    Calcule la probabilité de plusieurs langages pour un texte donné.
    retourne un dictionnaire {'french': 2, 'spanish': 4, 'english': 0}
    
Args:
    text(str): Texte dont on veut déterminer la langue
    
Returns:
    languages_ratios(dict): Dictionnaire avec les différents langages et leur score (fonction des stopwords)
    """

    languages_ratios = {}

    '''
    nltk.wordpunct_tokenize() sépare en fonction de la ponctuation
    
    >>> wordpunct_tokenize("That's thirty minutes away. I'll be there in ten.")
    ['That', "'", 's', 'thirty', 'minutes', 'away', '.', 'I', "'", 'll', 'be', 'there', 'in', 'ten', '.']
    '''

    tokens = wordpunct_tokenize(text)
    words = [word.lower() for word in tokens]

    # Calcul des stopwords des langages connus de nltk
    for language in stopwords.fileids():
        stopwords_set = set(stopwords.words(language))
        words_set = set(words)
        common_elements = words_set.intersection(stopwords_set)

        languages_ratios[language] = len(common_elements) # language "score"

    return languages_ratios


#----------------------------------------------------------------------
def detect_language(text):
    """
    Calcule la probabilité de différents langages pour un texte donné et 
    renvoie le plus probable.
    
    Utilise une approche basée sur les stopwords qui compte leurs occurences dans le texte.
    
Args:
    text(str): texte à analyser
    
Returns:
    most_rated_language(str): langage le plus probable
    """

    ratios = _calculate_languages_ratios(text)

    result = None
    val = 0

    for key, value in ratios.items():
        if value > val:
            result = key
            val = value

    most_rated_language = result

    return most_rated_language



if __name__=='__main__':

    """sudo pip3 install -U nltk
    Option: sudo pip3 install -U numpy

    Installez stopwords avant ou il y aura une erreur:
    
    Resource u’corpora/stopwords’ not found. Please use the NLTK
    Pour obtenir la ressource:
    Tout
    >>> nltk.download()

    Ou seulement stopwords:
    >>> python3
    >>> import nltk
    >>> nltk.download("stopwords")
    """

    text = open(sys.argv[1], "r").read()

    language = detect_language(text)

    print (language)
