#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#Utilisation :
# ./clustering.py https://fr.wikipedia.org/wiki/Wiki
#Lancement des test :
# ./clustering.py -t

try:
    from html_lang import *
except ImportError :
    from TiroirLemmeAnew.html_lang import *

try:
    from data_fetch import erreur, write_pickle
except ImportError :
    from TiroirLemmeAnew.data_fetch import erreur, write_pickle

from pprint import pprint
from threading import Thread, RLock
import concurrent.futures
from operator import itemgetter
import itertools
import sys

def transcaracter(mot) :
	"""
		Transformation des caractères spéciaux pour le bon fonctionnement de l'algorithme de Gaussier
		
		Arg :
		mot (str) : Chaîne de caractères avant transformation
		
		Return :
		mot (str) : Chaîne de caractères après transformation
	"""
	caracters = {"ç":"c", "è":"e", "ê":"e", "ï":"i", "î":"i", "y":"i", "é":"e", "û":"u"}
	doubles = {"ge":"g", "ll":"l", "qu":"c", "tt":"t"}
	for c in caracters :
		if c in mot :
			mot = mot.replace(c, caracters[c])
	for d in doubles :
		if d in mot :
			mot = mot.replace(d, doubles[d])
	return mot	
		
	
	
def p_similar(word1, word2):
	"""Calcule le nombre de lettres communes aux deux mots.

	Args:
	    word1 (str): le premier mot
	    word2 (str): le second mot

	Return:
	    similarity_score (int): le nombre de lettres communes"""
	
	similarity_score = 0

	if len(word1) < len(word2):
		temp1 = word1
		temp2 = word2
	else:
		temp1 = word2
		temp2 = word1

	for x in range(len(temp1)):
		if temp1[x] == temp2[x]:
			similarity_score += 1
		else:
			break

	return similarity_score

	
def extract_suffixes_pairs(conj) :
	"""
		Construction à partir d'une liste de mots (et de leurs grammaire), d'une liste de paires de pseudo-suffixes
		
		Arg:
		conj (list) : Liste de mots et de leur grammaire
		
		Return
		pairs (list) : Liste des différentes paires de pseudo-suffixes
	"""
	if type(conj) != list :
		return erreur("extract_suffixes_pairs : Erreur paramètre conj incorrecte", None)
	
	similiseuil = 1
	
	pairs = []
	similitude = 0
	try :
		compteur = 0
		starseuil = 1
		for i in range(len(conj)) :
			corrected1 = transcaracter(conj[i][0])
			for j in range(i+1, len(conj)) :
				corrected2 = transcaracter(conj[j][0])
				similitude = p_similar(corrected1, corrected2)
				if similitude < similiseuil :
					break
				if len(corrected1) == len(conj[i][0]) and len(corrected2) == len(conj[j][0]) :
					pairs.append([conj[i], conj[j], similitude, conj[i][0][similitude:], conj[j][0][similitude:]])
				else:
					pairs.append([conj[i], conj[j], similitude, corrected1[similitude:], corrected2[similitude:]])
	except :
		return erreur("extract_suffixes_pairs : Erreur pendant la construction de la liste des pseudo-suffixes", None)
	
	return pairs

def build_suffix_dict(suffix_dict,pseudo_suffix_pairs, similitude = False) :
	"""
		Construction d'un dictionnaire pour la validation des paires de pseudo-suffixes. Le dictionnaire présente la forme suivante :
		suffix_dict[pairSuff1][pairSuff2] = [cooccurrence, cooccurrence_total]
		cooccurrence : correspond à la valeur des cooccurrences sachant que seul les mots ayant une p-similitude de 5 minimum sont pris en compte
		cooccurrence_total : correspond à la valeur des cooccurrences en prenant en compte tous les mots 
	
		Args:
		suffix_dict (dict) : Le dictionnaire à construire
		pseudo_suffix_pairs (list) : La liste des paires de pseudo-suffixes
		similitude : Un flag qui permet d'activer le comptage des cooccurrences à la place des cooccurrences_total
	
		Return:
		suffix_dict (dict) : Le dictionnaire mit à jour
	"""
	
	if type(suffix_dict) != dict :
		return erreur("build_suffix_dict : paramètre suffix_dict incorrect : {}".format(suffix_dict), None)
	
	if type(pseudo_suffix_pairs) != list :
		return erreur("build_suffix_dict : paramètre suffix_dict incorrect : {}".format(pseudo_suffix_pairs), None)
		
	if type(similitude) != bool :
		similitude = erreur("build_suffix_dict : paramètre similitude incorrect : {}, changement vers le paramètre par défaut".format(similitude), False)
	try :
		for pair in pseudo_suffix_pairs :
			if pair[3] not in suffix_dict.keys() :
				suffix_dict[pair[3]] = {}
			if pair[4] not in suffix_dict[pair[3]].keys() :
				suffix_dict[pair[3]][pair[4]] = [0, 0]
			if similitude and pair[2] >= 5: #Si la p-similitude est supérieure ou égale à 5
				suffix_dict[pair[3]][pair[4]][0] += 1
			else :
				suffix_dict[pair[3]][pair[4]][1] += 1
	except :
		return erreur("build_suffix_dict : Echec de construction du dictionnaire pour la paire {}".format(pair), None)
	
	return suffix_dict
	
def valid_pairs(pairs, check_pairs) :
	"""
		Passage en revue de toutes les paires de pseudo-suffixes. Sont validées celles dont le nombre de co-occurrence est > 1 dans l'arbre des terminaisons check_pairs contenant uniquement des terminaisons dont la p-similitude est supérieure à 5
		
		Args:
		pairs (list) : Liste dont chaque composante est une sous liste de la forme [mot1, mot2, p-similitude, suffix1, suffix2]
		check_pairs (dict) : Dictionnaire contenant l'ensemble des suffixes
		
		Returns :
		words_score_stem (dict) : dictionnaire contenant les paires de suffixes valides, leurs score de coocurrences totale et leurs racines
		None : La fonction retourne None en cas d'erreur
	"""

	if type(pairs) != list :
		return erreur("valid_pairs : Paramètre pairs incorrect {}".format(pairs), None)
	if type(check_pairs) != dict :
		return erreur("valid_pairs : Paramètre check_pairs incorrect {}".format(check_pairs), None)
		
	cooccur = 0
	total_score = 0
	cooccur_seuil = 1
	words_score_stem = {}
	try :
		for elt in pairs :
			cooccur, total_score = check_pairs[elt[3]][elt[4]]
			if cooccur < cooccur_seuil :
				continue
			elt.append(total_score)
			if elt[0][0] >= elt[1][0]:
				a = (elt[1][0], elt[0][0])
				suff1 = len(elt[4])
				suff2 = len(elt[3])
				if suff1:
					stem1 = elt[1][0][:-suff1]
				else:
					stem1 = elt[1][0]
				if suff2:
					stem2 = elt[0][0][:-suff2]
				else:
					stem2 = elt[0][0]
			else:
				a = (elt[0][0], elt[1][0])
				suff1 = len(elt[3])
				suff2 = len(elt[4])
				if suff1:
					stem1 = elt[0][0][:-suff1]
				else:
					stem1 = elt[0][0]
				if suff2:
					stem2 = elt[1][0][:-suff2]
				else:
					stem2 = elt[1][0]
			if stem1 == stem2:
				words_score_stem[a] = [total_score, stem1]
			else:
				words_score_stem[a] = [total_score, stem1, stem2]
	except :
		return erreur("valid_pairs : Erreur dans la recherche de suffix_pairs valides {}".format(elt), None)
	
	return words_score_stem

def cluster(score_stem):
	"""Construction des familles de mots.

Args:
	score_stem (dict): clé le tuple couple de mots ordonné, valeur (list) la liste contenant les occurrences totales et le radical.
						Format : {(mot1, mot1): [occurrences_totales_paires, radical], ... }

Return:
	listFamily (list): la liste contenant les sous-listes constituant les familles
	"""

	listFamily = []
	conjList = []

	#~~~~~~~~~~ Création de la liste ordonnée des mots à tester ~~~~~~~~~~

	theKeys = score_stem.keys()
	for tup in theKeys:
		for word in tup:
			if word not in conjList:
				conjList.append(word)

	conjList.sort()

	#~~~~~~~~~~ Itération sur la liste ~~~~~~~~~~

	while (conjList):

		family = []
		radList = []
		score = -1
		star = ()
		rad1 = ""
		rad2 = ""

		#~~~~~~~~~~ Initialisation de la famille ~~~~~~~~~~

		for elt in score_stem.keys():
			if conjList[0] in elt:
				if score < score_stem[elt][0]:
					score = score_stem[elt][0]
					star = elt

		radicaux_init = score_stem[star]
		rad1 = radicaux_init[1]
		if len(radicaux_init) == 3:
			rad2 = radicaux_init[2]

		if star[0] in conjList:
			radList.append(rad1)
			family.append(star[0])
			index_1 = conjList.index(star[0])
			conjList.pop(index_1)
		if star[1] in conjList:
			if rad2:
				radList.append(rad2)
			family.append(star[1])
			index_2 = conjList.index(star[1])
			conjList.pop(index_2)

		p_similar = 1	# flag pour continuer

		#~~~~~~~~~~ Assemblage des familles ~~~~~~~~~~

		while p_similar and conjList:

			to_del = []

			for word_to_test in conjList:	#itération sur la liste de mots à  tester

				tested = 0
				to_test = word_to_test
				to_add_rad = []

				for word in family:			# itération sur la famille

					if to_test >= word:
						to_search = (word, to_test)
						if to_search in score_stem.keys() and len(score_stem[to_search]) == 3:
							to_search_value = score_stem[to_search]
							if to_search_value[2] not in to_add_rad:
								to_add_rad.append(to_search_value[2])
						elif to_search in score_stem.keys():
							to_search_value = score_stem[to_search]
							if to_search_value[1] not in to_add_rad:
								to_add_rad.append(to_search_value[1])
					else:
						to_search = (to_test, word)
						if to_search in score_stem.keys():
							to_search_value = score_stem[to_search]
							if to_search_value[1] not in to_add_rad:
								to_add_rad.append(to_search_value[1])
					if to_search in score_stem.keys():	# on cherche les couples de mots
						tested += 1						# s'il est présent +1 testé
						continue
					else:
						break

				#~~~~~~~~~~ Si le nombre testé est égal à la longueur de family et que le mot n'est pas présent ~~~~~~~~~~

				if tested == len(family) and to_test not in family:
					for rad in to_add_rad:
						if rad not in radList:
							radList.append(rad)
					family.append(to_test)	# on ajoute
					to_del.append(to_test)	# on mémorise pour effacer

			if to_del:
				p_similar = 1	# on continue tant qu'il y a des mots à effacer
			else:
				p_similar = 0	# sinon on arrête

			tmp = []

			#~~~~~~~~~~ Suppression des éléments ~~~~~~~~~~

			for elt in conjList:
				if elt not in to_del:
					tmp.append(elt)
			conjList = tmp

		#~~~~~~~~~~ Consitution du cluster ~~~~~~~~~~

		radList_sort = sorted(radList, key=len, reverse=True)
		family.append(radList_sort)
		listFamily.append(family)

	return listFamily

def separe_noms(familiesNames):
	"""Cette fonction créée des familles séparées pour les noms de même racine qui ne devraient pas être regroupés.

Args:
	familiesNames (list): la liste des cluster (familles)

Return:
	new_clusts (list): la liste des familles avec les différents noms (de même racine) séparés.
	"""
	new_clusts  = []
	for clust in familiesNames:
		rad = clust[-1]
		names = clust[:-1]
		rad.sort(key=len, reverse=True)
		names.sort(key=len, reverse=True)
		compute = len(names[0]) - len(names[-1])
		while rad and names:
				temp = []
				to_test = rad[0]
				rad.pop(0)
				for name in names:
					if to_test in name:
							temp.append(name)
				if temp:
					for elt in temp:
						names.remove(elt)
					temp.sort()
					temp.append([to_test])
					new_clusts.append(temp)
	return new_clusts
	
def main_clust(lverbs, lnames, saveTest = False) :

	lwords = [lverbs, lnames]
	familiesVerbs = []
	familiesNames = []
	
	for lword in lwords :	
		if lword is lverbs :
			print("Traitement des verbes\n")
		else :
			print("\nTraitement des noms\n")
	
		print("Recherche des paires de pseudo-suffixes")
		pseudo_suffix_pairs = extract_suffixes_pairs(lword)
		if pseudo_suffix_pairs == None :
			exit("Echec de la recherche des paires de pseudo-suffixes")
	
		print("Construction Dictionnaire des pseudo-suffixes")
		suffScor = {}
		suffScor = build_suffix_dict(suffScor, pseudo_suffix_pairs, False)
		if suffScor == None :
			exit("Echec de construction du dictionnaire pour les scores de similarité < 5")
		
		suffScor = build_suffix_dict(suffScor, pseudo_suffix_pairs, True) #Score de similarité >= 5
		if suffScor == None :
			exit("Echec de construction du dictionnaire pour les scores de similarité >= 5")
	
		print("Validation des paires de pseudo-suffixes")
		score_stem = valid_pairs(pseudo_suffix_pairs,suffScor)
		if score_stem == None :
			exit("Echec de validation des paires de pseudo_suffixes")
	
		print("Création des familles")
		families = cluster(score_stem)
		if families == None :
			exit("Echec de la création des familles")
		
		if lword is lverbs :
			familiesVerbs = families
		else :
			familiesNames = separe_noms(families)
		
		if saveTest == True :
			print("Sauvegarde des fichiers pour les tests")
			if lword is lverbs :
				write_pickle(score_stem, "tests/validVerbs.pickle")
				write_pickle(pseudo_suffix_pairs, "tests/pSuffixVerbs.pickle")
				write_pickle(familiesVerbs, "tests/familiesVerbs.pickle")
			else :
				write_pickle(score_stem, "tests/validNames.pickle")
				write_pickle(pseudo_suffix_pairs, "tests/pSuffixNames.pickle")
				write_pickle(familiesNames, "tests/familiesNames.pickle")
		
	return familiesVerbs, familiesNames
			
if __name__ == "__main__" :
	if "-t" in sys.argv :
		print("Récupération des verbes et des noms")
		LIMIT_VERBES = 4000
		LIMIT_NOMS = 4000 
		mots, verbes, noms, dictVerbs, dictNoms = main_html("https://fr.wikipedia.org/wiki/France")
		verbes.sort()
		noms.sort()
		print("Formation des clusters\n")
		gFamiliesVerbs, gFamiliesNames = main_clust(verbes, noms, True)
		
		print("Familles des verbes")
		#pprint(gFamiliesVerbs)
		
		print("Familles des noms")
		#pprint(gFamiliesNames)
	
	elif len(sys.argv) == 2:
		print("Récupération des verbes et des noms")
		mots, verbes, noms, dictVerbs, dictNoms = main_html(sys.argv[1])
		verbes.sort()
		noms.sort()
		print("Formation des clusters\n")
		gFamiliesVerbs, gFamiliesNames = main_clust(verbes, noms)
		
		print("Familles des verbes")
		#pprint(gFamiliesVerbs)
		print("\n----------\n")
		print("Familles des noms")
		#pprint(gFamiliesNames)
		
		
	else:
		exit("Un argument, l'url de la page à crawler.")



