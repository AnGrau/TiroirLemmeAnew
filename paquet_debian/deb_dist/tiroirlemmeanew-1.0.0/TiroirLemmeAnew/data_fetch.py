#! /usr/bin/env python3
# -*- coding: utf-8 -*-


from lxml import etree
from sys import stderr, argv
from pprint import pprint
import pickle
from operator import itemgetter
import os

def erreur(msg, retour) :
	"""
		Affichage des messages d'erreur et retour d'un paramètre en code erreur
		Args :
		msg (str) : Le message d'erreur
		retour (tous les types) : le code erreur à retourner
	
		Return
		retour (tous les types) : le code erreur à retourner
	"""
	stderr.write(msg + "\n")
	return retour
	
#Liste globale servant de stoplist lorsqu'un nouveau mot est taggé dans notre système il est ajouté à cette stoplist pour ne plus être ajouté plusieurs fois dans notre système.
lstoplist = set()

def word_tags(dico_tag_verbs, dico_tag_names, lverbs, lnames, wordlist, readNames = True, readVerbs = True, purge_stoplist = False) :
	"""
		Cette fonction réalise le tag des mots récupéré dans le Corpus pour attribuer un temps et une personne de conjugaison au verbes, un genre et un nombre aux noms.
		
		Args :
		dico_tag_verbs (dict) : Ensemble des tags possible pour les verbes
		dico_tag_names (dict) : Ensemble des tags possible pour les noms
		lverbs (list) : Liste des verbes taggés qui seront utilisés pour construire lemmeBrut et tiroirBrut
		lnames(list) : Liste des noms taggés qui seront utilisés pour construire lemmeBrut et tiroirBrut
		wordlist (list) : Liste des mots à tagger
		readNames (bool) : flag permettant d'activer ou de désactiver le tag des noms
		readVerbs (bool) : flag permettant d'activer ou de désactiver le tag des verbes
		purge_stoplist (bool) : flag permettant d'activer la purge de la stoplist pour les tests unitaires
		
		Returns :
		lverbs : Liste de sous-listes contenant l'ensemble des verbes et leurs tags
		lnames : Liste de sous-listes contenant l'ensembles des noms et  leurs tags
		En cas d'erreur la fonction retourne le tuple (None, None)
	"""

	if (type(dico_tag_verbs) != dict) or (type(dico_tag_names) != dict) :
		return erreur("word_tags :Erreur, un des dictionnaires en  entrée de la fonction est incorrect", (None, None))
	if (type(wordlist) != list) or (type(lverbs) != list) or (type(lnames) != list) :
		return erreur("word_tags : Erreur une des listes d'entrée est incorrecte", (None, None))
	if (type(readNames) != bool) or (type(readVerbs) != bool) :
		return erreur("word_tags : Erreur paramètre readNames ou readVerbs est incorrect", (None, None))
		
	if dico_tag_verbs == {} or dico_tag_verbs == {} :
		return erreur("word_tags : Erreur un des dictionnaire d'entrée est vide", (None, None))
	dico_tag_names = dict(dico_tag_names)
	global lstoplist
	if purge_stoplist :
		lstoplist = set()
	
	for word in wordlist :
		word = word.lower()
		if word == '' or word in lstoplist:
			continue
		
		if readVerbs and (word in dico_tag_verbs.keys()):
			for v in dico_tag_verbs[word] :
				if v[0].isalpha() == True:
					lverbs.append(v)
	
		if readNames and (word in dico_tag_names.keys()) :
			if len(dico_tag_names[word]) > 2 :
				dico_tag_names[word] = dico_tag_names[word][:2]
			for n in dico_tag_names[word] :
				if n[0].isalpha() == True:
					if n[-1] == 0 :
						tmp1 = n[:-1] + [1]
						tmp2 = n[:-1] + [2]
						lnames.append(tmp1)
						lnames.append(tmp2)
					else :
						lnames.append(n)
	
		lstoplist.add(word)
	return lverbs, lnames
	
def build_tags(filename) :
	"""
		Extraction des noms et des verbes du dictionnaire électronique et construction des tags des noms et des verbes
		
		Arg :
		filename (str) : chemin d'accès vers le dictionnaire électronique au format .xml
		
		Returns :
		dico_tag_verbs (dict) : Dictionnaire contenant tous les verbes du dictionnaire éléctronique et les tags possibles pour ces verbes
		dico_tag_names (dict) : Dictionnaire contenant tous les noms communs du dictionnaire éléctronique et les tags possibles pour ces noms
	"""
	
	if type(filename) != str :
		return erreur("build_tags : Erreur paramètre incorrect", (None, None))
	if filename[-len(".xml"):] != ".xml" :
		return erreur("build_tags : Erreur fichier dictionnaire incorrect", (None, None))
	
	#parsing du dictionnaire électronique
	try :
		to_read_xml = open(os.path.join(os.path.dirname(__file__), filename))
		tree = etree.parse(to_read_xml)
	except : 
		return erreur("build_tags : Erreur échec de parsing du document .xml", (None, None))
	
	#Extraction des verbes et des noms	
	dico_tag_verbs = {}
	dico_tag_names = {}
	x_path = [0, 0, 0, 0]
	x_path[0] = "/lexicon/lexicalEntry/formSet/lemmatizedForm[grammaticalCategory='verb']/*"
	x_path[1] = "/lexicon/lexicalEntry/formSet[lemmatizedForm/grammaticalCategory='verb'][not(lemmatizedForm/grammaticalSubCategory)]/inflectedForm/*"
	x_path[2] = "/lexicon/lexicalEntry/formSet[lemmatizedForm/grammaticalCategory='commonNoun'][lemmatizedForm/grammaticalGender='masculine']/inflectedForm/*"
	x_path[3] = "/lexicon/lexicalEntry/formSet[lemmatizedForm/grammaticalCategory='commonNoun'][lemmatizedForm/grammaticalGender='feminine']/inflectedForm/*"
	
	#Construction des tags
	for i in range(len(x_path)) :
		lpatt = tree.xpath(x_path[i])	
		if i == 0 :
			for elt in lpatt :
				if elt.tag != "orthography" :
					continue
				key = find_attribute(elt)
				if key not in dico_tag_verbs.keys() :
					dico_tag_verbs[key] = []
				dico_tag_verbs[key] += [[elt.text, "infinitif", 0]]
		elif i == 1 :
			dico_tag_verbs = searchVerbFlex(lpatt, dico_tag_verbs)
		elif i == 2 :
			dico_tag_names = searchNameFlex(lpatt, "masculine", dico_tag_names)
		elif i == 3 :
			dico_tag_names = searchNameFlex(lpatt, "feminine", dico_tag_names)
		
		if dico_tag_verbs == None :
			return erreur("build_tags : Echec de tag au niveau des verbes", (None, None))
		if dico_tag_names == None :
			return erreur("build_tags : Echec de tag au niveau des noms", (None, None))
	
	to_read_xml.close()
	
	return dico_tag_verbs, dico_tag_names


def find_attribute(node) :
	"""
		Fonction appelée par build_tags. Elle va remonter plusieurs noeuds parents du noeud courant à la recherche d'un tag nommé "LexicalEntry". A hauteur de se tag on récupère un attribut qui n'est autre que la forme lemmatizée de la flexion en cours d'analyse. Cet attribut est alors retourné à la fonction build_tags
		
		Arg:
		node : Noeud courant à remonter
		
		Return:
		attribut (str) : Forme lemmatizée du verbe ou du nom trouvé
	"""
	while node.tag != "lexicalEntry" :
		node = node.getparent()
	
	attribut = node.get("id")
	attribut = attribut[:attribut.index("_")]
	return attribut
	

def searchVerbFlex(lpatt, dico_tag_verbs) :
	"""
		Construction des tags des verbes à partir des informations récupérées du dictionnaire électronique. Le résultat est retourné
		sous la forme d'un dictionnaire
		
		Args :
		lpatt (list) : Ensemble des noeuds récupérés du dictionnaire électronique sous la forme d'une liste qu'il va falloir traiter
		dico_tag_verbs (dict) : Dictionnaire des verbes récupérés du dictionnaire électronique et formatté pour pouvoir tagger le texte ensuite
		
		Returns :
		dico_tag_verbs (dict) : Dictionnaire des verbes récupérés du dictionnaire électronique mis à jour après le travail de la fonction
		None : La fonction retourne None en cas d'erreur
	"""
	
	if type(lpatt) != list :
		return erreur("searchVerbFlex : Erreur le paramètre lpatt est incorrect : {}".format(lpatt), None)
	if type(dico_tag_verbs) != dict :
		return erreur("searchVerbFlex : Erreur le paramètre dico_tag_verbs est incorrect : {}".format(dico_tag_verbs), None)
		
	sublist = []
	for i in range(len(lpatt)) :
		sublist.append(lpatt[i].text)
		if i + 1 < len(lpatt) :
			if lpatt[i+1].tag == "orthography" :
				cle = find_attribute(lpatt[i])
				key, pers, add = locateFlex(sublist)
				if sublist == None :
					print(key)	
				if key != "" :
					if (key == None) or (pers == -1) or (add == -1) :
						return erreur("searchFlex : Echec de la fonction avec la sublist {}".format(sublist), None)
					if cle not in dico_tag_verbs.keys() :
						dico_tag_verbs[cle] = []
					dico_tag_verbs[cle] += [[sublist[0], key, pers + add]]
				sublist = []
		else :
			cle = find_attribute(lpatt[i])
			key, pers, add = locateFlex(sublist)	
			if key != "" :
				if (key == None) or (pers == -1) or (add == -1) :
					return erreur("searchFlex : Echec de la fonction avec la sublist {}".format(sublist), None)
				if cle not in dico_tag_verbs.keys() :
					dico_tag_verbs[cle] = []
				dico_tag_verbs[cle] += [[sublist[0], key, pers + add]]
			sublist = []
	
	return dico_tag_verbs	


def searchNameFlex(lpatt, key, dico_tag_names) :
	"""
		Fonction de construction des tags des noms à partir des informations récupérées du dictionnaire électronique. Le résultat est retourné sous
		la forme d'un dictionnaire
		
		Args :
		lpatt (list) : Ensemble des noeuds récupérés du dictionnaire électronique sous la forme d'une liste qu'il va falloir traiter
		key (str) : Désigne le genre masculin ou féminin des noms à tagger
		dico_tag_names (dict): Dictionnaire des verbes récupérés du dictionnaire électronique et formatté pour pouvoir tagger le texte par la suite
		
		Return :
		dico_tag_names (dict):  Dictionnaire des verbes récupérés du dictionnaire électronique mis à jour après le travail de la fonction. La fonction retourne None en cas d'erreur
	"""

	if type(lpatt) != list :
		return erreur("searchNameFlex : Erreur paramètre lpatt est incorrect : {}".format(lpatt), None)
	if type(key) != str :
		return erreur("searchNameFlex : Erreur paramètre key est incorrect : {}".format(key), None)
	if type(dico_tag_names) != dict :
		return erreur("searchNameFlex : Erreur paramètre dico_tag_names est incorrect : {}".format(dico_tag_names), None)
	
	sublist = []
	nombre = -1
	
	for i in range(len(lpatt)) :
		if lpatt[i].tag == "orthography" :
			sublist.append(lpatt[i].text)
			sublist.append(lpatt[i + 1].text)
			if sublist[1] == "singular":
				nombre = 1
			elif sublist[1] == "plural":
				nombre = 2
			elif sublist[1] == "invariable":
				nombre = 0

			if key != "" :
				if (key == None) or (nombre == -1) :
					return erreur("searchFlex : Echec de la fonction avec la sublist {}".format(sublist), None)
				cle = find_attribute(lpatt[i])
				tmp = [sublist[0], key, nombre]
				if cle not in dico_tag_names.keys() :
					dico_tag_names[cle] = []
				dico_tag_names[cle] += [tmp]
					
			sublist = []
	return dico_tag_names

def locateFlex(sublist) :
	"""
		Cette fonction va détécter le temps de conjugaisons et la personne de conjugaison de la forme fléchie d'un verbe analysé du dictionnaire électronique
		
		Arg :
		sublist (list) : Une liste composée de la forme fléchie d'un verbe, si la forme est conjuguée au singulier ou au pluriel, Si le temps de conjugaison est indicatif, subjonctif ou conditionnel, la personne de conjugaison (de un à trois), les temps de conjugaison (présent, futur, imparfait, passé simple) ex : [aimons, plural, indicative, firstPerson, present]
		
		Return :
		key (str) : temps de conjugaison adapté pour l'analyseur morphologique
		pers (int) : Le numéro de personne de 1 à 3
		add (int) : 0 si la personne de conjugaison est au singulier et 3 au pluriel
	"""
	
	if type(sublist) != list :
		return erreur("locateFlex : Erreur le paramètre sublist est incorrecte", (None, None, None))
	
	#sublist est trop court : on ne retourne rien
	if len(sublist) <= 3 :
		return "", "", ""
	pers = -1
	add = -1
	key = ""
	
	#sublist contient un participe présent : on ne retourne rien
	if sublist[1] == "participle" :
		return "", "", ""
	
	#Si le verbe est conjugué au pluriel, on ajoute trois à la personne détectée. Au singulier on ajoute rien	
	if sublist[1] == "singular" :
		add = 0
	elif sublist[1] == "plural" :
		add = 3
	
	#Si sublist contient un participe passée : on ne retourne rien
	if sublist[2] == "participle" :
		return "", "", ""		
		
	#Détection de la personne de conjugaison
	if sublist[3] == "firstPerson" :
		pers = 1
	elif sublist[3] == "secondPerson" :
		pers = 2
	elif sublist[3] == "thirdPerson" :
		pers = 3
	
	#Détection du temps de conjugaison
	if sublist[2] == "indicative" :
		if sublist[4] == "present" :
			key = "indpres"
		elif sublist[4] == "future" :
			key = "indfut"
		elif sublist[4] == "imperfect" :
			key = "indimpar"
		elif sublist[4] == "simplePast" :
			key = "simple"
	elif sublist[2] == "subjunctive" :
		if sublist[4] == "imperfect" :
			key = "subjimpar"
		elif sublist[4] == "present" :
			key = "subjpres" 
	elif sublist[2] == "conditional" :
		key = "cond"
	
	return key, pers, add
	
def write_pickle(data, filename) :
	"""
		Ecriture d'une structure de donnée dans un fichier sous une forme binaire
		
		Args:
		data : Donnée sous une forme quelconque à enregistrer
		filename (str) : Chemin d'accès vers le fichier dans lequel écrire
		
		Return :
		0 (int) : Si tout c'est bien passé
		-1 (int) : Si une erreur s'est produite
	"""
	if type(filename) != str :
		return erreur("write_pickle : type filename incorrecte", -1)
	try :
		with open(filename, "wb") as fichier :
			p_file = pickle.Pickler(fichier)
			p_file.dump(data)
		
		return 0
	except :
		return erreur("write_pickle : Echec d'écriture du fichier {}".format(filename), -1)

def read_pickle(filename) :
	"""
		Récupération d'une structure de donnée stockée dans un fichier sous une forme binaire
		
		Arg:
		filename (str) : Chemin d'accès vers le fichier à lire
		
		Return:
		data : Structure de donnée récupérée 
		La fonction retourne None en cas d'erreur
	"""
	if type(filename) != str :
		return erreur("read_pickle : type du paramètre filename incorrecte", None)
	try :
		with open(filename, "rb") as fichier: 
			p_file = pickle.Unpickler(fichier)
			data = p_file.load()
		return data
	except :
		return erreur("read_pickle : Erreur lors de la lecture par la fonction", None)

def write_file(filename, data) :
	"""
	Fonction d'écriture des données en sortie sous la forme de fichier simples
	
	Args:
	filename (str) : Le chemin d'accès du fichier à écrire
	data (list | dict) : La structure de données à sauvegarder
	
	Return :
	True (bool) : La fonction retourne True en cas de succès. En cas d'erreur elle retourne False
	"""	
	if type(filename) != str :
		return erreur("write_file : Fichier filename non conforme".format(filename), False)
		
	try :
		with open(filename, "w") as fichier :
			if type(data) == list :
				for line in data :
					sublist = []
					for elt in line :
						sublist.append(str(elt))
					fichier.write("\t".join(sublist) + "\n") 
			elif type(data) == dict :
				keylist = list(data.keys())
				keylist.sort()
				for key in keylist :
					fichier.write("'{}' : {}\n".format(key, str(data[key])))
			else :
				return erreur("write_file : Erreur data n'est pas reconnu par la fonction", False)
	except : 
		return erreur("write_file : Echec de lecture du fichier {}".format(filename), False)
	
	return True
