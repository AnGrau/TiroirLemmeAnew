#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
import os
import operator

try:
    from build_tiroir import build_names_tiroirList
except ImportError :
    from TiroirLemmeAnew.build_tiroir import build_names_tiroirList

class TestFonction_build_names_tiroirList(unittest.TestCase):

    def setUp(self):
        self.gender = ['invariable', 'masculine', 'feminine']
        self.sorted_suff = {'avocat': {'invariable': [0], 'feminine': ['e', 'es'], 'masculine': ['', 's']},\
        'berceau': {'invariable': [0], 'feminine': [0, 0], 'masculine': ['', 'x']},\
        'agneau': {'invariable': [0], 'feminine': ['', 's'], 'masculine': ['', 'x']},\
        'bal': {'invariable': [0], 'feminine': [0, 0], 'masculine': ['', 's']},\
        'aval': {'invariable': [0], 'feminine': [0, 0], 'masculine': ['', 's']},\
        'ail': {'invariable': [0], 'feminine': [0, 0], 'masculine': ['', 's']},\
        'berger': {'invariable': [0], 'feminine': ['e', 'es'], 'masculine': ['', 's']}}

    def tearDown(self):
        os.system("rm -rf __pycache__")
        os.system("rm -rf tests/__pycache__")

    def test_to_print_names_tiroir(self):
        
        mem_list = []
        to_comp_list = []
        mem = build_names_tiroirList(self.sorted_suff)
        mem = sorted(mem, key=operator.itemgetter(0, 1, 2, 3))
        for elt in mem:
            mem_list.append(list(elt))
        for elt in mem_list:
            del elt[2]
        to_comp = [('-', 'feminine', 1, 1), ('-', 'masculine', 1, 1), ('e-', 'feminine', 2, 1), ('es-', 'feminine', 2, 2), ('s-', 'feminine', 1, 2), ('s-', 'masculine', 2, 2), ('x-', 'masculine', 1, 2)]
        to_comp = sorted(to_comp, key=operator.itemgetter(0, 1, 2, 3))
        for elt in to_comp:
            to_comp_list.append(list(elt))
        for elt in to_comp_list:
            del elt[2]
        for elt in mem_list:
            tmp = []
            for ref in to_comp_list:
                if elt[0] == ref[0]:
                    tmp.append(ref)
            self.assertIn(elt, tmp)
    
    def test_gender(self):
        mem = build_names_tiroirList(self.sorted_suff)
        result_gender = []
        for elt in mem:
            if elt[1] not in result_gender:
                result_gender.append(elt[1])
        self.assertEqual(self.gender[1:].sort(), result_gender.sort())
        
    def test_number(self):
        mem = build_names_tiroirList(self.sorted_suff)
        person = [0, 1, 2]
        for elt in mem:
            self.assertIn(elt[3], person)

if __name__ == '__main__':
    unittest.main()
