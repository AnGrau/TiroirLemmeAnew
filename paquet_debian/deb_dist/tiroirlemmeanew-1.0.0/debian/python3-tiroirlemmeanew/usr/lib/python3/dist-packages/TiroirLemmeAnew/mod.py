#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#Utilisation :
# ./mod.py https://fr.wikipedia.org/wiki/Wiki
#Lancement des test :
# ./mod.py -t

try:
    from html_lang import *
except ImportError :
    from TiroirLemmeAnew.html_lang import *

try:
    from clustering import main_clust
except ImportError :
    from TiroirLemmeAnew.clustering import main_clust

try:
    from build_lemme import main_lem, final_lemmeBrut
except ImportError :
    from TiroirLemmeAnew.build_lemme import main_lem, final_lemmeBrut

try:
    from build_tiroir import constructNames, construct, build_names_tiroirList, build_tiroirList
except ImportError :
    from TiroirLemmeAnew.build_tiroir import constructNames, construct, build_names_tiroirList, build_tiroirList

try:
    from data_fetch import write_pickle, write_file, read_pickle
except ImportError :
    from TiroirLemmeAnew.data_fetch import write_pickle, write_file, read_pickle

try:
    from database import connecDb, resultsToDb, modelsToDb
except ImportError :
    from TiroirLemmeAnew.database import connecDb, resultsToDb, modelsToDb

from operator import itemgetter
from pprint import pprint
import sys, os

tenses = ['indpres', 'indimpar', 'indfut', 'simple', 'cond', 'subjpres', 'subjimpar']
genders = ['masculine', 'feminine']

def flex_number(flexion, tense, person, to_save):
	"""Cette fonction retourne le numéro de flexion correspondant à une flexion (associée au temps et à la personne.

Args:
	flexion (str): la terminaison (flexion) à analyser
	tense (str): le temps ou le genre tel que définit par l'implémentation
	person (int): le nombre associé à la personne à laquelle le verbe est conjugué
	to_save (list): liste de tuples des flexions au format demandé par AnalMor
Returns:
	num (int): le numéro de flexion
	"""
	num = None
	for elt in to_save:
		if flexion == elt[0] and tense == elt[1] and person == elt[3]:
			num = elt[2]
			break
	return num
	

def rad_number(radical, gLemmeBrutVerbs):
	"""Cette fonction retourne le numéro de lemme du mot considéré.

Args:
	radical (str):le lemme considéré
	gLemmeBrutVerbs (list): la liste des lemmes (radicaux) au format demandé par AnalMor

Returns:
	num (int): le numéro de lemme
	"""
	num = None
	for elt in gLemmeBrutVerbs:
		if elt[0] == radical:
			num = elt[3]
			break
	return num

def modelling(build_block, tense, gLemmeBrutVerbs, to_save):
	"""Cette fonction crée les valeurs à ajouter au modèle pour un verbe donné, à un temps considéré ou pour un nom donné à un genre considéré.

Args:
	build_block (list): liste des tuples (flexion, radical) pour le temps ou le genre visé
	tense (str): le temps ou le genre tel que définit par l'implémentation
	gLemmeBrutVerbs (list): la liste des lemmes (radicaux) au format demandé par AnalMor
	to_save (list): liste de tuples des flexions au format demandé par AnalMor

Return:
	result (list): la liste correspondant au build_block qui doit être ajoutée au modèle
	"""
	numbers = []
	to_compare = []
	result = []
	for tup_pos in range(len(build_block)):
		temp = []
		if build_block[tup_pos]:
			flex = flex_number(build_block[tup_pos][0]+'-', tense, tup_pos + 1, to_save)
			rad = rad_number(build_block[tup_pos][1], gLemmeBrutVerbs)
			if flex and rad:
				temp = [flex, rad, tup_pos + 1]					# flexion, radical et personne
				numbers.append(temp)
	if numbers:
		for elt in numbers:
			to_compare.append((elt[0], elt[1]))
		unique = set(to_compare)
		for header in unique:
			to_add = [str(header[0]), str(header[1])]					# flexion, radical
			to_add_person = []
			for person in numbers:
				if header[0] == person[0] and header[1] == person[1]:
					to_add_person.append(str(person[2]))
			to_add.append(to_add_person)
			result.append(to_add)
	return result

def doublons(modeles):
	"""Cette fonction élimine les doublons de la variable des modèles.

Args:
	modeles (dict): le dictionnaire des modèles  au format demandé par AnalMor

Returns:
	mod_final (dict): le dictionnaire sans doublon
	"""
	doublons = []
	mod_final = {}
	for elt in modeles:
		if modeles[elt] not in doublons:
			mod_final[elt] = modeles[elt]
			doublons.append(modeles[elt])
	return mod_final

def main(url = "https://fr.wikipedia.org/wiki/France", saveTest = False):
	"""Cette fonction met en oeuvre le programme depuis la récupération du texte sur internet à l'élaboration des lemmes, flexions et modèles.

Args:
	url (str): l'adresse de la page qui amorce l'extraction  de données
	saveTest (Bool): flag permettant ou non la construction des fichiers pickles

Returns:
	-
	"""

	if len(sys.argv) == 2:
		if "-t" in sys.argv :
			saveTest = True
		else:
			url = sys.argv[1]
	else:
		exit("Un argument attendu: l'url ou -t")

	modeles = {}
	modeles_noms = {}

	print("Récupération des verbes et des noms")
	LIMIT_VERBES = 4000
	LIMIT_NOMS = 4000 
	mots, verbes, noms, dictVerbs, dictNoms = main_html(url)
	noms.sort()
	verbes.sort()
	print("\nFormation des clusters\n")
	gFamiliesVerbs, gFamiliesNames = main_clust(verbes, noms)

	print("\nConstruction des variables nécessaires\n")
	gLemmeBrutVerbs, gLemmeBrutNames, final_clustVerbs, final_clustNames = main_lem(gFamiliesVerbs, gFamiliesNames, False)

	print("\nConstruction de tiroirBrut\n")
	names_sor = constructNames(noms, final_clustNames)
	names_sorted_suff = names_sor[0]
	names_sorted_suff_complete = names_sor[1]

	sor = construct(verbes, final_clustVerbs)
	sorted_suff = sor[0]
	sorted_suff_complete = sor[1]

	names_to_save = build_names_tiroirList(names_sorted_suff)
	to_save = build_tiroirList(sorted_suff)

	print("\nConstruction des modèles de verbes\n")

	for infinitive in sorted(sorted_suff_complete):
		modeles[infinitive] = ['#']
		for tense in tenses:
			one_tense = [tense]
			to_build = sorted_suff_complete[infinitive][tense]
			to_add = modelling(to_build, tense, gLemmeBrutVerbs, to_save)
			to_add.sort(key=itemgetter(2))
			one_tense.append(to_add)
			modeles[infinitive].append(one_tense)
	
	"""print('-----------------------------------------------')
	print(len(modeles))
	pprint(modeles)"""
	mod_final = doublons(modeles)
	"""print('-----------------------------------------------')
	print(len(mod_final))
	pprint(mod_final)"""

	print("\nConstruction des modèles de noms\n")
	
	print("\nFinalisation de lemmeBrut\n")
	gLemmeBrutVerbs = final_lemmeBrut(modeles, list(mod_final.keys()), gLemmeBrutVerbs)

	"""print(gLemmeBrutNames)
	print("-----------------------------------------------------")
	print(names_to_save)
	print("-----------------------------------------------------")"""

	for ref in sorted(names_sorted_suff_complete):
		modeles_noms[ref] = ['#']
		for gender in genders:
			one_gender = [gender]
			to_build = names_sorted_suff_complete[ref][gender]
			to_add = modelling(to_build, gender, gLemmeBrutNames, names_to_save)
			to_add.sort(key=itemgetter(2))
			one_gender.append(to_add)
			modeles_noms[ref].append(one_gender)

	"""print('-----------------------------------------------')
	print(len(modeles_noms))
	pprint(modeles_noms)"""

	modeles_noms_final = doublons(modeles_noms)
	
	print("\nFinalisation de lemmeBrut\n")
	gLemmeBrutNames = final_lemmeBrut(modeles_noms, list(modeles_noms_final.keys()), gLemmeBrutNames)

	"""print('-----------------------------------------------')
	print(len(modeles_noms_final))
	pprint(modeles_noms_final)"""

	"""print(modelling([('ers', 'acqui'), ('ers', 'acqui'), ('ert', 'acqui'), ('ons', 'acquér'), ('ez', 'acquér'), ('èrent', 'acqui')], 'indpres', gLemmeBrutVerbs, to_save))"""
	
	if saveTest :
		print("\nSauvegarde des résultats pour les tests\n")
		write_pickle(gLemmeBrutVerbs, "tests/final_lemmeBrutVerbs.pickle")
		write_pickle(gLemmeBrutNames, "tests/final_lemmeBrutNames.pickle")
		write_pickle(modeles_noms, "tests/modeles_noms.pickle")
		write_pickle(modeles, "tests/modeles_verbes.pickle")
		write_pickle(modeles_noms_final, "tests/modeles_noms_final.pickle")
		write_pickle(mod_final, "tests/modeles_verbes_final.pickle")

	os.makedirs(os.getenv("HOME") + "/tiroirlemme", exist_ok=True)
	homepath = os.getenv("HOME") + "/tiroirlemme/"
	print("\nEcriture des fichiers pour les calculs statistiques dans le répertoire : {}\n".format(homepath))
	write_file(homepath + "lemmeBrutVerbs", gLemmeBrutVerbs)
	write_file(homepath + "lemmeBrutNames", gLemmeBrutNames)
	write_file(homepath + "tiroirBrutVerbs", to_save)
	write_file(homepath + "tiroirBrutNames", names_to_save)
	write_file(homepath + "modelsverbs", mod_final)
	write_file(homepath + "modelsnames", modeles_noms_final)
	
	print("\nSauvegarde des résultats dans la base de données")
	db = connecDb("tluser", "tldatabase", "tluser")
	if db == None :
		exit("Echec de connexion à la base de données")
		
	resultsToDb("lemmeBrutVerbs", gLemmeBrutVerbs, [["racine", "str"], ["lemme", "str"], ["modele", "str"], ["NoRacine", "int"]])
	resultsToDb("lemmeBrutNames", gLemmeBrutNames, [["racine", "str"], ["lemme", "str"], ["modele", "str"], ["NoRacine", "int"]])
	resultsToDb("tiroirBrutVerbs", to_save, [["terminaison", "str"], ["temps", "str"], ["NoTiroir", "int"], ["NoPersonne", "int"]])
	resultsToDb("tiroirBrutNames", names_to_save, [["terminaison", "str"], ["temps", "str"], ["NoTiroir", "int"], ["NoPersonne", "int"]])
	modelsToDb("modelsverbs", mod_final, [["key", "str"], ["shiftdown", "int"], ["shiftup", "int"], ["value", "str"]])
	modelsToDb("modelsnames", modeles_noms_final, [["key", "str"], ["shiftdown", "int"], ["shiftup", "int"], ["value", "str"]])
	
	db.close()

if __name__ == '__main__':
    if len(sys.argv) == 2:
        if "-t" in sys.argv :
            main("https://fr.wikipedia.org/wiki/France", True)
        else:
            main(sys.argv[1])
    else:
        exit("Un argument: l'url ou -t")
