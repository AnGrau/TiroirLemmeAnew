#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
import os
import sys
sys.path.append(os.getenv("PWD")[:-len("/tests")])

try:
	from clustering import *
except ImportError :
	from TiroirLemmeAnew.clustering import *

try:
	from data_fetch import *
except ImportError :
	from TiroirLemmeAnew.data_fetch import *

from pprint import pprint

class Test_clustering(unittest.TestCase) :
	
	def setUp(self) :
		path = os.getcwd()
		if "TiroirLemmeAnew/TiroirLemmeAnew/tests" in path :
			path =  ""
		elif "TiroirLemmeAnew/TiroirLemmeAnew" in path :
			path = "tests/"
		else :
			path = "TiroirLemmeAnew/tests"
		self.caracteresTest = {"sibile":"sibile", "çibile":"cibile", "sîbïle":"sibile", "sêbèle":"sebele", "sibyle":"sibile", "sibille":"sibile", "sibylle":"sibile", "sébûle":"sebule", "sigeule":"sigule", "siqule":"sicle", "sittule":"situle"}
		self.chainesTest = ["papounet", "papa", ""]
		self.lverbs = read_pickle(path + "lverbs.pickle")
		self.lnames = read_pickle(path +"lnames.pickle")
		self.pSuffixVerbs = read_pickle(path +"pSuffixVerbs.pickle")
		self.pSuffixNames = read_pickle(path +"pSuffixNames.pickle")
		self.validVerbs = read_pickle(path +"validVerbs.pickle")
		self.validNames = read_pickle(path +"validNames.pickle")
		self.score = { ('accrois', 'accroître'): [14, 'accroi', 'accroî'], ('accroissais', 'accroître'): [10, 'accroi', 'accroî'], ('accrois', 'accrois'): [1103, 'accrois'], ('accrois', 'accroissaient'): [12, 'accrois'], ('accrois', 'accroissais'): [51, 'accrois'], ('accrois', 'accroissait'): [13, 'accrois'], ('accroissaient', 'accroissais'): [411, 'accroissai'], ('accroissaient', 'accroissait'): [253, 'accroissai'], ('accroissais', 'accroissais'): [1103, 'accroissais'], ('accroissais', 'accroissait'): [302, 'accroissai'], ('nais', 'naître'): [14, 'nai', 'naî'], ('naissais', 'naître'): [10, 'nai', 'naî'], ('nais', 'nais'): [1103, 'nais'], ('nais', 'naissaient'): [12, 'nais'], ('nais', 'naissais'): [51, 'nais'], ('nais', 'naissait'): [13, 'nais'], ('naissaient', 'naissais'): [411, 'naissai'], ('naissaient', 'naissait'): [253, 'naissai'], ('naissais', 'naissais'): [1103, 'naissais'], ('naissais', 'naissait'): [302, 'naissai'] }
		self.familiesNames = [['abaisse', 'abaisses', 'abaissement', 'abaissements', ['abaissement', 'abaisse']], ['acteur', 'acteurs', 'action', 'actions', ['acteur', 'action', 'act']]]
		
	def test_transcaracter(self) :
		for motTest in self.caracteresTest.keys() :
			testeur = transcaracter(motTest)
			self.assertEqual(testeur, self.caracteresTest[motTest])	
	
	def test_p_similar(self) :
		valeur = p_similar(self.chainesTest[0], self.chainesTest[1])
		self.assertEqual(valeur, 3)
		
		valeur = p_similar(self.chainesTest[1], self.chainesTest[0])
		self.assertEqual(valeur, 3)
		
		valeur = p_similar(self.chainesTest[0], self.chainesTest[2])
		self.assertEqual(valeur, 0)
		
		valeur = p_similar(self.chainesTest[1], self.chainesTest[2])
		self.assertEqual(valeur, 0)
		
		valeur = p_similar(self.chainesTest[2], self.chainesTest[2])
		self.assertEqual(valeur, 0)
		
	def test_extract_suffixes_pairs(self) :
		test_pSuffixVerbs = extract_suffixes_pairs(self.lverbs)
		self.assertEqual(len(test_pSuffixVerbs), len(self.pSuffixVerbs))
		
		test_pSuffixNames = extract_suffixes_pairs(self.lnames)
		self.assertEqual(len(test_pSuffixNames), len(self.pSuffixNames))
		
		test_pseudo_suffix = extract_suffixes_pairs(None)
		self.assertEqual(test_pseudo_suffix, None)
		
		test_pseudo_suffix = extract_suffixes_pairs(["test"])
		self.assertEqual(test_pseudo_suffix, [])
		
		test_pseudo_suffix = extract_suffixes_pairs([])
		self.assertEqual(test_pseudo_suffix, [])
	
	def test_build_suffix_dict(self) :
		test_suffix_dict = {}
		test_suffix_dict = build_suffix_dict(test_suffix_dict, self.pSuffixVerbs, False)
		test_suffix_dict = build_suffix_dict(test_suffix_dict, self.pSuffixVerbs, True)
		self.assertNotEqual(test_suffix_dict, {})
		self.assertNotEqual(test_suffix_dict, None)
		
		test_suffix_dict = {}
		test_suffix_dict = build_suffix_dict(test_suffix_dict, self.pSuffixNames, False)
		test_suffix_dict = build_suffix_dict(test_suffix_dict, self.pSuffixNames, True)
		self.assertNotEqual(test_suffix_dict, {})
		self.assertNotEqual(test_suffix_dict, None)
		
		test_suffix_dict = build_suffix_dict(None, self.pSuffixNames, False)
		self.assertEqual(test_suffix_dict, None)
		test_suffix_dict = build_suffix_dict({}, None, False)
		self.assertEqual(test_suffix_dict, None)
		test_suffix_dict = build_suffix_dict(None, self.pSuffixNames, None)
		self.assertEqual(test_suffix_dict, None)
		test_suffix_dict = build_suffix_dict({}, {}, None)
		self.assertEqual(test_suffix_dict, None)
	
	def test_valid_pairs(self) :
		test_suffix_dict = {}
		test_suffix_dict = build_suffix_dict(test_suffix_dict, self.pSuffixVerbs, False)
		test_suffix_dict = build_suffix_dict(test_suffix_dict, self.pSuffixVerbs, True)
		test_valid = valid_pairs(self.pSuffixVerbs, test_suffix_dict)
		self.assertEqual(test_valid == self.validVerbs, True)
		
		test_suffix_dict = {}
		test_suffix_dict = build_suffix_dict(test_suffix_dict, self.pSuffixNames, False)
		test_suffix_dict = build_suffix_dict(test_suffix_dict, self.pSuffixNames, True)
		test_valid = valid_pairs(self.pSuffixNames, test_suffix_dict)
		self.assertEqual(test_valid == self.validNames, True)
		
		
		test_valid = valid_pairs(None, test_suffix_dict)
		self.assertEqual(test_valid, None)
		
		test_valid = valid_pairs(self.pSuffixVerbs, None)
		self.assertEqual(test_valid, None)
		
		test_valid = valid_pairs(["test"], test_suffix_dict)
		self.assertEqual(test_valid, None)
		
		test_valid = valid_pairs(self.pSuffixVerbs, ["test"])
		self.assertEqual(test_valid, None)
	
	def test_cluster(self) :
		#Test cluster sortie
		families = cluster(self.score)
		self.assertEqual(families, [['accrois', 'accroissaient', 'accroissais', 'accroissait', ['accroissai', 'accrois']], ['accroître', ['accroî']], ['nais', 'naissaient', 'naissais', 'naissait', ['naissai', 'nais']], ['naître', ['naî']]])
		
		#Test cluster doublons
		families = cluster(self.score)
		tmp = []
		for word_list in families[:-1]:
			tmp.append(len(set(word_list[:-1])))
		for i in range(len(tmp)):
			self.assertEqual(tmp[i], len(families[i][:-1]))
		
		#Test cluster entre listes
		families = cluster(self.score)
		for i in range(len(families) - 1):
			for j in range(i + 1, len(families)):
				common = set(families[i][:-1]) & set(families[j][:-1])
				self.assertEqual(common, set())
        
		#Test cluster stem
		stem_from_score = []
		stem_from_fam = []
		families = cluster(self.score)
		for elt in self.score:
			for stem in self.score[elt][1:]:
				stem_from_score.append(stem)
		stem_set = set(stem_from_score)
		stem_from_score = sorted(list(stem_set))
		for fam in families:
 			for stem in fam[-1]:
 				stem_from_fam.append(stem)
		stem_from_fam = sorted(stem_from_fam)
		for stem in stem_from_fam:
			self.assertIn(stem, stem_from_score)

	def test_separe_noms(self):
		new_clusts = separe_noms(self.familiesNames)
		self.assertEqual(new_clusts, [['abaissement', 'abaissements', ['abaissement']], ['abaisse', 'abaisses', ['abaisse']], ['acteur', 'acteurs', ['acteur']], ['action', 'actions', ['action']]])

if __name__ == '__main__':
	unittest.main()
