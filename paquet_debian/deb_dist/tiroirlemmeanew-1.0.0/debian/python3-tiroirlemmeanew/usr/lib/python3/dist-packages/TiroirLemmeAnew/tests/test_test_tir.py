#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
import os

try:
    from build_tiroir import *
except ImportError :
    from TiroirLemmeAnew.build_tiroir import *

class TestFonction_test_tir(unittest.TestCase):

    def setUp(self):
        self.tup_list = [('oîtrais', 'cond', 1, 2), ('oîtrait', 'cond', 1, 3), ('oîtrions', 'cond', 1, 4), ('oîtriez', 'cond', 1, 5), ('oîtraient', 'cond', 1, 6), ('erais', 'cond', 2, 2), ('erait', 'cond', 2, 3), ('erions', 'cond', 2, 4), ('eriez', 'cond', 2, 5), ('eraient', 'cond', 2, 6), ('oîtrai', 'indfut', 1, 1), ('oîtras', 'indfut', 1, 2), ('oîtra', 'indfut', 1, 3), ('oîtrons', 'indfut', 1, 4), ('oîtrez', 'indfut', 1, 5), ('oîtront', 'indfut', 1, 6), ('erai', 'indfut', 2, 1), ('eras', 'indfut', 2, 2), ('era', 'indfut', 2, 3), ('erons', 'indfut', 2, 4), ('erez', 'indfut', 2, 5), ('eront', 'indfut', 2, 6), ('oissais', 'indimpar', 1, 1), ('oissais', 'indimpar', 1, 2), ('oissait', 'indimpar', 1, 3), ('oissions', 'indimpar', 1, 4), ('oissiez', 'indimpar', 1, 5), ('oissaient', 'indimpar', 1, 6), ('ais', 'indimpar', 2, 1), ('ais', 'indimpar', 2, 2), ('ait', 'indimpar', 2, 3), ('ions', 'indimpar', 2, 4), ('iez', 'indimpar', 2, 5), ('aient', 'indimpar', 2, 6), ('ois', 'indpres', 1, 1), ('ois', 'indpres', 1, 2), ('oît', 'indpres', 1, 3), ('oissons', 'indpres', 1, 4), ('oissez', 'indpres', 1, 5), ('oissent', 'indpres', 1, 6), ('e', 'indpres', 2, 1), ('es', 'indpres', 2, 2), ('e', 'indpres', 2, 3), ('ons', 'indpres', 2, 4), ('ez', 'indpres', 2, 5), ('ent', 'indpres', 2, 6), ('us', 'simple', 1, 1), ('us', 'simple', 1, 2), ('ut', 'simple', 1, 3), ('ûmes', 'simple', 1, 4), ('ûtes', 'simple', 1, 5), ('urent', 'simple', 1, 6), ('ai', 'simple', 2, 1), ('as', 'simple', 2, 2), ('a', 'simple', 2, 3), ('âmes', 'simple', 2, 4), ('âtes', 'simple', 2, 5), ('èrent', 'simple', 2, 6), ('usse', 'subjimpar', 1, 1), ('usses', 'subjimpar', 1, 2), ('ût', 'subjimpar', 1, 3), ('ussions', 'subjimpar', 1, 4), ('ussiez', 'subjimpar', 1, 5), ('ussent', 'subjimpar', 1, 6), ('asse', 'subjimpar', 2, 1), ('asses', 'subjimpar', 2, 2), ('ât', 'subjimpar', 2, 3), ('assions', 'subjimpar', 2, 4), ('assiez', 'subjimpar', 2, 5), ('assent', 'subjimpar', 2, 6), ('oisse', 'subjpres', 1, 1), ('oisses', 'subjpres', 1, 2), ('oisse', 'subjpres', 1, 3), ('oissions', 'subjpres', 1, 4), ('oissiez', 'subjpres', 1, 5), ('oissent', 'subjpres', 1, 6), ('e', 'subjpres', 2, 1), ('es', 'subjpres', 2, 2), ('e', 'subjpres', 2, 3), ('ions', 'subjpres', 2, 4), ('iez', 'subjpres', 2, 5), ('ent', 'subjpres', 2, 6)]
        
    def tearDown(self):
        os.system("rm -rf __pycache__")
        os.system("rm -rf tests/__pycache__")
    
    def test_elt_0(self):
        ok = test_tir(('zzz', 'subjpres', 2, 6), self.tup_list)
        self.assertEqual(ok, 1)
    
    def test_elt_1(self):
        ok = test_tir(('ent', 'zzz', 2, 6), self.tup_list)
        self.assertEqual(ok, 1)
    
    def test_elt_3(self):
        ok = test_tir(('ent', 'subjpres', 2, 10), self.tup_list)
        self.assertEqual(ok, 1)
    
    def test_elt_all_different(self):
        ok = test_tir(('zzz', 'zzz', 2, 10), self.tup_list)
        self.assertEqual(ok, 1)
    
    def test_elt_in_list(self):
        ok = test_tir(('ent', 'subjpres', 2, 6), self.tup_list)
        self.assertEqual(ok, 0)

if __name__ == '__main__':
    unittest.main()
