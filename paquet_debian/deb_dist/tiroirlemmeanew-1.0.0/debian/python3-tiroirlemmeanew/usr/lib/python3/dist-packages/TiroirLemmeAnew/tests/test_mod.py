#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
import os

try:
	from mod import *
except ImportError :
	from TiroirLemmeAnew.mod import *

class Test_mod(unittest.TestCase):

	def setUp(self):
		self.to_save = [('erais-', 'cond', 1, 1), ('erais-', 'cond', 1, 2), ('erait-', 'cond', 1, 3), ('erions-', 'cond', 1, 4), ('eriez-', 'cond', 1, 5), ('eraient-', 'cond', 1, 6), ('ais-', 'cond', 4, 2), ('ait-', 'cond', 4, 3), ('ions-', 'cond', 4, 4), ('iez-', 'cond', 4, 5), ('aient-', 'cond', 4, 6), ('ais-', 'cond', 5, 1)]
		self.gLemmeBrutVerbs = [['acquerr', 'acquérir', 'acquérir', 1], ['acqui', 'acquérir', 'acquérir', 2], ['acquér', 'acquérir', 'acquérir', 3], ['acquî', 'acquérir', 'acquérir', 4]]
		self.sorted_suff_complete = {'acquérir': {'cond': [('ais', 'acquerr'), ('ais', 'acquerr'), ('ait', 'acquerr'), ('ions', 'acquerr'), ('iez', 'acquerr'), ('aient', 'acquerr')] } }
		self.modeles = { 'abattu': ['#', ['masculine', [['1', '1', ['1', '2']]]], ['feminine', []]], 'abord': ['#', ['masculine', [['1', '1', ['1', '2']]]], ['feminine', []]] }

	def tearDown(self):
		os.system("rm -rf __pycache__")
		os.system("rm -rf tests/__pycache__")

	def test_flex_number(self):
		result = flex_number('erais-', 'cond', 2, self.to_save)
		self.assertEqual(result, 1)

	def test_flex_number_none(self):
		result = flex_number('eraient-', 'cond', 2, self.to_save)
		self.assertEqual(result, None)
		result = flex_number('erais-', 'indfut', 2, self.to_save)
		self.assertEqual(result, None)
		result = flex_number('erais-', 'cond', 3, self.to_save)
		self.assertEqual(result, None)

	def test_rad_number(self):
		result = rad_number('acqui', self.gLemmeBrutVerbs)
		self.assertEqual(result, 2)

	def test_rad_number_none(self):
		result = rad_number('ac', self.gLemmeBrutVerbs)
		self.assertEqual(result, None)

	def test_modelling(self):
		to_build = self.sorted_suff_complete['acquérir']['cond']
		to_add = modelling(to_build, 'cond', self.gLemmeBrutVerbs, self.to_save)
		self.assertEqual(to_add, [['5', '1', ['1']], ['4', '1', ['2', '3', '4', '5', '6']]])

	def test_modelling_none(self):
		to_build = self.sorted_suff_complete['acquérir']['cond']
		to_add = modelling(to_build, 'indfut', self.gLemmeBrutVerbs, self.to_save)
		self.assertEqual(to_add, [])

	def test_doublons(self):
		mod_final = doublons(self.modeles)
		self.assertIn(mod_final, [{'abattu': ['#', ['masculine', [['1', '1', ['1', '2']]]], ['feminine', []]]}, {'abord': ['#', ['masculine', [['1', '1', ['1', '2']]]], ['feminine', []]]}])

if __name__ == '__main__':
	unittest.main()
