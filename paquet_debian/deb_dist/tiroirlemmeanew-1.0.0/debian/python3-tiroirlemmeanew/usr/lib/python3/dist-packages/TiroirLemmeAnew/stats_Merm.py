#! /usr/bin/env python3

from pprint import pprint
from math import ceil

try:
    import data_fetch
except ImportError :
    import TiroirLemmeAnew.data_fetch

try:
    import clustering
except ImportError :
    import TiroirLemmeAnew.clustering

def stats_pseudo_suffix_pairs(verbes, noms, pspVerbs, pspNames) :
	print("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n")
	print("\nstats_pseudo_suffix_pairs : Statistiques des paires de pseudo-suffixes\n")
	nb_couples_verbes_totaux = len(verbes) * len(verbes)
	nb_couples_verbes = len(pspVerbs)
	
	nb_couples_noms_totaux = len(noms) * len(noms)
	nb_couples_noms = len(pspNames)
	
	print("Verbes : Nombre de paires de pseudo-suffixes {} | Nombre total de couples possibles {} | soit {} % utilisé(s)\n".format(nb_couples_verbes, nb_couples_verbes_totaux, ceil(float(nb_couples_verbes) / nb_couples_verbes_totaux * 100)))
	
	print("Noms : Nombre de paires de pseudo-suffixes {} | Nombre total de couples possibles {} | soit {} % utilisé(s)\n".format(nb_couples_noms, nb_couples_noms_totaux, ceil(float(nb_couples_noms) / nb_couples_noms_totaux * 100)))
	
def stats_cooccurrences(suffScorVerbs, score_stemVerbs,suffScorNames, score_stemNames ) :
	print("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n")
	print("\nstats_cooccurrences : Statistiques sur les calculs de cooccurrences\n")
	#~~~~~ Déclaration des variables ~~~~~
	
	#Etude de la structure suffScor pour les verbes et les noms
	somme_cooccur_verbes = 0
	somme_cooccur_tot_verbes = 0
	somme_cooccur_noms = 0
	somme_cooccur_tot_noms = 0
	nb_couples_verbes = 0
	nb_couples_noms = 0
	
	#Etude de la structure score_stem pour les verbes et les noms
	valid_cooccur_verbes = 0
	valid_cooccur_noms = 0
	valid_nb_couples_verbes = len(score_stemVerbs)
	valid_nb_couples_noms = len(score_stemNames)
	
	#~~~~~Réalisation des calculs~~~~~
	
	#Calculs pour les verbes de suffScor
	for k in suffScorVerbs :
		nb_couples_verbes += len(suffScorVerbs[k])
		for k2 in suffScorVerbs[k] :
			somme_cooccur_verbes += suffScorVerbs[k][k2][0]
			somme_cooccur_tot_verbes += suffScorVerbs[k][k2][1]
	
	moyenne_cooccur_verbes = ceil(somme_cooccur_verbes / nb_couples_verbes)
	moyenne_cooccur_tot_verbes = ceil(somme_cooccur_tot_verbes / nb_couples_verbes)
	
	#Calculs pour les noms de suffScor
	for k in suffScorNames :
		nb_couples_noms += len(suffScorNames[k])
		for k2 in suffScorNames[k] :
			somme_cooccur_noms += suffScorNames[k][k2][0]
			somme_cooccur_tot_noms += suffScorNames[k][k2][1]
	
	moyenne_cooccur_noms = ceil(somme_cooccur_noms / nb_couples_noms)
	moyenne_cooccur_tot_noms = ceil(somme_cooccur_tot_noms / nb_couples_noms)
	
	valid_pairs_perc_verbes = ceil(len(score_stemVerbs) / nb_couples_verbes * 100)
	valid_pairs_perc_noms = ceil(len(score_stemNames) / nb_couples_noms * 100)
	
	#Calculs pour les verbes de score_stem
	for k in score_stemVerbs :
		valid_cooccur_verbes += score_stemVerbs[k][0]
	
	#Calculs pour les noms de score_stem
	for k in score_stemNames :
		valid_cooccur_noms += score_stemNames[k][0]
	
	moyenne_valid_verbes = ceil(valid_cooccur_verbes / valid_nb_couples_verbes)
	moyenne_valid_noms = ceil(valid_cooccur_noms / valid_nb_couples_noms)
	
	#~~~~~Affichage des résultats~~~~~
	print("\nVerbes\n")
	print("Paires de pseudo-suffixes")
	print("Moyenne de cooccurrence des verbes : {}".format(moyenne_cooccur_verbes))
	print("Moyenne de cooccurrence totale des verbes : {}".format(moyenne_cooccur_tot_verbes))
	print("\nPaires de suffixes valides")
	print("Moyenne de cooccurrence totale pour les verbes : {}".format(moyenne_valid_verbes))
	print("\nEnviron {} % de paires de suffixes sont validés sur l'ensemble des paires de pseudo-suffixes".format(valid_pairs_perc_verbes))
	
	print("\nNoms\n")
	print("Paires de pseudo-suffixes")
	print("Moyenne de cooccurrence des noms : {}".format(moyenne_cooccur_noms))
	print("Moyenne de cooccurrence totale des noms : {}".format(moyenne_cooccur_tot_noms))
	print("\nPaires de suffixes valides")
	print("Moyenne de cooccurrence totale pour les noms : {}".format(moyenne_valid_noms))
	print("\nEnviron {} % de paires de suffixes sont validés sur l'ensemble des paires de pseudo-suffixes".format(valid_pairs_perc_noms))
	
			
def stats_gaussier(verbes, noms, clusters_verbes, clusters_noms) :
	print("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n")
	print("\nstats_gaussier : Statistiques sur l'algorithme de Gaussier\n")
	#~~~~~ Déclaration des variables ~~~~~
	lost_list_verbs = []
	lost_list_names = []
	
	#~~~~~ Réalisation des calculs ~~~~~
	for v in verbes :
		inside = False
		for cluster in clusters_verbes :
			if v[0] in cluster :
				inside = True
				break
		if inside == False :
			lost_list_verbs.append(v)
	
	for n in noms :
		inside = False
		for cluster in clusters_noms :
			if n[0] in  cluster :
				inside = True
				break
		if inside == False :
			lost_list_names.append(n)
			
	#~~~~~ Affichage des résultats ~~~~~
	
	print("\nVerbes\n")
	print("Affichage des verbes perdus {} sur {} \n".format(len(lost_list_verbs), len(verbes)))
	pprint(lost_list_verbs)
	
	print("\nNoms\n")
	print ("\nAffichage des noms perdus {} sur {}\n".format(len(lost_list_names), len(noms)))
	pprint(lost_list_names)

def stats_final_clust(familiesVerbs, familiesNames, final_clustVerbs, final_clustNames) :
	print("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n")
	print("\nstats_final_clust : Statistiques sur la finalisation des clusters\n")
	
	#~~~~~Déclaration des variables ~~~~~
	lost_cluster_verbes = []
	lost_cluster_noms = []
	nb_clusters_verbes = 0
	nb_clusters_noms = 0
	nb_final_clust_verbes = 0
	nb_final_clust_noms = 0
	
	#~~~~~Réalisation des calculs~~~~~
	#Calculs des verbes dans familiesVerbs
	for cluster in familiesVerbs :
		if(len(cluster) > 0) :
			ajout = len(cluster) - 1
		else :
			ajout = 0
		nb_clusters_verbes += ajout
	
	#Calculs des noms dans familiesNames
	for cluster in familiesNames :
		if(len(cluster) > 0) :
			ajout = len(cluster) - 1
		else :
			ajout = 0
		nb_clusters_noms += ajout
	
	#Calculs des verbes dans final_clustVerbs
	for k in final_clustVerbs :
		if len(final_clustVerbs[k]) > 0 :
			ajout = len(final_clustVerbs[k]) -1
		else :
			ajout = 0
		nb_final_clust_verbes += ajout
	
	#Calculs des noms dans final_clustNames
	for k in final_clustNames :
		if len(final_clustNames[k]) > 0 :
			ajout = len(final_clustNames[k]) -1
		else :
			ajout = 0
		nb_final_clust_noms += ajout
	
	for cluster in familiesVerbs :
		inside = False
		for k in final_clustVerbs :
			if cluster[0] in final_clustVerbs[k] :
				inside = True
				break
		if inside == False :
			lost_cluster_verbes.append(cluster)
	
	for cluster in familiesNames :
		inside = False
		for k in final_clustNames :
			if cluster[0] in final_clustNames[k] :
				inside = True
				break
		if inside == False :
			lost_cluster_noms.append(cluster)
	
	#~~~~~Affichage des résultats ~~~~~
	print("\nVerbes\n")
	print("Nombre de verbes après l'algorithme de gaussier {}".format(nb_clusters_verbes))
	print("Nombre de verbes après la finalisation des clusters {}".format(nb_final_clust_verbes))
	print("Perte de {} verbe(s)".format(nb_clusters_verbes - nb_final_clust_verbes))
	print("Affichage des clusters orphelins")
	pprint(lost_cluster_verbes)
	
	print("\nNoms\n")
	print("Nombre de noms après l'algorithme de gaussier {}".format(nb_clusters_noms))
	print("Nombre de noms après la finalisation des clusters {}".format(nb_final_clust_noms))
	print("Perte de {} nom(s)".format(nb_clusters_noms - nb_final_clust_noms))
	print("Affichage des clusters orphelins")
	pprint(lost_cluster_noms)
	
def stats_models_viability(modelsverbs, modelsnames, lemmeBrutVerbs, lemmeBrutNames) :
	print("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n")
	print("stats_models_viability : Statistiques sur la viablité des modèles")
	
	#~~~~~ Déclaration des variables ~~~~~
	nb_temps_vides = 0
	nb_verbes_incomplets = 0
	nb_assoc = 0
	faux_verbes = []
	faux_noms = []
	
	#~~~~~ Calculs et affichage des résultats pour les verbes ~~~~~
	print("\nVerbes\n")
	for verb in modelsverbs :
		if (verb[-3:] == "ère") or (verb[-4:] == "eure") :
			faux_verbes.append(verb)
		for elt in modelsverbs[verb] :
			if type(elt) == list :
				nb_temps_vides += elt.count([])
		if nb_temps_vides > 0 :
			print("verbe '{}' | nombre de temps vides {} | Affichage du modèle {}\n".format(verb, nb_temps_vides, modelsverbs[verb]))
			nb_temps_vides = 0
			nb_verbes_incomplets += 1
	
	print("Nombre de modèles incomplets {} sur {}\n".format(nb_verbes_incomplets, len(modelsverbs)))
	
	print("Liste des verbes erronés\n")
	for verb in faux_verbes :
		print("{} | {}\n".format(verb, modelsverbs[verb]))
	print("Nombre de verbes erronés {} sur {}\n".format(len(faux_verbes), len(modelsverbs)))
	
	print("Association des verbes modèles à lemmeBrut\n")
	for verb in modelsverbs :
		nb_assoc = 0
		for line in lemmeBrutVerbs :
			if verb in line :
				nb_assoc += 1 
		print("Nombre d'associations au verbe {} : {}".format(verb, nb_assoc))
	
	#~~~~~Calcul et affichage des résultats pour les noms ~~~~~
	
	print("\nNoms\n")
	print("Association des noms modèles à lemmeBrut\n")
	for name in modelsnames :
		nb_assoc = 0
		for line in lemmeBrutNames :
			if name in line :
				nb_assoc += 1 
		print("Nombre d'associations au nom {} : {}".format(name, nb_assoc))	
		if nb_assoc <= 10 :
			faux_noms.append(name)
	
	print("\nNombre de modèles de noms avec peu d'association {} sur {}".format(len(faux_noms), len(modelsnames)))
	for name in faux_noms :
		print("{} | {}\n".format(name, modelsnames[name]))

def stats_lemmeBrut(lemmeBrutVerbs, lemmeBrutNames, final_clustVerbs, final_clustNames, lverbs, lnames) :
	print("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n")
	print("stats_lemmeBrut : Statistiques sur lemmeBrut")
	
	#~~~~~ Préparation des variables ~~~~~
	nb_lemmes_verbes = len(final_clustVerbs)
	nb_lemmes_noms = len(final_clustNames)
	nb_racines_verbes = len(lemmeBrutVerbs)
	nb_racines_noms = len(lemmeBrutNames)
	nb_flexions_verbes = len(lverbs)
	nb_flexions_noms = len(lnames)
	
	#~~~~~ Affichage des résultats ~~~~~
	print("\nVerbes\n")
	print("Nous avons extraits {} verbes".format(nb_flexions_verbes))
	print("Sur cet ensemble lemmeBrut se compose de {} lemmes".format(nb_lemmes_verbes))
	print("Sur cet ensemble lemmeBrut se compose de {} racines".format(nb_racines_verbes))
	
	print("\nNoms\n")
	print("Nous avons extraits {} noms".format(nb_flexions_noms))
	print("Sur cet ensemble lemmeBrut se compose de {} lemmes".format(nb_lemmes_noms))
	print("Sur cet ensemble lemmeBrut se compose de {} racines".format(nb_racines_noms))
				
	

def main() :
	print("\nSTATISTIQUES SUR TIROIRLEMME\n")
	verbes = data_fetch.read_pickle("tests/lverbs.pickle")
	noms = data_fetch.read_pickle("tests/lnames.pickle")
	pspVerbs = data_fetch.read_pickle("tests/pSuffixVerbs.pickle")
	pspNames = data_fetch.read_pickle("tests/pSuffixNames.pickle")
	score_stemVerbs = data_fetch.read_pickle("tests/validVerbs.pickle")
	score_stemNames = data_fetch.read_pickle("tests/validNames.pickle")
	familiesVerbs = data_fetch.read_pickle("tests/familiesVerbs.pickle")
	familiesNames = data_fetch.read_pickle("tests/familiesNames.pickle")
	final_clustVerbs = data_fetch.read_pickle("tests/final_clust.pickle")
	final_clustNames = data_fetch.read_pickle("tests/final_clustName.pickle")
	modelsverbs = data_fetch.read_pickle("tests/modeles_verbes_final.pickle")
	modelsnames = data_fetch.read_pickle("tests/modeles_noms_final.pickle")
	lemmeBrutVerbs = data_fetch.read_pickle("tests/final_lemmeBrutVerbs.pickle")
	lemmeBrutNames = data_fetch.read_pickle("tests/final_lemmeBrutNames.pickle")
	
	suffScorVerbs = {}
	suffScorVerbs = clustering.build_suffix_dict(suffScorVerbs, pspVerbs, False)
	if suffScorVerbs == None :
		exit("Echec de construction du dictionnaire pour les scores de similarité < 5")
		
	suffScorVerbs = clustering.build_suffix_dict(suffScorVerbs, pspVerbs, True) #Score de similarité >= 5
	if suffScorVerbs == None :
		exit("Echec de construction du dictionnaire pour les scores de similarité >= 5")
	
	suffScorNames = {}
	suffScorNames = clustering.build_suffix_dict(suffScorNames, pspNames, False)
	if suffScorNames == None :
		exit("Echec de construction du dictionnaire pour les scores de similarité < 5")
		
	suffScorNames = clustering.build_suffix_dict(suffScorNames, pspNames, True) #Score de similarité >= 5
	if suffScorNames == None :
		exit("Echec de construction du dictionnaire pour les scores de similarité >= 5")
	
	stats_pseudo_suffix_pairs(verbes, noms, pspVerbs, pspNames)
	
	stats_cooccurrences(suffScorVerbs, score_stemVerbs,suffScorNames, score_stemNames)
	
	stats_gaussier(verbes, noms, familiesVerbs, familiesNames)
	
	stats_final_clust(familiesVerbs, familiesNames, final_clustVerbs, final_clustNames)
	
	stats_models_viability(modelsverbs, modelsnames, lemmeBrutVerbs, lemmeBrutNames)
	
	stats_lemmeBrut(lemmeBrutVerbs, lemmeBrutNames, final_clustVerbs, final_clustNames, verbes, noms)

main()
	
