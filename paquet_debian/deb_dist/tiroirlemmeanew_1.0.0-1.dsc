Format: 3.0 (quilt)
Source: tiroirlemmeanew
Binary: python3-tiroirlemmeanew
Architecture: all
Version: 1.0.0-1
Maintainer: Boris Merminod, Anthony Grau <anthony.grau@etud.univ-paris8.fr>
Standards-Version: 3.9.1
Build-Depends: python3-setuptools, python3-all, debhelper (>= 7.4.3)
Package-List:
 python3-tiroirlemmeanew deb python optional arch=all
Checksums-Sha1:
 12ef112a0a44f54f460bd0a408ea4ba8960ff131 75473643 tiroirlemmeanew_1.0.0.orig.tar.gz
 6572fa9db9412254450bc0e439575c7c9ffaec44 3260 tiroirlemmeanew_1.0.0-1.debian.tar.xz
Checksums-Sha256:
 6cb44cd52ddd085df59b2bbc6087d94c7a100c658d1666fd7dca0e0e2bdbe36b 75473643 tiroirlemmeanew_1.0.0.orig.tar.gz
 d3eb8697e27d842d8798274205e9ae6d34382880dc76a63fd170fd499ff7a2a7 3260 tiroirlemmeanew_1.0.0-1.debian.tar.xz
Files:
 9e20a08b0cdd49927fd88c010d5b4801 75473643 tiroirlemmeanew_1.0.0.orig.tar.gz
 a8fa98f3c4cc75fc4c717b3a4d0649ac 3260 tiroirlemmeanew_1.0.0-1.debian.tar.xz
