Installer le programme:
=======================

Le nécessaire d'installation se trouve dans le répertoire /installation. Il contient :
- Le script install_sh
- le paquet debian : python3-tiroirlemmeanew_1.0.0-1_all.deb

On peut vérifier et installer les dépendances via :

$ ./install_script.sh

Puis installer ensuite le programme avec :

$ sudo dpkg -i python3-tiroirlemmeanew_1.0.0-1_all.deb

En utilisant le paramètre -i le script peut vérifier les dépendances et installer le programme :

$ ./install_script.sh -i 

Desinstaller le programme:
==========================

$ sudo apt-get remove python3-tiroirlemmeanew

Utiliser le programme:
======================

Une fois installé:

$ tiroirLemme "https://fr.wikipedia.org/wiki/Commode_(empereur)"

Format:

$ tiroirLemme url

Veuillez vous reporter à la documentation en ouvrant le fichier 'doc/index.html'.

Git global setup
================

git config --global user.name "Grau Anthony"

git config --global user.email "anthony.grau@gmail.com"

Create a new repository
=======================

git clone https://gitlab.com/AnGrau/TiroirLemmeAnew.git

cd TiroirLemmeAnew

touch README.md

git add README.md

git commit -m "add README"

git push -u origin master

Existing folder
===============

cd existing_folder

git init

git remote add origin https://gitlab.com/AnGrau/TiroirLemmeAnew.git

git add .

git commit -m "Initial commit"

git push -u origin master

Existing Git repository
=======================

cd existing_repo

git remote rename origin old-origin

git remote add origin https://gitlab.com/AnGrau/TiroirLemmeAnew.git

git push -u origin --all

git push -u origin --tags
